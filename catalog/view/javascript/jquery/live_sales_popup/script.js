
var liveSalesPopupCurrentOrder = 0;
var liveSalesPopupI = null;
var liveSalesPopupT = null;
var liveSalesPopupT2 = null;
var liveSalesPopupOrdersDisplayed = [];
var liveSalesPopupClosed = false;
$(function(){
    if (liveSalesPopupNotificationSound){
        $('body').append('<audio id="liveSalesPopup-sound" src="' + liveSalesPopupNotificationSound + '"></audio>')
    }
    if (liveSalesPopupGetCookie('liveSalesPopup_close') != 1){
        setTimeout(liveSalesPopupLoadOrder, liveSalesPopupGetDelay(liveSalesPopupDelayFirstMin, liveSalesPopupDelayFirstMax));
    }else{
        liveSalesPopupClosed = true;
    }
    $('body').on('mouseenter', '#liveSalesPopup-notification', function(){
        clearTimeout(liveSalesPopupT);
        clearTimeout(liveSalesPopupT2);
        clearInterval(liveSalesPopupI);
    });
    $('body').on('click', '#liveSalesPopup-notification .liveSalesPopup-close-button', function(){
        liveSalesPopupCreateCookie('liveSalesPopup_close', 1, liveSalesPopupCloseLifetime);
        clearTimeout(liveSalesPopupT);
        clearTimeout(liveSalesPopupT2);
        clearInterval(liveSalesPopupI);
        liveSalesPopupClosed = true;
        $('#liveSalesPopup-notification').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $('#liveSalesPopup-notification').removeClass('active');
        });
        $('#liveSalesPopup-notification').removeClass(liveSalesPopupInAnimation).addClass(liveSalesPopupOutAnimation);
    });
    $('body').on('mouseleave', '#liveSalesPopup-notification', function(){
        liveSalesPopupT = setTimeout(function(){
            if (!liveSalesPopupClosed){
                $('#liveSalesPopup-notification').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                    $('#liveSalesPopup-notification').removeClass('active');
                });
                $('#liveSalesPopup-notification').removeClass(liveSalesPopupInAnimation).addClass(liveSalesPopupOutAnimation);
                liveSalesPopupT2 = setTimeout(liveSalesPopupLoadOrder, liveSalesPopupGetDelay(liveSalesPopupDelayMin, liveSalesPopupDelayMax));
            }
        }, liveSalesPopupDisplayTime);
    });
});

var liveSalesPopupCreateCookie = function(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function liveSalesPopupGetCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return 0;
}

function liveSalesPopupGetDelay(min, max){
    return Math.random() * (max - min) + min;
}

function liveSalesPopupLoadOrder(){
    if (liveSalesPopupDisplayTimes > 0 && liveSalesPopupDisplayTimes <= parseInt(liveSalesPopupGetCookie('liveSalesPopup_c'))){
        return false;
    }
    $.post(
        liveSalesPopupAjaxUrl, {
            id: liveSalesPopupCurrentOrder,
            displayed: liveSalesPopupOrdersDisplayed,
            action: 'getOrder',
            lastCart: liveSalesPopupLastCartItem,
            ajax: true
        }, function(data){
            if (data && data.content){
                $('#liveSalesPopup-notification').remove();
                $('body').append(data.content);
                setTimeout(function(){
                    if (liveSalesPopupDisplayTimes > 0){
                        var liveSalesPopupC = parseInt(liveSalesPopupGetCookie('liveSalesPopup_c')) + 1;
                        liveSalesPopupCreateCookie('liveSalesPopup_c', liveSalesPopupC);
                    }
                    $('#liveSalesPopup-notification').addClass('active').addClass('animated').addClass(liveSalesPopupInAnimation);
                    if (liveSalesPopupNotificationSound){
                        document.getElementById('liveSalesPopup-sound').play();
                    }
                }, 200);
                liveSalesPopupT = setTimeout(function(){
                    if (!liveSalesPopupClosed){
                        liveSalesPopupT2 = setTimeout(liveSalesPopupLoadOrder, liveSalesPopupGetDelay(liveSalesPopupDelayMin, liveSalesPopupDelayMax));
                        $('#liveSalesPopup-notification').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                            $('#liveSalesPopup-notification').removeClass('active');
                        });
                        $('#liveSalesPopup-notification').removeClass(liveSalesPopupInAnimation).addClass(liveSalesPopupOutAnimation);
                    }
                }, liveSalesPopupDisplayTime);
            }else{
                if (!liveSalesPopupClosed){
                    liveSalesPopupT2 = setTimeout(liveSalesPopupLoadOrder, liveSalesPopupGetDelay(liveSalesPopupDelayMin, liveSalesPopupDelayMax));
                }
            }
            if (data && data.type && data.type == 'order' && data.order){
				//alert(data.reset  + '&&' + data.loop);
                if (data.reset && data.loop){
                    liveSalesPopupCurrentOrder = null;
                    liveSalesPopupOrdersDisplayed = [];
                }
                liveSalesPopupCurrentOrder = data.order;
                if (liveSalesPopupOrdersDisplayed.indexOf(liveSalesPopupCurrentOrder) == -1){
                    liveSalesPopupOrdersDisplayed.push(liveSalesPopupCurrentOrder);
                }
            }
        }, 'json'
    );
}