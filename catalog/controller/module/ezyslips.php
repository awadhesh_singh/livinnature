<?php

error_reporting(0);
ini_set("display_errors",0);

include_once(DIR_SYSTEM.'library/ezyslipsRest.php');

class ControllerModuleEzyslips extends Controller {  
	
	private $ezy_rest = NULL;
	private $key_hash = '';
	private $service  = '';
	private function initiate(){
		$this->ezy_rest = new EzyslipsRest();
		
		$this->service 	= (isset($_REQUEST['service'])) ? trim($_REQUEST['service']) : '';		
		$this->key_hash	= (isset($_REQUEST['key_hash'])) ? trim($_REQUEST['key_hash']) : '';
		
		if(!$this->key_hash || !$this->isValidService($this->service)){
			$this->ezy_rest->response(array('status' => 'error' ,'errors' => 'Bad REquest: Invalid inputs.'),400);
		}
	}
	public function index(){
		
		$this->initiate();
		
		$this->load->model('extension/module/ezyslips');
		
		if(!$this->model_extension_module_ezyslips->authenticate($this->key_hash)){
			$this->ezy_rest->response(array('status' => 'error' ,'errors' => 'Authentication failed.'),401);
		}
		
		$func = 'service'.$this->service;
		
		if(method_exists($this,$func)){
			return call_user_func( array($this , $func) ,$_REQUEST );
		}else{
			$this->ezy_rest->response(array('status' => 'error' ,'errors' => 'Service not implemented.'),501);
		}
		
	}
	
	protected function serviceGetOrders($params = array()){
         $this->load->model('setting/setting');
         
        $params['order_status'] =$this->config->get('module_ezyslips_connector_selected');
        $order_m_status = $this->config->get('module_ezyslips_connector_status');
        if($order_m_status=='1'){
        	
		list( $orderList,$params) = $this->model_extension_module_ezyslips->getOrderList($params);
		
		$formattedOrders = array();
		
		if(!empty($orderList)){
			
			foreach($orderList as $order){
				
				$order_id = $order['order_id'];
				
				$order_info = $this->model_extension_module_ezyslips->getOrder($order_id);
				
				if(!empty($order_info)){
					
					$orderCluster = array();
					$orderCluster['order_info'] = $order_info;
					
					$order_data['order_id'] = $order_info['order_id'];
					$order_data['date_added'] = $order_info['date_added'];
					$order_data['shipping_cost'] 	= 0;
					$order_data['total_discount'] 	= 0;
					$order_data['total_tax'] 		= 0;
					$order_data['order_total'] 		= 0;
					
					$order_total = $this->model_extension_module_ezyslips->getOrderTotals($order_id);					
					
					$orderCluster['order_total'] = $order_total;
					
					foreach($order_total as $total){
					
						$text = ( isset( $order_total['text'] ) ) ? 'text' : 'code';
						
						switch( trim($total[$text]) ){
							case 'total' 	: 	$order_data['order_total'] 		+= $total['value'];
												break;
							case 'shipping'	:	$order_data['shipping_cost']	+= $total['value'];
												break;
							case 'tax'		: 	$order_data['total_tax']		+= $total['value'];
												break;
							case 'coupon'	: 	$order_data['total_discount']	+= abs($total['value']);
												break;
						}
						
					}
					
					$order_data['store_code'] = 6;
					$order_data['payment_type'] = $order_info['payment_method'];
					$order_data['email'] = $order_info['email'];
					
					$order_products = $this->model_extension_module_ezyslips->getOrderProducts($order_id);
					
					$orderCluster['order_products'] = $order_products;					
										
					$products = array();
				
					foreach($order_products as $product){
						$products[$product['product_id']]['product_id'] 	= $product['product_id'];
						$products[$product['product_id']]['price'] 			= $product['price'];
						$products[$product['product_id']]['name'] 			= $product['name'];
						$products[$product['product_id']]['product_code'] 	= $product['model'];
						$products[$product['product_id']]['amount'] 		= $product['quantity'];
						$products[$product['product_id']]['weight'] 		= '';
						$products[$product['product_id']]['dimentions'] 	= '';
					}
					
					$order_data['products'] = $products;
					
					$user_data = array();
				
					$fields = 'firstname|lastname|address_1|address_2|city';
					
					
					foreach(explode('|',$fields) as $field){
						$user_data['billing_address'][$field] = $order_info['payment_'.$field];
						$user_data['shipping_address'][$field] = $order_info['shipping_'.$field];
					}
					
					$user_data['billing_address']['phone'] 		= $order_info['telephone'];
					$user_data['billing_address']['state'] 		= $order_info['payment_zone'];
					$user_data['billing_address']['country'] 	= $order_info['payment_country'];
					$user_data['billing_address']['zipcode'] 	= $order_info['payment_postcode'];
					$user_data['shipping_address']['phone'] 	= $order_info['telephone'];
					$user_data['shipping_address']['state'] 	= $order_info['shipping_zone'];
					$user_data['shipping_address']['country'] 	= $order_info['shipping_country'];
					$user_data['shipping_address']['zipcode'] 	= $order_info['shipping_postcode'];
					
					$order_data['user_data'] = $user_data;
					
					$orderCluster = base64_encode(json_encode($orderCluster));
					$order_data['order_hash'] = $orderCluster;
					$formattedOrders[$order['order_id']] = $order_data;
				}
			}
		}
		//echo "<pre>";print_r($formattedOrders);die;
		$this->ezy_rest->response( array('orders' => $formattedOrders , 'params' => $params ), 200);
	   }	
	}
	
	private function isValidService($service){
		$availableServices = array('GetOrders');		
		return in_array($service , $availableServices);
	}
	
}