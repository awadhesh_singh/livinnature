<?php
class ControllerExtensionModuleLiveSalesPopup extends Controller {
	public function index() {
		$this->load->language('module/live_sales_popup');

		$this->document->addStyle('catalog/view/javascript/jquery/live_sales_popup/style.css');
		$this->document->addStyle('catalog/view/javascript/jquery/live_sales_popup/animate.css');
		$this->document->addScript('catalog/view/javascript/jquery/live_sales_popup/script.js');
		
		$data['live_sales'] =  $this->config->get('module_live_sales_popup');
		$this->session->data['order'] = 0;
		return $this->load->view('extension/module/live_sales_popup', $data);
	}
	
	public function ajax(){
		
		$this->load->model('extension/module/live_sales_popup');
		$this->load->model('tool/image');
		
		$json = array();

		$orders = $this->model_extension_module_live_sales_popup->getliveOrders();

		$live_sales =  $this->config->get('module_live_sales_popup');
		
		/*if($live_sales['cutomer_session'] == $this->session->data['order']){
			$this->session->data['order'] = 0;
		}*/
		if(isset($this->session->data['order'])){
			$this->session->data['order'] += 1;
		}else{
			$this->session->data['order'] = 0;			
		}	
		
		

		$variables = array( //'{total_items}',
			'{firstname}', '{lastname}', '{firstname_lastname}', '{lastname_firstname}', '{country}',  '{state}','{city}', '{date}', '{time_ago}', '{order_total}', '{product_name}', '{product_name_with_opts}'
		);
		
		$products = $this->model_extension_module_live_sales_popup->getOrderProducts();
//print_r($products);
		//for($i=0; $i<sizeof($products); $i++){
		if(isset($products[$this->session->data['order']]['order_id'])){
			$order = $this->model_extension_module_live_sales_popup->getliveOrder($products[$this->session->data['order']]['order_id']);
			
			if(isset($products[$this->session->data['order']]['image']) && $products[$this->session->data['order']]['image']){
				$image = $this->model_tool_image->resize($products[$this->session->data['order']]['image'], $live_sales['height'], $live_sales['width']);
			}else{
				$image = $this->model_tool_image->resize('no_image.png', $live_sales['height'], $live_sales['width']);
			}
			
			$replace = array(
				'firstname' => $order['firstname'],
				'lastname'  => $order['lastname'],
				'firstname_lastname' => $order['firstname'] . ' ' . $order['lastname'],
				'lastname_firstname' => $order['lastname'] . ' ' . $order['firstname'],
				'country'   => $order['shipping_country'],
				'state'      => $order['shipping_zone'],
				'city'      => $order['shipping_city'],
				'date'   =>  date($this->language->get('date_format_short'), strtotime($order['date_added'])),
				'time_ago'   => $this->time_elapsed_string($order['date_added']),
				'order_total' => $this->currency->format($order['total'], $order['currency_code'], $order['currency_value']), 
				//'{total_items}', 
				'product_name' => '<a href="'. $this->url->link('product/product', 'product_id='. $products[$this->session->data['order']]['product_id'] ).'">'. $products[$this->session->data['order']]['name'] .'</a>', 
				'product_name_with_opts' => ''
			);				
			
			$html = '<div id="liveSalesPopup-notification" class="animated">';
			$html .= '	<div class="liveSalesPopup-inner">';
			$html .= '		<div class="liveSalesPopup-image">';
			$html .= '			<a href="'. $this->url->link('product/product', 'product_id='. $products[$this->session->data['order']]['product_id'] ).'"><img src="' . $image .'" /></a>';
			$html .= '		</div>';
			$html .= '		<div class="liveSalesPopup-content">';
			if($live_sales['content_line_1'] != ''){
				$html .= '			<p class="liveSalesPopup-line-1">'. str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($variables, $replace, $live_sales['content_line_1'])))) . '</p>';
			}
			if($live_sales['content_line_2'] != ''){
				$html .= '			<p class="liveSalesPopup-line-2">'. str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($variables, $replace, $live_sales['content_line_2'])))) . '</p>';
			}
			if($live_sales['content_line_3'] != ''){
				$html .= '			<p class="liveSalesPopup-line-3">'. str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($variables, $replace, $live_sales['content_line_3'])))) . '</p>';
			}	
			if($live_sales['content_line_4'] != ''){
				$html .= '			<p class="liveSalesPopup-line-4">'. str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($variables, $replace, $live_sales['content_line_4'])))) . '</p>';
			}	
			if($live_sales['content_line_5']){
				$html .= '			<p class="liveSalesPopup-line-5">'. str_replace(array("\r\n", "\r", "\n"), ' ', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), ' ', trim(str_replace($variables, $replace, $live_sales['content_line_5'])))) . '</p>';
			}	
			
			$html .= '<a class="liveSalesPopup-whole-link" target="_blank" href="' . $this->url->link('product/product', 'product_id='. $products[$this->session->data['order']]['product_id']) . '"></a>';
			
			if($live_sales['close_button']){
				$html .= '<button class="liveSalesPopup-close-button">×</button>';
			}	
			$html .= '		</div>';
			$html .= '	</div>';
			
			$html .= '</div>';
			//$end_order = end($products);
			$json['order_id'] = $products[$this->session->data['order']]['order_id'];
			$json['order'] = $this->session->data['order'];
			$json['type'] = 'order';
		//	$json['lstid'] = $end_order['order_id'];
			foreach($products as $key => $val){
				$end_order = $key;
			}
			
			//echo $end_order . '==' . $this->session->data['order'];
			if($end_order == $this->session->data['order']){
				if($live_sales['loop']){
					$json['reset'] = 1;				
					$this->session->data['order'] = 0;
				}	
			}else{
				$json['reset'] = 0;
			}	
			if($live_sales['loop']){
				$json['loop'] = 1;
			}else{
				$json['loop'] = 0;				
			}	
			$json['content'] = $html;
		}
		
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));		
		
	}
	
	private function time_elapsed_string($datetime, $full = false) {
		$now = new DateTime;
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}

		if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
}