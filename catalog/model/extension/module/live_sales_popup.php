<?php 
class ModelExtensionModuleLiveSalesPopup extends Model {

	public function getliveOrder($order_id){
		$order_query = $this->db->query("SELECT *, (SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'customer'                => $order_query->row['customer'],
				'customer_group_id'       => $order_query->row['customer_group_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'email'                   => $order_query->row['email'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'total'                   => $order_query->row['total'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'order_status'            => $order_query->row['order_status'],
				'date_added'              => $order_query->row['date_added'],
				'date_modified'           => $order_query->row['date_modified'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'           => $order_query->row['currency_value']
			);
		} else {
			return;
		}		
	}	
	
  	public function getliveOrders() {
		$sql = "SELECT o.order_id, CONCAT(o.firstname, ' ', o.lastname) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS order_status, o.shipping_code, o.total, o.currency_code, o.currency_value, o.date_added, o.date_modified FROM `" . DB_PREFIX . "order` o";

		if (isset($data['filter_order_status'])) {
			$implode = array();

			$order_statuses = explode(',', $data['filter_order_status']);

			foreach ($order_statuses as $order_status_id) {
				$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
			}

			if ($implode) {
				$sql .= " WHERE (" . implode(" OR ", $implode) . ")";
			}
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND o.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$sql .= " AND CONCAT(o.firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_date_modified'])) {
			$sql .= " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
		}

		if (!empty($data['filter_total'])) {
			$sql .= " AND o.total = '" . (float)$data['filter_total'] . "'";
		}

		$sort_data = array(
			'o.order_id',
			'customer',
			'status',
			'o.date_added',
			'o.date_modified',
			'o.total'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY o.order_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		
		/*foreach($query->rows as $result){
			$order_data[] = array(
				'order_id' => $result['order_id'],
				'customer' => $result['customer'],
				'date_added' => $result['date_added'],
				'total' => $result['total'],
				'customer' => $result['customer'],
			);	
		}
		return $order_data;	*/	
		return $query->rows; 	
	}
	
	public function getOrderProducts(){
		
		$live_sales = $this->config->get('module_live_sales_popup');
		
		/*$sql = "SELECT * FROM " . DB_PREFIX . "order_product op";
		
		if($live_sales['active_product']){
			$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON(op.product_id = p.product_id) WHERE p.status = '1'";
		}
		
		
		if (isset($live_sales['sequence']) && ($live_sales['sequence'] == 'rand')) {
			$sql .= " ORDER BY RAND()";
		}elseif(isset($live_sales['sequence']) && ($live_sales['sequence'] == 'desc')) {
			$sql .= " ORDER BY op.order_id DESC";
		}else{
			$sql .= " ORDER BY op.order_id ASC";			
		}*/
		//, (SELECT total, order_status_id FROM " . DB_PREFIX . "order o WHERE op.order_id = o.order_id) as total
		$sql = "SELECT op.*, o.total,o.order_status_id,p.image FROM " . DB_PREFIX . "order_product op";
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "order o ON(op.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "product p ON(op.product_id = p.product_id)";
		
		if($live_sales['active_product']){
			$sql .= "  WHERE p.status = '1'";
		}	
		
		if (isset($live_sales['sequence']) && ($live_sales['sequence'] == 'rand')) {
			$sql .= " ORDER BY RAND()";
		}elseif(isset($live_sales['sequence']) && ($live_sales['sequence'] == 'desc')) {
			$sql .= " ORDER BY op.order_id DESC";
		}else{
			$sql .= " ORDER BY op.order_id ASC";			
		}
		
		//echo $sql;
		$query = $this->db->query($sql);
		
		$order_data = array();
		$i=0;
		
		foreach($query->rows as $order){
			if(($live_sales['min_amount'] == 0 ) || ($order['total'] >= $live_sales['min_amount'])){
				if(in_array($order['order_status_id'], $live_sales['order_status'])){
				$i++;	
					$order_data[$i] = array(
						'order_id' => $order['order_id'],
						'image'	=>  $order['image'],
						'product_id' => $order['product_id'],
						'name'	=> $order['name'],
						'total'	=> $order['total']
					);
				}	
			}	
		}

		return $order_data;
	}	
}