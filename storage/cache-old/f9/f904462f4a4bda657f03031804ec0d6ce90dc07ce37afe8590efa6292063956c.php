<?php

/* tt_naturecircle1/template/blog/blog_home.twig */
class __TwigTemplate_8f3d4422aaa6638021c35d32fd4e5cc3122dd2dcc367f5646e331dd4c5758e97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"blog_home\" class=\"menu-recent \">
\t<div>
\t\t<div class=\"blog-title module-title\">
\t\t   <h2>
\t\t\t";
        // line 5
        if ((isset($context["text_store1"]) ? $context["text_store1"] : null)) {
            // line 6
            echo "\t\t\t  <span>";
            echo (isset($context["text_store1"]) ? $context["text_store1"] : null);
            echo "</span>
\t\t\t";
        }
        // line 8
        echo "\t\t\t";
        echo (isset($context["text_headingtitle"]) ? $context["text_headingtitle"] : null);
        echo "
\t\t   </h2>
\t\t</div>
\t \t";
        // line 11
        list($context["count"], $context["rows"]) =         array(0, $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "rows", array()));
        // line 12
        echo "\t\t";
        if ( !(isset($context["rows"]) ? $context["rows"] : null)) {
            // line 13
            echo "\t\t\t";
            $context["rows"] = 1;
            // line 14
            echo "\t\t";
        }
        // line 15
        echo "
\t\t";
        // line 16
        if ((isset($context["articles"]) ? $context["articles"] : null)) {
            // line 17
            echo "\t\t\t<div class=\"owl-container\">
\t\t\t<div class=\"articles-container owl-carousel owl-theme\">
\t\t\t";
            // line 19
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["articles"]) ? $context["articles"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
                // line 20
                echo "\t\t\t\t";
                if ((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0)) {
                    // line 21
                    echo "\t\t\t\t<div class=\"row_items\">
\t\t\t\t";
                }
                // line 23
                echo "\t\t\t\t";
                $context["count"] = ((isset($context["count"]) ? $context["count"] : null) + 1);
                // line 24
                echo "\t\t\t\t\t<div class=\"articles-inner \">
\t\t\t\t\t\t<div class=\"articles-image\">
\t\t\t\t\t\t\t<a href=\"";
                // line 26
                echo $this->getAttribute($context["article"], "href", array());
                echo "\"><img src=\"";
                echo $this->getAttribute($context["article"], "image", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["article"], "name", array());
                echo "\"/></a>
\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"aritcles-content\">
\t\t\t\t\t\t\t<div class=\"content-inner\">
\t\t\t\t\t\t\t\t<a class=\"articles-name\" href=\"";
                // line 31
                echo $this->getAttribute($context["article"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["article"], "name", array());
                echo "</a>
\t\t\t\t\t\t\t\t<div class=\"articles-date\">
\t\t\t\t\t\t\t\t\t";
                // line 33
                if ($this->getAttribute($context["article"], "author", array())) {
                    // line 34
                    echo "\t\t\t\t\t\t\t\t\t\t<span class=\"author-name\">";
                    echo (isset($context["text_post_by"]) ? $context["text_post_by"] : null);
                    echo "<span>";
                    echo $this->getAttribute($context["article"], "author", array());
                    echo "</span></span>
\t\t\t\t\t\t\t\t\t";
                }
                // line 36
                echo "\t\t\t\t\t\t\t\t\t<span class=\"artice-day\">";
                echo (((((" - " . $this->getAttribute($context["article"], "date_added_m", array())) . " ") . $this->getAttribute($context["article"], "date_added_d", array())) . ",") . $this->getAttribute($context["article"], "date_added_y", array()));
                echo "</span>
\t\t\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t\t\t<div class=\"articles-intro\">";
                // line 38
                echo $this->getAttribute($context["article"], "intro_text", array());
                echo "</div>
\t\t\t\t\t\t\t\t<a class=\"read-more\" href=\"";
                // line 39
                echo $this->getAttribute($context["article"], "href", array());
                echo "\">";
                echo (isset($context["button_read_more"]) ? $context["button_read_more"] : null);
                echo "</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t";
                // line 43
                if (((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0) || ((isset($context["count"]) ? $context["count"] : null) == twig_length_filter($this->env, (isset($context["articles"]) ? $context["articles"] : null))))) {
                    // line 44
                    echo "\t\t\t\t</div>
\t\t\t\t";
                }
                // line 46
                echo "\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "\t\t\t</div>
\t\t\t</div>
\t\t";
        } else {
            // line 50
            echo "\t\t\t<p>";
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
\t\t";
        }
        // line 52
        echo "\t</div>

</div>
<script >
    \$(document).ready(function() {
        \$(\".articles-container\").owlCarousel({
            autoPlay : ";
        // line 58
        if ($this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "auto", array())) {
            echo " true ";
        } else {
            echo " false ";
        }
        echo ",
        \titems : ";
        // line 59
        echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "items", array(), "array");
        echo ",
\t\t\tmargin: 0,
\t\t\tloop: true,\t\t\t
\t\t\tnavSpeed : ";
        // line 62
        echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "speed", array(), "array");
        echo ",
\t\t\tdotsSpeed : ";
        // line 63
        echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "speed", array(), "array");
        echo ",
\t\t\tautoplaySpeed : ";
        // line 64
        echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "speed", array(), "array");
        echo ",
\t\t\tnav : ";
        // line 65
        if ($this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "navigation", array())) {
            echo " true ";
        } else {
            echo " false ";
        }
        echo ",
\t\t\tdots : ";
        // line 66
        if ($this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "pagination", array())) {
            echo " true ";
        } else {
            echo " false ";
        }
        echo ",
\t\t\tnavText : ['<i class=\"ion-chevron-left\"></i>','<i class=\"ion-chevron-right\"></i>'],
\t\t\tresponsive:{
\t\t\t\t0:{
\t\t\t\t\titems: 1,
\t\t\t\t\tnav: false
\t\t\t\t},
\t\t\t\t480:{
\t\t\t\t\titems: 2,
\t\t\t\t\tnav: false
\t\t\t\t},
\t\t\t\t768:{
\t\t\t\t\titems: 2
\t\t\t\t},
\t\t\t\t992:{
\t\t\t\t\titems: 3
\t\t\t\t},
\t\t\t\t1200:{
\t\t\t\t\titems: ";
        // line 84
        echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "items", array(), "array");
        echo "
\t\t\t\t}
\t\t\t}
    \t});
    });
</script>";
    }

    public function getTemplateName()
    {
        return "tt_naturecircle1/template/blog/blog_home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 84,  191 => 66,  183 => 65,  179 => 64,  175 => 63,  171 => 62,  165 => 59,  157 => 58,  149 => 52,  143 => 50,  138 => 47,  132 => 46,  128 => 44,  126 => 43,  117 => 39,  113 => 38,  107 => 36,  99 => 34,  97 => 33,  90 => 31,  78 => 26,  74 => 24,  71 => 23,  67 => 21,  64 => 20,  60 => 19,  56 => 17,  54 => 16,  51 => 15,  48 => 14,  45 => 13,  42 => 12,  40 => 11,  33 => 8,  27 => 6,  25 => 5,  19 => 1,);
    }
}
/* <div id="blog_home" class="menu-recent ">*/
/* 	<div>*/
/* 		<div class="blog-title module-title">*/
/* 		   <h2>*/
/* 			{% if text_store1 %}*/
/* 			  <span>{{ text_store1 }}</span>*/
/* 			{% endif %}*/
/* 			{{ text_headingtitle }}*/
/* 		   </h2>*/
/* 		</div>*/
/* 	 	{% set count, rows = 0, slide.rows %}*/
/* 		{% if not rows %}*/
/* 			{% set rows = 1 %}*/
/* 		{% endif %}*/
/* */
/* 		{% if articles %}*/
/* 			<div class="owl-container">*/
/* 			<div class="articles-container owl-carousel owl-theme">*/
/* 			{% for article in articles %}*/
/* 				{% if count % rows == 0 %}*/
/* 				<div class="row_items">*/
/* 				{% endif %}*/
/* 				{% set count = count + 1 %}*/
/* 					<div class="articles-inner ">*/
/* 						<div class="articles-image">*/
/* 							<a href="{{ article.href }}"><img src="{{ article.image }}" alt="{{ article.name }}"/></a>*/
/* 							*/
/* 						</div>*/
/* 						<div class="aritcles-content">*/
/* 							<div class="content-inner">*/
/* 								<a class="articles-name" href="{{ article.href }}">{{ article.name }}</a>*/
/* 								<div class="articles-date">*/
/* 									{% if article.author %}*/
/* 										<span class="author-name">{{ text_post_by }}<span>{{ article.author }}</span></span>*/
/* 									{% endif %}*/
/* 									<span class="artice-day">{{ ' - ' ~ article.date_added_m ~ ' ' ~ article.date_added_d ~ ',' ~ article.date_added_y }}</span>*/
/* 								</div>		*/
/* 								<div class="articles-intro">{{ article.intro_text }}</div>*/
/* 								<a class="read-more" href="{{ article.href }}">{{ button_read_more }}</a>*/
/* 							</div>*/
/* 						</div>												*/
/* 					</div>*/
/* 				{% if (count % rows == 0) or (count == articles|length ) %}*/
/* 				</div>*/
/* 				{% endif %}*/
/* 			{% endfor %}*/
/* 			</div>*/
/* 			</div>*/
/* 		{% else %}*/
/* 			<p>{{ text_empty }}</p>*/
/* 		{% endif %}*/
/* 	</div>*/
/* */
/* </div>*/
/* <script >*/
/*     $(document).ready(function() {*/
/*         $(".articles-container").owlCarousel({*/
/*             autoPlay : {% if slide.auto %} true {% else %} false {% endif %},*/
/*         	items : {{ slide['items'] }},*/
/* 			margin: 0,*/
/* 			loop: true,			*/
/* 			navSpeed : {{ slide['speed'] }},*/
/* 			dotsSpeed : {{ slide['speed'] }},*/
/* 			autoplaySpeed : {{ slide['speed'] }},*/
/* 			nav : {% if slide.navigation %} true {% else %} false {% endif %},*/
/* 			dots : {% if slide.pagination %} true {% else %} false {% endif %},*/
/* 			navText : ['<i class="ion-chevron-left"></i>','<i class="ion-chevron-right"></i>'],*/
/* 			responsive:{*/
/* 				0:{*/
/* 					items: 1,*/
/* 					nav: false*/
/* 				},*/
/* 				480:{*/
/* 					items: 2,*/
/* 					nav: false*/
/* 				},*/
/* 				768:{*/
/* 					items: 2*/
/* 				},*/
/* 				992:{*/
/* 					items: 3*/
/* 				},*/
/* 				1200:{*/
/* 					items: {{ slide['items'] }}*/
/* 				}*/
/* 			}*/
/*     	});*/
/*     });*/
/* </script>*/
