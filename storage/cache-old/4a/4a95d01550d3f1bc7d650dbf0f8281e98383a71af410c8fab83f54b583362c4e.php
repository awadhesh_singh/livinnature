<?php

/* tt_naturecircle1/template/extension/module/octestimonial.twig */
class __TwigTemplate_aaf9f0a0c6e1613331c6527a2a0d5ded9380ffa5c05f05e2eb25aa23f468accb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"testimonial-container \">
<div class=\"container-inner\">
\t<div class=\"module-title\">
\t\t<h2>
\t\t\t";
        // line 5
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "
\t\t</h2>
\t</div>
\t";
        // line 8
        $context["count"] = 0;
        // line 9
        echo "\t";
        if ((isset($context["rows"]) ? $context["rows"] : null)) {
            echo "\t\t
\t\t";
            // line 10
            $context["rows"] = $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "rows", array());
            // line 11
            echo "\t";
        } else {
            // line 12
            echo "\t\t";
            $context["rows"] = 1;
            // line 13
            echo "\t";
        }
        // line 14
        echo "\t";
        if ((isset($context["testimonials"]) ? $context["testimonials"] : null)) {
            // line 15
            echo "\t\t<div class=\"block-content\">
\t\t\t<div id=\"slides\" class=\"owl-carousel owl-theme\">
\t\t\t\t";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["testimonials"]) ? $context["testimonials"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["testimonial"]) {
                // line 18
                echo "\t\t\t\t\t";
                if ((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0)) {
                    // line 19
                    echo "\t\t\t\t\t<div class=\"row_items\">
\t\t\t\t\t";
                }
                // line 21
                echo "\t\t\t\t\t";
                $context["count"] = ((isset($context["count"]) ? $context["count"] : null) + 1);
                // line 22
                echo "\t\t\t\t\t<div class=\"testimonial-content\">
\t\t\t\t\t\t<div class=\"testimonial-box\">\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<a href=\"";
                // line 24
                echo (isset($context["more"]) ? $context["more"] : null);
                echo "\">";
                echo $this->getAttribute($context["testimonial"], "content", array());
                echo "</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t</div><!--testimonial-content-->
\t\t\t\t\t";
                // line 29
                if (((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0) || ((isset($context["count"]) ? $context["count"] : null) == twig_length_filter($this->env, (isset($context["testimonials"]) ? $context["testimonials"] : null))))) {
                    // line 30
                    echo "\t\t\t\t\t\t</div>
\t\t\t\t\t";
                }
                // line 32
                echo "\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['testimonial'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "\t\t\t</div>
\t\t\t<ul class=\"thumb owl-carousel owl-theme\" id=\"sync2\">
\t\t\t\t";
            // line 35
            list($context["i"], $context["j"]) =             array(0, 0);
            // line 36
            echo "\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["testimonials"]) ? $context["testimonials"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["testimonial"]) {
                // line 37
                echo "\t\t\t\t\t";
                if ($this->getAttribute($context["testimonial"], "content", array())) {
                    // line 38
                    echo "\t\t\t\t\t<li class=\"testithumb";
                    echo (isset($context["j"]) ? $context["j"] : null);
                    echo " ";
                    echo ((((isset($context["j"]) ? $context["j"] : null) == 0)) ? ("active") : (""));
                    echo "\" onclick=\"\">
\t\t\t\t\t\t";
                    // line 39
                    $context["j"] = ((isset($context["j"]) ? $context["j"] : null) + 1);
                    // line 40
                    echo "\t\t\t\t\t\t";
                    if ($this->getAttribute($context["testimonial"], "image", array())) {
                        // line 41
                        echo "\t\t\t\t\t\t<img src=\"";
                        echo $this->getAttribute($context["testimonial"], "image", array());
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["testimonial"], "customer_name", array());
                        echo "\">
\t\t\t\t\t\t";
                    }
                    // line 43
                    echo "\t\t\t\t\t\t<span class=\"testimonial-author\">";
                    echo $this->getAttribute($context["testimonial"], "customer_name", array());
                    echo "</span>
\t\t\t\t\t</li>
\t\t\t\t\t";
                }
                // line 46
                echo "\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['testimonial'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "\t\t\t</ul>
\t\t</div><!--block-content-->
\t";
        } else {
            // line 50
            echo "\t\t<p class=\"text_empty\">";
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
\t";
        }
        // line 52
        echo "</div>
</div><!--testimonial-container-->\t\t\t\t\t
<script >
\$(document).ready(function() {
\tvar sync1 = \$(\"#slides\");
\tvar sync2 = \$(\"#sync2\");
\tvar syncedSecondary = true;
\t
    sync1.owlCarousel({
\t\tautoPlay : ";
        // line 61
        if ($this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "auto", array(), "array")) {
            echo " ";
            echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "auto", array(), "array");
            echo " ";
        } else {
            echo " false ";
        }
        echo ",
\t\titems : ";
        // line 62
        echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "items", array(), "array");
        echo ",\t
\t\tloop: true,
\t\tmargin: 30,
\t\tnavSpeed : ";
        // line 65
        echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "speed", array(), "array");
        echo ",
\t\tdotsSpeed : ";
        // line 66
        echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "speed", array(), "array");
        echo ",
\t\tautoplaySpeed : ";
        // line 67
        echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "speed", array(), "array");
        echo ",
\t\tnav : ";
        // line 68
        if ($this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "navigation", array(), "array")) {
            echo " true  ";
        } else {
            echo " false ";
        }
        echo ",
\t\tdots :";
        // line 69
        if ($this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "pagination", array(), "array")) {
            echo " true ";
        } else {
            echo " false ";
        }
        echo ",
\t\tlazyLoad: true,
\t\tnavText : ['<i class=\"ion-ios-arrow-left\"></i>','<i class=\"ion-ios-arrow-right\"></i>'],
\t\tresponsive:{
\t\t\t0:{
\t\t\t\titems: 1,
\t\t\t\tnav: false
\t\t\t},
\t\t\t480:{
\t\t\t\titems: 1,
\t\t\t\tnav: false
\t\t\t},
\t\t\t768:{
\t\t\t\titems: 1
\t\t\t},
\t\t\t980:{
\t\t\t\titems: 1
\t\t\t}
\t\t}
    }).on('changed.owl.carousel', syncPosition);
\t
\tsync2.on('initialized.owl.carousel', function () {
      sync2.find(\".owl-item\").eq(0).addClass(\"current\");
    })
\t.owlCarousel({
\t\tautoPlay : ";
        // line 94
        if ($this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "auto", array(), "array")) {
            echo " ";
            echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "auto", array(), "array");
            echo " ";
        } else {
            echo " false ";
        }
        echo ",
\t\tmargin: 0,
\t\tnavSpeed : ";
        // line 96
        echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "speed", array(), "array");
        echo ",
\t\tdotsSpeed : ";
        // line 97
        echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "speed", array(), "array");
        echo ",
\t\tautoplaySpeed : ";
        // line 98
        echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "speed", array(), "array");
        echo ",
\t\tnav : false,
\t\tdots :false,
\t\tlazyLoad: true,
\t\tnavText : ['<i class=\"ion-ios-arrow-left\"></i>','<i class=\"ion-ios-arrow-right\"></i>'],
\t\tresponsive:{
\t\t\t0:{
\t\t\t\titems: 2,
\t\t\t\tnav: false
\t\t\t},
\t\t\t480:{
\t\t\t\titems: 3,
\t\t\t\tnav: false
\t\t\t},
\t\t\t768:{
\t\t\t\titems: 3
\t\t\t},
\t\t\t992:{
\t\t\t\titems: 3
\t\t\t},
\t\t\t1200:{
\t\t\t\titems: 3
\t\t\t}
\t\t}
    }).on('changed.owl.carousel', syncPosition2);
\t
\tfunction syncPosition(el) {
    //if you set loop to false, you have to restore this next line
    //var current = el.item.index;
    
    //if you disable loop you have to comment this block
    var count = el.item.count-1;
    var current = Math.round(el.item.index - (el.item.count/2) - .5);
    
    if(current < 0) {
      current = count;
    }
    if(current > count) {
      current = 0;
    }
    
    //end block

    sync2
      .find(\".owl-item\")
      .removeClass(\"current\")
      .eq(current)
      .addClass(\"current\");
    var onscreen = sync2.find('.owl-item.active').length - 1;
    var start = sync2.find('.owl-item.active').first().index();
    var end = sync2.find('.owl-item.active').last().index();
    
    if (current > end) {
      sync2.data('owl.carousel').to(current, 100, true);
    }
    if (current < start) {
      sync2.data('owl.carousel').to(current - onscreen, 100, true);
    }
  }
  
  function syncPosition2(el) {
    if(syncedSecondary) {
      var number = el.item.index;
      sync1.data('owl.carousel').to(number, 100, true);
    }
  }
  
  sync2.on(\"click\", \".owl-item\", function(e){
    e.preventDefault();
    var number = \$(this).index();
    sync1.data('owl.carousel').to(number, 300, true);
  });
});
</script>";
    }

    public function getTemplateName()
    {
        return "tt_naturecircle1/template/extension/module/octestimonial.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  252 => 98,  248 => 97,  244 => 96,  233 => 94,  201 => 69,  193 => 68,  189 => 67,  185 => 66,  181 => 65,  175 => 62,  165 => 61,  154 => 52,  148 => 50,  143 => 47,  137 => 46,  130 => 43,  122 => 41,  119 => 40,  117 => 39,  110 => 38,  107 => 37,  102 => 36,  100 => 35,  96 => 33,  90 => 32,  86 => 30,  84 => 29,  74 => 24,  70 => 22,  67 => 21,  63 => 19,  60 => 18,  56 => 17,  52 => 15,  49 => 14,  46 => 13,  43 => 12,  40 => 11,  38 => 10,  33 => 9,  31 => 8,  25 => 5,  19 => 1,);
    }
}
/* <div class="testimonial-container ">*/
/* <div class="container-inner">*/
/* 	<div class="module-title">*/
/* 		<h2>*/
/* 			{{ title }}*/
/* 		</h2>*/
/* 	</div>*/
/* 	{% set count = 0 %}*/
/* 	{% if rows %}		*/
/* 		{% set rows = slide.rows %}*/
/* 	{% else %}*/
/* 		{% set rows = 1 %}*/
/* 	{% endif %}*/
/* 	{% if testimonials %}*/
/* 		<div class="block-content">*/
/* 			<div id="slides" class="owl-carousel owl-theme">*/
/* 				{% for testimonial in testimonials %}*/
/* 					{% if count % rows == 0 %}*/
/* 					<div class="row_items">*/
/* 					{% endif %}*/
/* 					{% set count = count + 1 %}*/
/* 					<div class="testimonial-content">*/
/* 						<div class="testimonial-box">							*/
/* 							<a href="{{ more }}">{{ testimonial.content }}</a>*/
/* 						</div>*/
/* 						*/
/* 						*/
/* 					</div><!--testimonial-content-->*/
/* 					{% if count % rows == 0 or count == testimonials|length %}*/
/* 						</div>*/
/* 					{% endif %}*/
/* 				{% endfor %}*/
/* 			</div>*/
/* 			<ul class="thumb owl-carousel owl-theme" id="sync2">*/
/* 				{% set i, j = 0, 0 %}*/
/* 				{% for testimonial in testimonials %}*/
/* 					{% if testimonial.content %}*/
/* 					<li class="testithumb{{ j }} {{ (j == 0) ? 'active' : '' }}" onclick="">*/
/* 						{% set j = j + 1 %}*/
/* 						{% if testimonial.image %}*/
/* 						<img src="{{ testimonial.image }}" alt="{{ testimonial.customer_name }}">*/
/* 						{% endif %}*/
/* 						<span class="testimonial-author">{{ testimonial.customer_name }}</span>*/
/* 					</li>*/
/* 					{% endif %}*/
/* 				{% endfor %}*/
/* 			</ul>*/
/* 		</div><!--block-content-->*/
/* 	{% else %}*/
/* 		<p class="text_empty">{{ text_empty }}</p>*/
/* 	{% endif %}*/
/* </div>*/
/* </div><!--testimonial-container-->					*/
/* <script >*/
/* $(document).ready(function() {*/
/* 	var sync1 = $("#slides");*/
/* 	var sync2 = $("#sync2");*/
/* 	var syncedSecondary = true;*/
/* 	*/
/*     sync1.owlCarousel({*/
/* 		autoPlay : {% if slide['auto'] %} {{ slide['auto'] }} {% else %} false {% endif %},*/
/* 		items : {{ slide['items'] }},	*/
/* 		loop: true,*/
/* 		margin: 30,*/
/* 		navSpeed : {{ slide['speed'] }},*/
/* 		dotsSpeed : {{ slide['speed'] }},*/
/* 		autoplaySpeed : {{ slide['speed'] }},*/
/* 		nav : {% if slide['navigation'] %} true  {% else %} false {% endif %},*/
/* 		dots :{% if slide['pagination'] %} true {% else %} false {% endif %},*/
/* 		lazyLoad: true,*/
/* 		navText : ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],*/
/* 		responsive:{*/
/* 			0:{*/
/* 				items: 1,*/
/* 				nav: false*/
/* 			},*/
/* 			480:{*/
/* 				items: 1,*/
/* 				nav: false*/
/* 			},*/
/* 			768:{*/
/* 				items: 1*/
/* 			},*/
/* 			980:{*/
/* 				items: 1*/
/* 			}*/
/* 		}*/
/*     }).on('changed.owl.carousel', syncPosition);*/
/* 	*/
/* 	sync2.on('initialized.owl.carousel', function () {*/
/*       sync2.find(".owl-item").eq(0).addClass("current");*/
/*     })*/
/* 	.owlCarousel({*/
/* 		autoPlay : {% if slide['auto'] %} {{ slide['auto'] }} {% else %} false {% endif %},*/
/* 		margin: 0,*/
/* 		navSpeed : {{ slide['speed'] }},*/
/* 		dotsSpeed : {{ slide['speed'] }},*/
/* 		autoplaySpeed : {{ slide['speed'] }},*/
/* 		nav : false,*/
/* 		dots :false,*/
/* 		lazyLoad: true,*/
/* 		navText : ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],*/
/* 		responsive:{*/
/* 			0:{*/
/* 				items: 2,*/
/* 				nav: false*/
/* 			},*/
/* 			480:{*/
/* 				items: 3,*/
/* 				nav: false*/
/* 			},*/
/* 			768:{*/
/* 				items: 3*/
/* 			},*/
/* 			992:{*/
/* 				items: 3*/
/* 			},*/
/* 			1200:{*/
/* 				items: 3*/
/* 			}*/
/* 		}*/
/*     }).on('changed.owl.carousel', syncPosition2);*/
/* 	*/
/* 	function syncPosition(el) {*/
/*     //if you set loop to false, you have to restore this next line*/
/*     //var current = el.item.index;*/
/*     */
/*     //if you disable loop you have to comment this block*/
/*     var count = el.item.count-1;*/
/*     var current = Math.round(el.item.index - (el.item.count/2) - .5);*/
/*     */
/*     if(current < 0) {*/
/*       current = count;*/
/*     }*/
/*     if(current > count) {*/
/*       current = 0;*/
/*     }*/
/*     */
/*     //end block*/
/* */
/*     sync2*/
/*       .find(".owl-item")*/
/*       .removeClass("current")*/
/*       .eq(current)*/
/*       .addClass("current");*/
/*     var onscreen = sync2.find('.owl-item.active').length - 1;*/
/*     var start = sync2.find('.owl-item.active').first().index();*/
/*     var end = sync2.find('.owl-item.active').last().index();*/
/*     */
/*     if (current > end) {*/
/*       sync2.data('owl.carousel').to(current, 100, true);*/
/*     }*/
/*     if (current < start) {*/
/*       sync2.data('owl.carousel').to(current - onscreen, 100, true);*/
/*     }*/
/*   }*/
/*   */
/*   function syncPosition2(el) {*/
/*     if(syncedSecondary) {*/
/*       var number = el.item.index;*/
/*       sync1.data('owl.carousel').to(number, 100, true);*/
/*     }*/
/*   }*/
/*   */
/*   sync2.on("click", ".owl-item", function(e){*/
/*     e.preventDefault();*/
/*     var number = $(this).index();*/
/*     sync1.data('owl.carousel').to(number, 300, true);*/
/*   });*/
/* });*/
/* </script>*/
