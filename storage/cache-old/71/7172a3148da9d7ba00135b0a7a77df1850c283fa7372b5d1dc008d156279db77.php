<?php

/* tt_naturecircle1/template/extension/module/ocinstagram.twig */
class __TwigTemplate_ccce3ebce244f1ee72ff1a9ba20ff2237fed350aa60c222638df8be6485a7b5c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"instagram_block_home\" class=\"block\">
\t<div class=\"title_block footer-title\">
\t\t<h3 >";
        // line 3
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h3>
\t</div>
\t";
        // line 5
        if (((isset($context["error_connect"]) ? $context["error_connect"] : null) == false)) {
            // line 6
            echo "\t\t<p class=\"text_error_instagram\">";
            echo (isset($context["text_error"]) ? $context["text_error"] : null);
            echo "</p>
\t";
        } else {
            // line 8
            echo "        ";
            list($context["count"], $context["rows"]) =             array(0, $this->getAttribute((isset($context["config_slide"]) ? $context["config_slide"] : null), "f_rows", array()));
            // line 9
            echo "        ";
            if ( !(isset($context["rows"]) ? $context["rows"] : null)) {
                // line 10
                echo "            ";
                $context["rows"] = 1;
                // line 11
                echo "        ";
            }
            // line 12
            echo "            <div class=\"content_block owl-carousel owl-theme\">
                ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["instagrams"]) ? $context["instagrams"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["instagram"]) {
                // line 14
                echo "                    ";
                if ((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0)) {
                    // line 15
                    echo "                        <div class=\"row_items\">
                    ";
                }
                // line 17
                echo "                    ";
                $context["count"] = ((isset($context["count"]) ? $context["count"] : null) + 1);
                // line 18
                echo "                            <a class=\"fancybox ";
                if (($this->getAttribute((isset($context["config_slide"]) ? $context["config_slide"] : null), "f_view_mode", array()) != "slider")) {
                    echo " col-xs-4 ";
                }
                echo "\" href=\"";
                echo $this->getAttribute($context["instagram"], "image", array());
                echo "\" style=\"display: block;\"><img src=\"";
                echo $this->getAttribute($context["instagram"], "image", array());
                echo "\" alt=\"\" /></a>
                    ";
                // line 19
                if ((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0)) {
                    // line 20
                    echo "                        </div>
                    ";
                } else {
                    // line 22
                    echo "                        ";
                    if (((isset($context["count"]) ? $context["count"] : null) == twig_length_filter($this->env, (isset($context["instagrams"]) ? $context["instagrams"] : null)))) {
                        // line 23
                        echo "                        </div>
                        ";
                    }
                    // line 25
                    echo "                    ";
                }
                // line 26
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['instagram'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "        </div>
        ";
            // line 28
            if (($this->getAttribute((isset($context["config_slide"]) ? $context["config_slide"] : null), "f_view_mode", array()) == "slider")) {
                // line 29
                echo "        <script >
            \$(\"#instagram_block_home .content_block\").owlCarousel({
                autoPlay: ";
                // line 31
                if ($this->getAttribute((isset($context["config_slide"]) ? $context["config_slide"] : null), "autoplay", array())) {
                    echo " true ";
                } else {
                    echo " false ";
                }
                echo ",
                navSpeed : ";
                // line 32
                if ($this->getAttribute((isset($context["config_slide"]) ? $context["config_slide"] : null), "f_speed", array())) {
                    echo " ";
                    echo $this->getAttribute((isset($context["config_slide"]) ? $context["config_slide"] : null), "f_speed", array());
                    echo " ";
                } else {
                    echo " 3000 ";
                }
                echo ",
                nav : ";
                // line 33
                if ($this->getAttribute((isset($context["config_slide"]) ? $context["config_slide"] : null), "f_show_nextback", array())) {
                    echo " true ";
                } else {
                    echo " false ";
                }
                echo ",
                dots : ";
                // line 34
                if ($this->getAttribute((isset($context["config_slide"]) ? $context["config_slide"] : null), "f_show_ctr", array())) {
                    echo " true ";
                } else {
                    echo " false ";
                }
                echo ",
                autoplayHoverPause : true,
\t\t\t\tmargin: 10,
                responsive:{
\t\t\t\t\t0:{
\t\t\t\t\t\titems: 2
\t\t\t\t\t},
\t\t\t\t\t480:{
\t\t\t\t\t\titems: 3
\t\t\t\t\t},
\t\t\t\t\t768:{
\t\t\t\t\t\titems: 3
\t\t\t\t\t},
\t\t\t\t\t992:{
\t\t\t\t\t\titems: 3
\t\t\t\t\t},
\t\t\t\t\t1200:{
\t\t\t\t\t\titems: ";
                // line 51
                echo $this->getAttribute((isset($context["config_slide"]) ? $context["config_slide"] : null), "items", array());
                echo "
\t\t\t\t\t},
                }
            });
        </script>
        ";
            }
            // line 57
            echo "        <script >
            \$('.content_block').magnificPopup({
                type: 'image',
                delegate: 'a',
                gallery: {
                    enabled : true
                }
            });
        </script>
    ";
        }
        // line 67
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "tt_naturecircle1/template/extension/module/ocinstagram.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 67,  166 => 57,  157 => 51,  133 => 34,  125 => 33,  115 => 32,  107 => 31,  103 => 29,  101 => 28,  98 => 27,  92 => 26,  89 => 25,  85 => 23,  82 => 22,  78 => 20,  76 => 19,  65 => 18,  62 => 17,  58 => 15,  55 => 14,  51 => 13,  48 => 12,  45 => 11,  42 => 10,  39 => 9,  36 => 8,  30 => 6,  28 => 5,  23 => 3,  19 => 1,);
    }
}
/* <div id="instagram_block_home" class="block">*/
/* 	<div class="title_block footer-title">*/
/* 		<h3 >{{ heading_title }}</h3>*/
/* 	</div>*/
/* 	{% if error_connect == false %}*/
/* 		<p class="text_error_instagram">{{ text_error }}</p>*/
/* 	{% else %}*/
/*         {% set count, rows = 0, config_slide.f_rows %}*/
/*         {% if not rows %}*/
/*             {% set rows = 1 %}*/
/*         {% endif %}*/
/*             <div class="content_block owl-carousel owl-theme">*/
/*                 {% for instagram in instagrams %}*/
/*                     {% if count % rows == 0 %}*/
/*                         <div class="row_items">*/
/*                     {% endif %}*/
/*                     {% set count = count + 1 %}*/
/*                             <a class="fancybox {% if config_slide.f_view_mode != 'slider' %} col-xs-4 {% endif %}" href="{{ instagram.image }}" style="display: block;"><img src="{{ instagram.image }}" alt="" /></a>*/
/*                     {% if count % rows == 0 %}*/
/*                         </div>*/
/*                     {% else %}*/
/*                         {% if count == instagrams|length %}*/
/*                         </div>*/
/*                         {% endif %}*/
/*                     {% endif %}*/
/*                 {% endfor %}*/
/*         </div>*/
/*         {% if config_slide.f_view_mode == 'slider' %}*/
/*         <script >*/
/*             $("#instagram_block_home .content_block").owlCarousel({*/
/*                 autoPlay: {% if config_slide.autoplay %} true {% else %} false {% endif %},*/
/*                 navSpeed : {% if config_slide.f_speed %} {{ config_slide.f_speed }} {% else %} 3000 {% endif %},*/
/*                 nav : {% if config_slide.f_show_nextback %} true {% else %} false {% endif %},*/
/*                 dots : {% if config_slide.f_show_ctr %} true {% else %} false {% endif %},*/
/*                 autoplayHoverPause : true,*/
/* 				margin: 10,*/
/*                 responsive:{*/
/* 					0:{*/
/* 						items: 2*/
/* 					},*/
/* 					480:{*/
/* 						items: 3*/
/* 					},*/
/* 					768:{*/
/* 						items: 3*/
/* 					},*/
/* 					992:{*/
/* 						items: 3*/
/* 					},*/
/* 					1200:{*/
/* 						items: {{ config_slide.items }}*/
/* 					},*/
/*                 }*/
/*             });*/
/*         </script>*/
/*         {% endif %}*/
/*         <script >*/
/*             $('.content_block').magnificPopup({*/
/*                 type: 'image',*/
/*                 delegate: 'a',*/
/*                 gallery: {*/
/*                     enabled : true*/
/*                 }*/
/*             });*/
/*         </script>*/
/*     {% endif %}*/
/* </div>*/
