<?php

/* tt_naturecircle1/template/common/footer.twig */
class __TwigTemplate_67dc7d73e49bd0d9364e24d2613da40fe4dd8a6ee3bae63ada7e32292466507f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer>
\t<div class=\"container\">
\t\t";
        // line 3
        if ((isset($context["block4"]) ? $context["block4"] : null)) {
            // line 4
            echo "\t\t\t";
            echo (isset($context["block4"]) ? $context["block4"] : null);
            echo "
\t\t";
        }
        // line 5
        echo "\t
\t\t";
        // line 6
        if ((isset($context["block5"]) ? $context["block5"] : null)) {
            // line 7
            echo "\t\t\t";
            echo (isset($context["block5"]) ? $context["block5"] : null);
            echo "
\t\t";
        }
        // line 8
        echo "\t
\t\t";
        // line 9
        if ((isset($context["block6"]) ? $context["block6"] : null)) {
            // line 10
            echo "\t\t\t";
            echo (isset($context["block6"]) ? $context["block6"] : null);
            echo "
\t\t";
        }
        // line 11
        echo "\t
\t</div>
\t<div class=\"footer-top\">
\t  <div class=\"container\">
\t\t<div class=\"row\">
\t\t\t  <div class=\"col1 col-md-5 col-sm-12 col-footer\">
\t\t\t\t\t";
        // line 17
        if ((isset($context["block7"]) ? $context["block7"] : null)) {
            // line 18
            echo "\t\t\t\t\t\t";
            echo (isset($context["block7"]) ? $context["block7"] : null);
            echo "
\t\t\t\t\t";
        }
        // line 19
        echo "\t
\t\t\t\t</div>
\t\t\t  ";
        // line 21
        if ((isset($context["informations"]) ? $context["informations"] : null)) {
            // line 22
            echo "\t\t\t  <div class=\"col-md-2 col-sm-6 col-footer\">
\t\t\t\t<div class=\"footer-title\"><h3>";
            // line 23
            echo (isset($context["text_information"]) ? $context["text_information"] : null);
            echo "</h3></div>
\t\t\t\t<div class=\"footer-content\">
\t\t\t\t\t<ul class=\"list-unstyled text-content\">
\t\t\t\t\t ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["informations"]) ? $context["informations"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
                // line 27
                echo "\t\t\t\t\t  <li><a href=\"";
                echo $this->getAttribute($context["information"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["information"], "title", array());
                echo "</a></li>
\t\t\t\t\t  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "\t\t\t\t\t  ";
            // line 30
            echo "\t\t\t\t\t  ";
            // line 31
            echo "\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  ";
        }
        // line 35
        echo "\t\t\t  <div class=\"col-md-2 col-sm-6 col-footer\">
\t\t\t\t<div class=\"footer-title\"><h3>";
        // line 36
        echo (isset($context["text_service"]) ? $context["text_service"] : null);
        echo "</h3></div>
\t\t\t\t<div class=\"footer-content\">
\t\t\t\t\t<ul class=\"list-unstyled text-content\">
\t\t\t\t\t  <li><a href=\"";
        // line 39
        echo (isset($context["contact"]) ? $context["contact"] : null);
        echo "\">";
        echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
        echo "</a></li>
\t\t\t\t\t  ";
        // line 41
        echo "\t\t\t\t\t  ";
        // line 42
        echo "\t\t\t\t\t  ";
        // line 43
        echo "\t\t\t\t\t  ";
        // line 44
        echo "\t\t\t\t\t  ";
        // line 45
        echo "\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t  <div class=\"col-md-3 col-sm-12 col-footer\">
\t\t\t\t";
        // line 49
        if ((isset($context["block8"]) ? $context["block8"] : null)) {
            // line 50
            echo "\t\t\t\t\t";
            echo (isset($context["block8"]) ? $context["block8"] : null);
            echo "
\t\t\t\t";
        }
        // line 51
        echo "\t
\t\t\t  </div>
\t\t</div>
\t  </div>
\t</div>
\t<div class=\"footer-bottom\">
\t\t<div class=\"container\">
\t\t\t<div class=\"container-inner\">
\t\t\t\t<div class=\"footer-copyright\">
\t\t\t\t\t<span>";
        // line 60
        echo (isset($context["powered"]) ? $context["powered"] : null);
        echo "</span>
\t\t\t\t</div>
\t\t\t\t";
        // line 62
        if ((isset($context["block9"]) ? $context["block9"] : null)) {
            // line 63
            echo "\t\t\t\t\t";
            echo (isset($context["block9"]) ? $context["block9"] : null);
            echo "
\t\t\t\t";
        }
        // line 64
        echo "\t
\t\t\t</div>
\t\t</div>
\t</div>
\t<div id=\"back-top\"><i class=\"fa fa-angle-up\"></i></div>
</footer>
<script >

\$(document).ready(function(){
\t// hide #back-top first
\t\$(\"#back-top\").hide();
\t// fade in #back-top
\t\$(function () {
\t\t\$(window).scroll(function () {
\t\t\tif (\$(this).scrollTop() > \$('body').height()/3) {
\t\t\t\t\$('#back-top').fadeIn();
\t\t\t} else {
\t\t\t\t\$('#back-top').fadeOut();
\t\t\t}
\t\t});
\t\t// scroll body to 0px on click
\t\t\$('#back-top').click(function () {
\t\t\t\$('body,html').animate({scrollTop: 0}, 800);
\t\t\treturn false;
\t\t});
\t});
});
</script>
";
        // line 92
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 93
            echo "<script src=\"";
            echo $context["script"];
            echo "\" ></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
</div><!-- wrapper -->
</body></html>";
    }

    public function getTemplateName()
    {
        return "tt_naturecircle1/template/common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 95,  204 => 93,  200 => 92,  170 => 64,  164 => 63,  162 => 62,  157 => 60,  146 => 51,  140 => 50,  138 => 49,  132 => 45,  130 => 44,  128 => 43,  126 => 42,  124 => 41,  118 => 39,  112 => 36,  109 => 35,  103 => 31,  101 => 30,  99 => 29,  88 => 27,  84 => 26,  78 => 23,  75 => 22,  73 => 21,  69 => 19,  63 => 18,  61 => 17,  53 => 11,  47 => 10,  45 => 9,  42 => 8,  36 => 7,  34 => 6,  31 => 5,  25 => 4,  23 => 3,  19 => 1,);
    }
}
/* <footer>*/
/* 	<div class="container">*/
/* 		{% if block4 %}*/
/* 			{{ block4 }}*/
/* 		{% endif %}	*/
/* 		{% if block5 %}*/
/* 			{{ block5 }}*/
/* 		{% endif %}	*/
/* 		{% if block6 %}*/
/* 			{{ block6 }}*/
/* 		{% endif %}	*/
/* 	</div>*/
/* 	<div class="footer-top">*/
/* 	  <div class="container">*/
/* 		<div class="row">*/
/* 			  <div class="col1 col-md-5 col-sm-12 col-footer">*/
/* 					{% if block7 %}*/
/* 						{{ block7 }}*/
/* 					{% endif %}	*/
/* 				</div>*/
/* 			  {% if informations %}*/
/* 			  <div class="col-md-2 col-sm-6 col-footer">*/
/* 				<div class="footer-title"><h3>{{ text_information }}</h3></div>*/
/* 				<div class="footer-content">*/
/* 					<ul class="list-unstyled text-content">*/
/* 					 {% for information in informations %}*/
/* 					  <li><a href="{{ information.href }}">{{ information.title }}</a></li>*/
/* 					  {% endfor %}*/
/* 					  {#<li><a href="{{ manufacturer }}">{{ text_manufacturer }}</a></li>#}*/
/* 					  {#<li><a href="{{ voucher }}">{{ text_voucher }}</a></li>#}*/
/* 					</ul>*/
/* 				</div>*/
/* 			  </div>*/
/* 			  {% endif %}*/
/* 			  <div class="col-md-2 col-sm-6 col-footer">*/
/* 				<div class="footer-title"><h3>{{ text_service }}</h3></div>*/
/* 				<div class="footer-content">*/
/* 					<ul class="list-unstyled text-content">*/
/* 					  <li><a href="{{ contact }}">{{ text_contact }}</a></li>*/
/* 					  {#<li><a href="{{ return }}">{{ text_return }}</a></li>#}*/
/* 					  {#<li><a href="{{ sitemap }}">{{ text_sitemap }}</a></li>#}*/
/* 					  {#<li><a href="{{ affiliate }}">{{ text_affiliate }}</a></li>#}*/
/* 					  {#<li><a href="{{ special }}">{{ text_special }}</a></li>#}*/
/* 					  {#<li><a href="{{ newsletter }}">{{ text_newsletter }}</a></li>#}*/
/* 					</ul>*/
/* 				</div>*/
/* 			  </div>*/
/* 			  <div class="col-md-3 col-sm-12 col-footer">*/
/* 				{% if block8 %}*/
/* 					{{ block8 }}*/
/* 				{% endif %}	*/
/* 			  </div>*/
/* 		</div>*/
/* 	  </div>*/
/* 	</div>*/
/* 	<div class="footer-bottom">*/
/* 		<div class="container">*/
/* 			<div class="container-inner">*/
/* 				<div class="footer-copyright">*/
/* 					<span>{{ powered }}</span>*/
/* 				</div>*/
/* 				{% if block9 %}*/
/* 					{{ block9 }}*/
/* 				{% endif %}	*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 	<div id="back-top"><i class="fa fa-angle-up"></i></div>*/
/* </footer>*/
/* <script >*/
/* */
/* $(document).ready(function(){*/
/* 	// hide #back-top first*/
/* 	$("#back-top").hide();*/
/* 	// fade in #back-top*/
/* 	$(function () {*/
/* 		$(window).scroll(function () {*/
/* 			if ($(this).scrollTop() > $('body').height()/3) {*/
/* 				$('#back-top').fadeIn();*/
/* 			} else {*/
/* 				$('#back-top').fadeOut();*/
/* 			}*/
/* 		});*/
/* 		// scroll body to 0px on click*/
/* 		$('#back-top').click(function () {*/
/* 			$('body,html').animate({scrollTop: 0}, 800);*/
/* 			return false;*/
/* 		});*/
/* 	});*/
/* });*/
/* </script>*/
/* {% for script in scripts %}*/
/* <script src="{{ script }}" ></script>*/
/* {% endfor %}*/
/* <!--*/
/* OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.*/
/* Please donate via PayPal to donate@opencart.com*/
/* //-->*/
/* </div><!-- wrapper -->*/
/* </body></html>*/
