<?php

/* tt_naturecircle1/template/extension/module/ocproduct.twig */
class __TwigTemplate_49774aca79a7bcd8450a43ffa137f91643c222d9fc08d9bbc7e0697306708edc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"tt_product_module";
        if (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "row", array()) >= 2)) {
            echo " multi-rows";
        }
        echo " ";
        echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "class", array());
        echo "\" id=\"product_module";
        echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
        echo "\">
    <div class=\"module-title\">
\t  
\t  <h2>
\t\t";
        // line 5
        if ((isset($context["text_store1"]) ? $context["text_store1"] : null)) {
            // line 6
            echo "\t\t  <span>";
            echo (isset($context["text_store1"]) ? $context["text_store1"] : null);
            echo "</span>
\t\t";
        }
        // line 8
        echo "\t\t";
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "title_lang", array()), (isset($context["code"]) ? $context["code"] : null), array(), "array"), "title", array())) {
            // line 9
            echo "\t\t  ";
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "title_lang", array()), (isset($context["code"]) ? $context["code"] : null), array(), "array"), "title", array());
            echo "
\t\t";
        } else {
            // line 11
            echo "\t\t  ";
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "
\t\t";
        }
        // line 13
        echo "\t  </h2>
\t  ";
        // line 14
        if ((isset($context["module_description"]) ? $context["module_description"] : null)) {
            // line 15
            echo "\t\t<div class=\"module-description\">
\t\t  ";
            // line 16
            echo (isset($context["module_description"]) ? $context["module_description"] : null);
            echo "
\t\t</div>
\t  ";
        }
        // line 19
        echo "\t</div>
\t";
        // line 20
        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "slider", array())) {
            // line 21
            echo "\t\t";
            $context["class_slider"] = " owl-carousel owl-theme ";
            // line 22
            echo "\t";
        } else {
            // line 23
            echo "\t\t";
            $context["class_slider"] = "";
            // line 24
            echo "\t";
        }
        // line 25
        echo "\t";
        if (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "nrow", array()) == 0)) {
            // line 26
            echo "\t\t";
            $context["class"] = "two_items col-lg-6 col-md-6 col-sm-6 col-xs-12";
            // line 27
            echo "\t";
        } elseif (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "nrow", array()) == 1)) {
            // line 28
            echo "\t\t";
            $context["class"] = "three_items col-lg-4 col-md-4 col-sm-4 col-xs-12";
            // line 29
            echo "\t";
        } elseif (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "nrow", array()) == 2)) {
            // line 30
            echo "\t\t";
            $context["class"] = "four_items col-lg-3 col-md-3 col-sm-3 col-xs-12";
            // line 31
            echo "\t";
        } else {
            echo "\t\t
\t\t";
            // line 32
            $context["class"] = "six_items col-lg-2 col-md-2 col-sm-2 col-xs-12";
            // line 33
            echo "\t";
        }
        // line 34
        echo "\t";
        if ((twig_length_filter($this->env, (isset($context["products"]) ? $context["products"] : null)) > 0)) {
            // line 35
            echo "\t\t";
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "row", array())) {
                // line 36
                echo "\t\t\t";
                $context["rows"] = $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "row", array());
                // line 37
                echo "\t\t";
            } else {
                // line 38
                echo "\t\t\t";
                $context["rows"] = 1;
                // line 39
                echo "\t\t";
            }
            // line 40
            echo "\t\t";
            $context["count"] = 0;
            // line 41
            echo "    <div class=\"owl-container\">
\t<div class=\"tt-product ";
            // line 42
            echo (isset($context["class_slider"]) ? $context["class_slider"] : null);
            echo "\">\t
        ";
            // line 43
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                echo " 
\t\t\t";
                // line 44
                if ( !$this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "slider", array())) {
                    $context["rows"] = 1;
                }
                // line 45
                echo "            <!-- Grid -->
\t\t\t";
                // line 46
                if ((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0)) {
                    // line 47
                    echo "\t\t\t<div class=\"row_items ";
                    if ( !$this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "slider", array())) {
                        echo (isset($context["class"]) ? $context["class"] : null);
                    }
                    echo "\">
\t\t\t";
                }
                // line 49
                echo "\t\t\t";
                $context["count"] = ((isset($context["count"]) ? $context["count"] : null) + 1);
                // line 50
                echo "            ";
                if (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "type", array()) == 0)) {
                    // line 51
                    echo "\t\t\t\t<div class=\"product-layout grid-style \">
\t\t\t\t\t<div class=\"product-thumb transition\">
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"item-inner\">
\t\t\t\t\t\t\t\t<div class=\"image images-container\">
\t\t\t\t\t\t\t\t\t<a href=\"";
                    // line 56
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\" class=\"product-image\">
\t\t\t\t\t\t\t\t\t\t";
                    // line 57
                    if (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "rotator", array()) && $this->getAttribute($context["product"], "rotator_image", array()))) {
                        echo "<img class=\"img-r\" src=\"";
                        echo $this->getAttribute($context["product"], "rotator_image", array());
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" />";
                    }
                    // line 58
                    echo "\t\t\t\t\t\t\t\t\t\t<img src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-responsive\" />
\t\t\t\t\t\t\t\t\t</a>\t\t\t\t  
\t\t\t\t\t\t\t\t\t";
                    // line 60
                    if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "salelabel", array())) {
                        // line 61
                        echo "\t\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["product"], "special", array())) {
                            // line 62
                            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_sale\">";
                            echo (isset($context["text_label_sale"]) ? $context["text_label_sale"] : null);
                            echo "</div>
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 63
                        echo " 
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 65
                    echo "\t\t\t\t\t\t\t\t\t";
                    if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "newlabel", array())) {
                        // line 66
                        echo "\t\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["product"], "is_new", array())) {
                            // line 67
                            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_new\">";
                            echo (isset($context["text_label_new"]) ? $context["text_label_new"] : null);
                            echo "</div>
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 69
                        echo "\t\t\t\t\t\t\t\t\t";
                    }
                    // line 70
                    echo "\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                    // line 71
                    if ((($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showwishlist", array()) || $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showquickview", array())) || $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcompare", array()))) {
                        echo "\t
\t\t\t\t\t\t\t\t\t\t<div class=\"action-links\">
\t\t\t\t\t\t\t\t\t\t\t";
                        // line 73
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showwishlist", array())) {
                            // line 74
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-wishlist\" type=\"button\"  title=\"";
                            echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                            echo "\" onclick=\"wishlist.add('";
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "');\"><span>";
                            echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                            echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 76
                        echo "\t\t\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcompare", array())) {
                            // line 77
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-compare\" type=\"button\"  title=\"";
                            echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                            echo "\" onclick=\"compare.add('";
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "');\"><span>";
                            echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                            echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 79
                        echo "\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t";
                        // line 80
                        if ((isset($context["use_quickview"]) ? $context["use_quickview"] : null)) {
                            // line 81
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showquickview", array())) {
                                // line 82
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-quickview\" type=\"button\"  title=\"";
                                echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                                echo "\" onclick=\"ocquickview.ajaxView('";
                                echo $this->getAttribute($context["product"], "href", array());
                                echo "')\"><span>";
                                echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 84
                            echo "\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 85
                        echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 87
                    echo "\t\t\t\t\t\t\t\t</div><!-- image -->
\t\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                    // line 90
                    if ($this->getAttribute($context["product"], "manufacturer", array())) {
                        // line 91
                        echo "\t\t\t\t\t\t\t\t\t<p class=\"manufacture-product\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
                        // line 92
                        echo $this->getAttribute($context["product"], "manufacturers", array());
                        echo "\">";
                        echo $this->getAttribute($context["product"], "manufacturer", array());
                        echo "</a>
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 95
                    echo "\t\t\t\t\t\t\t\t\t";
                    if ($this->getAttribute($context["product"], "rating", array())) {
                        // line 96
                        echo "\t\t\t\t\t\t\t\t\t\t<div class=\"ratings\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"rating-box\">
\t\t\t\t\t\t\t\t\t\t\t";
                        // line 98
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(range(0, 5));
                        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                            // line 99
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                            if (($this->getAttribute($context["product"], "rating", array()) == $context["i"])) {
                                // line 100
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                                $context["class_r"] = ("rating" . $context["i"]);
                                // line 101
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"";
                                echo (isset($context["class_r"]) ? $context["class_r"] : null);
                                echo "\">rating</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 103
                            echo "\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 104
                        echo "\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 107
                    echo "\t\t\t\t\t\t\t\t\t<h4 class=\"product-name\"><a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "</a></h4>
\t\t\t\t\t\t\t\t\t";
                    // line 108
                    if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "description", array())) {
                        // line 109
                        echo "\t\t\t\t\t\t\t\t\t<div class=\"product-des\">";
                        echo $this->getAttribute($context["product"], "description", array());
                        echo "</div>
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 111
                    echo "\t\t\t\t\t\t\t\t\t";
                    if ((isset($context["use_catalog"]) ? $context["use_catalog"] : null)) {
                        // line 112
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["product"], "price", array())) {
                            // line 113
                            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"price-box\">
\t\t\t\t\t\t\t\t\t\t<label>";
                            // line 114
                            echo (isset($context["price_label"]) ? $context["price_label"] : null);
                            echo "</label>
\t\t\t\t\t\t\t\t\t\t";
                            // line 115
                            if ( !$this->getAttribute($context["product"], "special", array())) {
                                // line 116
                                echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"regular-price\"><span class=\"price\">";
                                echo $this->getAttribute($context["product"], "price", array());
                                echo "</span></p>
\t\t\t\t\t\t\t\t\t\t";
                            } else {
                                // line 118
                                echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"special-price\"><span class=\"price\">";
                                echo $this->getAttribute($context["product"], "special", array());
                                echo "</span></p>
\t\t\t\t\t\t\t\t\t\t\t<p class=\"old-price\"><span class=\"price\">";
                                // line 119
                                echo $this->getAttribute($context["product"], "price", array());
                                echo "</span></p>\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 121
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($context["product"], "tax", array())) {
                                // line 122
                                echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"price-tax\"><span class=\"price\">";
                                echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                                echo " ";
                                echo $this->getAttribute($context["product"], "tax", array());
                                echo "</span></p>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 124
                            echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 126
                        echo "\t\t\t\t\t\t\t\t\t";
                    }
                    // line 127
                    echo "\t\t\t\t\t\t\t\t\t     
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<p class=\"available\">";
                    // line 129
                    echo (isset($context["text_stock"]) ? $context["text_stock"] : null);
                    echo "<span class=\"ex-text\">";
                    echo $this->getAttribute($context["product"], "stock", array());
                    echo "</span></p>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                    // line 131
                    if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "countdown", array())) {
                        echo "<div class=\"text-hurryup\"><p>";
                        echo (isset($context["text_hurryup"]) ? $context["text_hurryup"] : null);
                        echo "</p></div><div id=\"Countdown";
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "-";
                        echo (isset($context["i"]) ? $context["i"] : null);
                        echo "\" class=\"box-timer\"></div> ";
                    }
                    // line 132
                    echo "\t\t\t\t\t\t\t\t\t";
                    if ((isset($context["use_catalog"]) ? $context["use_catalog"] : null)) {
                        // line 133
                        echo "\t\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcart", array())) {
                            // line 134
                            echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"box-hover\"><button class=\"button btn-cart\" type=\"button\"  title=\"";
                            echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                            echo "\" onclick=\"cart.add('";
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "');\"><span>";
                            echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                            echo "</span></button></div>
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 136
                        echo "\t\t\t\t\t\t\t\t\t";
                    }
                    // line 137
                    echo "\t\t\t\t\t\t\t\t</div><!-- caption -->
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div><!-- product-thumb -->
\t\t\t\t\t\t";
                    // line 141
                    if (($this->getAttribute($context["product"], "date_end", array()) && $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "countdown", array()))) {
                        // line 142
                        echo "\t\t\t\t\t\t<script >
\t\t\t\t\t\t\$(document).ready(function () {
\t\t\t\t\t\t\$('#Countdown";
                        // line 144
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "-";
                        echo (isset($context["i"]) ? $context["i"] : null);
                        echo "').countdown({
\t\t\t\t\t\tuntil: new Date(";
                        // line 145
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "Y");
                        echo ", ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "m");
                        echo "-1, ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "d");
                        echo ", ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "H");
                        echo ", ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "i");
                        echo ", ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "s");
                        echo "),
\t\t\t\t\t\tlabels: ['";
                        // line 146
                        echo (isset($context["text_years"]) ? $context["text_years"] : null);
                        echo "', '";
                        echo (isset($context["text_months"]) ? $context["text_months"] : null);
                        echo " ', '";
                        echo (isset($context["text_weeks"]) ? $context["text_weeks"] : null);
                        echo "', '";
                        echo (isset($context["text_days"]) ? $context["text_days"] : null);
                        echo "', '";
                        echo (isset($context["text_hrs"]) ? $context["text_hrs"] : null);
                        echo "', '";
                        echo (isset($context["text_mins"]) ? $context["text_mins"] : null);
                        echo "', '";
                        echo (isset($context["text_secs"]) ? $context["text_secs"] : null);
                        echo "'],
\t\t\t\t\t\tlabels1: ['";
                        // line 147
                        echo (isset($context["text_year"]) ? $context["text_year"] : null);
                        echo "', '";
                        echo (isset($context["text_month"]) ? $context["text_month"] : null);
                        echo " ', '";
                        echo (isset($context["text_week"]) ? $context["text_week"] : null);
                        echo "', '";
                        echo (isset($context["text_day"]) ? $context["text_day"] : null);
                        echo "', '";
                        echo (isset($context["text_hr"]) ? $context["text_hr"] : null);
                        echo "', '";
                        echo (isset($context["text_min"]) ? $context["text_min"] : null);
                        echo "', '";
                        echo (isset($context["text_sec"]) ? $context["text_sec"] : null);
                        echo "'],
\t\t\t\t\t\t});
\t\t\t\t\t\t //\$('#Countdown";
                        // line 149
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "-";
                        echo (isset($context["i"]) ? $context["i"] : null);
                        echo "').countdown('pause');
\t\t\t\t\t\t});
\t\t\t\t\t\t</script>
\t\t\t\t\t\t";
                    }
                    // line 153
                    echo "\t\t\t\t</div><!-- product-layout -->
            ";
                } elseif (($this->getAttribute(                // line 154
(isset($context["config_module"]) ? $context["config_module"] : null), "type", array()) == 1)) {
                    // line 155
                    echo "            <!-- List -->
            <div class=\"product-layout list-style \">
\t\t\t\t\t<div class=\"product-thumb transition\">
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"item-inner\">
\t\t\t\t\t\t\t\t<div class=\"image images-container\">
\t\t\t\t\t\t\t\t\t<a href=\"";
                    // line 161
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\" class=\"product-image\">
\t\t\t\t\t\t\t\t\t\t";
                    // line 162
                    if (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "rotator", array()) && $this->getAttribute($context["product"], "rotator_image", array()))) {
                        echo "<img class=\"img-r\" src=\"";
                        echo $this->getAttribute($context["product"], "rotator_image", array());
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" />";
                    }
                    // line 163
                    echo "\t\t\t\t\t\t\t\t\t\t<img src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-responsive\" />
\t\t\t\t\t\t\t\t\t</a>\t\t\t\t  
\t\t\t\t\t\t\t\t\t";
                    // line 165
                    if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "salelabel", array())) {
                        // line 166
                        echo "\t\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["product"], "special", array())) {
                            // line 167
                            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_sale\">";
                            echo (isset($context["text_label_sale"]) ? $context["text_label_sale"] : null);
                            echo "</div>
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 169
                        echo "\t\t\t\t\t\t\t\t\t";
                    }
                    // line 170
                    echo "\t\t\t\t\t\t\t\t\t";
                    if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "newlabel", array())) {
                        // line 171
                        echo "\t\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["product"], "is_new", array())) {
                            // line 172
                            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_new\">";
                            echo (isset($context["text_label_new"]) ? $context["text_label_new"] : null);
                            echo "</div>
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 174
                        echo "\t\t\t\t\t\t\t\t\t";
                    }
                    // line 175
                    echo "\t\t\t\t\t\t\t\t\t";
                    if ((($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showwishlist", array()) || $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showquickview", array())) || $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcompare", array()))) {
                        echo "\t
\t\t\t\t\t\t\t\t\t\t<div class=\"action-links\">
\t\t\t\t\t\t\t\t\t\t\t";
                        // line 177
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showwishlist", array())) {
                            // line 178
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-wishlist\" type=\"button\"  title=\"";
                            echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                            echo "\" onclick=\"wishlist.add('";
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "');\"><span>";
                            echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                            echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 180
                        echo "\t\t\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcompare", array())) {
                            // line 181
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-compare\" type=\"button\"  title=\"";
                            echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                            echo "\" onclick=\"compare.add('";
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "');\"><span>";
                            echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                            echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 183
                        echo "\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t";
                        // line 184
                        if ((isset($context["use_quickview"]) ? $context["use_quickview"] : null)) {
                            // line 185
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showquickview", array())) {
                                // line 186
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-quickview\" type=\"button\"  title=\"";
                                echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                                echo "\" onclick=\"ocquickview.ajaxView('";
                                echo $this->getAttribute($context["product"], "href", array());
                                echo "')\"><span>";
                                echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 188
                            echo "\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 189
                        echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 191
                    echo "\t\t\t\t\t\t\t\t</div><!-- image -->
\t\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t\t";
                    // line 193
                    if ($this->getAttribute($context["product"], "manufacturer", array())) {
                        // line 194
                        echo "\t\t\t\t\t\t\t\t\t<p class=\"manufacture-product\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
                        // line 195
                        echo $this->getAttribute($context["product"], "manufacturers", array());
                        echo "\">";
                        echo $this->getAttribute($context["product"], "manufacturer", array());
                        echo "</a>
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 198
                    echo "\t\t\t\t\t\t\t\t\t";
                    if ($this->getAttribute($context["product"], "rating", array())) {
                        // line 199
                        echo "\t\t\t\t\t\t\t\t\t<div class=\"ratings\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rating-box\">
\t\t\t\t\t\t\t\t\t\t";
                        // line 201
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(range(0, 5));
                        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                            // line 202
                            echo "\t\t\t\t\t\t\t\t\t\t\t";
                            if (($this->getAttribute($context["product"], "rating", array()) == $context["i"])) {
                                // line 203
                                echo "\t\t\t\t\t\t\t\t\t\t\t";
                                $context["class_r"] = ("rating" . $context["i"]);
                                // line 204
                                echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"";
                                echo (isset($context["class_r"]) ? $context["class_r"] : null);
                                echo "\">rating</div>
\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 206
                            echo "\t\t\t\t\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 207
                        echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 210
                    echo "\t\t\t\t\t\t\t\t\t<h4 class=\"product-name\"><a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "</a></h4> 
\t\t\t\t\t\t\t\t\t";
                    // line 211
                    if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "description", array())) {
                        // line 212
                        echo "\t\t\t\t\t\t\t\t\t<div class=\"product-des\">";
                        echo $this->getAttribute($context["product"], "description", array());
                        echo "</div>
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 214
                    echo "\t\t\t\t\t\t\t\t\t";
                    if ((isset($context["use_catalog"]) ? $context["use_catalog"] : null)) {
                        // line 215
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["product"], "price", array())) {
                            // line 216
                            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"price-box\">
\t\t\t\t\t\t\t\t\t\t<label>";
                            // line 217
                            echo (isset($context["price_label"]) ? $context["price_label"] : null);
                            echo "</label>
\t\t\t\t\t\t\t\t\t\t";
                            // line 218
                            if ( !$this->getAttribute($context["product"], "special", array())) {
                                // line 219
                                echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"regular-price\"><span class=\"price\">";
                                echo $this->getAttribute($context["product"], "price", array());
                                echo "</span></p>
\t\t\t\t\t\t\t\t\t\t";
                            } else {
                                // line 221
                                echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"special-price\"><span class=\"price\">";
                                echo $this->getAttribute($context["product"], "special", array());
                                echo "</span></p>
\t\t\t\t\t\t\t\t\t\t\t<p class=\"old-price\"><span class=\"price\">";
                                // line 222
                                echo $this->getAttribute($context["product"], "price", array());
                                echo "</span></p>\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 224
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($context["product"], "tax", array())) {
                                // line 225
                                echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"price-tax\"><span class=\"price\">";
                                echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                                echo " ";
                                echo $this->getAttribute($context["product"], "tax", array());
                                echo "</span></p>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 227
                            echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 229
                        echo "\t\t\t\t\t\t\t\t\t";
                    }
                    // line 230
                    echo "\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                    // line 231
                    if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "countdown", array())) {
                        echo "<div class=\"text-hurryup\"><p>";
                        echo (isset($context["text_hurryup"]) ? $context["text_hurryup"] : null);
                        echo "</p></div><div id=\"Countdown";
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "-";
                        echo (isset($context["i"]) ? $context["i"] : null);
                        echo "\" class=\"box-timer\"></div> ";
                    }
                    // line 232
                    echo "\t\t\t\t\t\t\t\t\t";
                    if ((isset($context["use_catalog"]) ? $context["use_catalog"] : null)) {
                        // line 233
                        echo "\t\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcart", array())) {
                            // line 234
                            echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"box-hover\"><button class=\"button btn-cart\" type=\"button\"  title=\"";
                            echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                            echo "\" onclick=\"cart.add('";
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "');\"><span>";
                            echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                            echo "</span></button></div>
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 236
                        echo "\t\t\t\t\t\t\t\t\t";
                    }
                    // line 237
                    echo "\t\t\t\t\t\t\t\t</div><!-- caption -->
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div><!-- product-thumb -->
\t\t\t\t\t\t";
                    // line 241
                    if (($this->getAttribute($context["product"], "date_end", array()) && $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "countdown", array()))) {
                        // line 242
                        echo "\t\t\t\t\t\t<script >
\t\t\t\t\t\t\$(document).ready(function () {
\t\t\t\t\t\t\$('#Countdown";
                        // line 244
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "-";
                        echo (isset($context["i"]) ? $context["i"] : null);
                        echo "').countdown({
\t\t\t\t\t\tuntil: new Date(";
                        // line 245
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "Y");
                        echo ", ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "m");
                        echo "-1, ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "d");
                        echo ", ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "H");
                        echo ", ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "i");
                        echo ", ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "s");
                        echo "),
\t\t\t\t\t\tlabels: ['";
                        // line 246
                        echo (isset($context["text_years"]) ? $context["text_years"] : null);
                        echo "', '";
                        echo (isset($context["text_months"]) ? $context["text_months"] : null);
                        echo " ', '";
                        echo (isset($context["text_weeks"]) ? $context["text_weeks"] : null);
                        echo "', '";
                        echo (isset($context["text_days"]) ? $context["text_days"] : null);
                        echo "', '";
                        echo (isset($context["text_hrs"]) ? $context["text_hrs"] : null);
                        echo "', '";
                        echo (isset($context["text_mins"]) ? $context["text_mins"] : null);
                        echo "', '";
                        echo (isset($context["text_secs"]) ? $context["text_secs"] : null);
                        echo "'],
\t\t\t\t\t\tlabels1: ['";
                        // line 247
                        echo (isset($context["text_year"]) ? $context["text_year"] : null);
                        echo "', '";
                        echo (isset($context["text_month"]) ? $context["text_month"] : null);
                        echo " ', '";
                        echo (isset($context["text_week"]) ? $context["text_week"] : null);
                        echo "', '";
                        echo (isset($context["text_day"]) ? $context["text_day"] : null);
                        echo "', '";
                        echo (isset($context["text_hr"]) ? $context["text_hr"] : null);
                        echo "', '";
                        echo (isset($context["text_min"]) ? $context["text_min"] : null);
                        echo "', '";
                        echo (isset($context["text_sec"]) ? $context["text_sec"] : null);
                        echo "'],
\t\t\t\t\t\t});
\t\t\t\t\t\t//\$('#Countdown";
                        // line 249
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "-";
                        echo (isset($context["i"]) ? $context["i"] : null);
                        echo "').countdown('pause');
\t\t\t\t\t\t});
\t\t\t\t\t\t</script>
\t\t\t\t\t\t";
                    }
                    // line 253
                    echo "\t\t\t\t</div><!-- product-layout -->
            ";
                } else {
                    // line 255
                    echo "            <!-- other type -->
            <div class=\"product-layout product-customize \">
\t\t\t\t\t<div class=\"product-thumb transition\">
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"item-inner\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col col1 col-sm-4 col-xs-12\">
\t\t\t\t\t\t\t\t\t\t<div class=\"caption-top\">
\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"product-name\"><a href=\"";
                    // line 263
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "</a></h4>
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 264
                    if ($this->getAttribute($context["product"], "manufacturer", array())) {
                        // line 265
                        echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"manufacture-product\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 266
                        echo (isset($context["text_manufacture"]) ? $context["text_manufacture"] : null);
                        echo "
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                        // line 267
                        echo $this->getAttribute($context["product"], "manufacturers", array());
                        echo "\">";
                        echo $this->getAttribute($context["product"], "manufacturer", array());
                        echo "</a>
\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 270
                    echo "\t\t\t\t\t\t\t\t\t\t\t";
                    if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "description", array())) {
                        // line 271
                        echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"product-des\">";
                        echo $this->getAttribute($context["product"], "description", array());
                        echo "</div>
\t\t\t\t\t\t\t\t\t\t\t<a class=\"read-more\" href=\"";
                        // line 272
                        echo $this->getAttribute($context["product"], "href", array());
                        echo "\">";
                        echo (isset($context["text_more"]) ? $context["text_more"] : null);
                        echo "</a>
\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 274
                    echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col col2 col-sm-4 col-xs-12\">
\t\t\t\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 278
                    if ((isset($context["use_catalog"]) ? $context["use_catalog"] : null)) {
                        // line 279
                        echo "\t\t\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["product"], "price", array())) {
                            // line 280
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"price-box\">
\t\t\t\t\t\t\t\t\t\t\t\t<label>";
                            // line 281
                            echo (isset($context["price_label"]) ? $context["price_label"] : null);
                            echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t";
                            // line 282
                            if ( !$this->getAttribute($context["product"], "special", array())) {
                                // line 283
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"regular-price\"><span class=\"price\">";
                                echo $this->getAttribute($context["product"], "price", array());
                                echo "</span></p>
\t\t\t\t\t\t\t\t\t\t\t\t";
                            } else {
                                // line 285
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"special-price\"><span class=\"price\">";
                                echo $this->getAttribute($context["product"], "special", array());
                                echo "</span></p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"old-price\"><span class=\"price\">";
                                // line 286
                                echo $this->getAttribute($context["product"], "price", array());
                                echo "</span></p>\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 288
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($context["product"], "tax", array())) {
                                // line 289
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"price-tax\"><span class=\"price\">";
                                echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                                echo " ";
                                echo $this->getAttribute($context["product"], "tax", array());
                                echo "</span></p>
\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 291
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 293
                        echo "\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 294
                    echo "\t\t\t\t\t\t\t\t\t\t\t";
                    if ($this->getAttribute($context["product"], "rating", array())) {
                        // line 295
                        echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"ratings\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"rating-box\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 297
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(range(0, 5));
                        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                            // line 298
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if (($this->getAttribute($context["product"], "rating", array()) == $context["i"])) {
                                // line 299
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                $context["class_r"] = ("rating" . $context["i"]);
                                // line 300
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"";
                                echo (isset($context["class_r"]) ? $context["class_r"] : null);
                                echo "\">rating</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 302
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 303
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 306
                    echo "\t\t\t\t\t\t\t\t\t\t\t";
                    if ((($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcart", array()) || $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showwishlist", array())) || $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcompare", array()))) {
                        echo "\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"action-links\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 308
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcompare", array())) {
                            // line 309
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-compare\" type=\"button\"  title=\"";
                            echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                            echo "\" onclick=\"compare.add('";
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "');\"><span>";
                            echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                            echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 311
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        if ((isset($context["use_catalog"]) ? $context["use_catalog"] : null)) {
                            // line 312
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcart", array())) {
                                // line 313
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-cart\" type=\"button\"  title=\"";
                                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                                echo "\" onclick=\"cart.add('";
                                echo $this->getAttribute($context["product"], "product_id", array());
                                echo "');\"><span>";
                                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 315
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 316
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        if ((isset($context["use_quickview"]) ? $context["use_quickview"] : null)) {
                            // line 317
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showquickview", array())) {
                                // line 318
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-quickview\" type=\"button\"  title=\"";
                                echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                                echo "\" onclick=\"ocquickview.ajaxView('";
                                echo $this->getAttribute($context["product"], "href", array());
                                echo "')\"><span>";
                                echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 320
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 321
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 325
                    echo "\t\t\t\t\t\t\t\t\t\t\t";
                    if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "countdown", array())) {
                        echo "<div class=\"text-hurryup\"><p>";
                        echo (isset($context["text_hurryup"]) ? $context["text_hurryup"] : null);
                        echo "</p></div><div id=\"Countdown";
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "-";
                        echo (isset($context["i"]) ? $context["i"] : null);
                        echo "\" class=\"box-timer\"></div> ";
                    }
                    // line 326
                    echo "\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</div><!-- caption -->
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col col3 col-sm-4 col-xs-12\">
\t\t\t\t\t\t\t\t\t\t<div class=\"image images-container \">
\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    // line 331
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\" class=\"product-image\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 332
                    if (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "rotator", array()) && $this->getAttribute($context["product"], "rotator_image", array()))) {
                        echo "<img class=\"img-r\" src=\"";
                        echo $this->getAttribute($context["product"], "rotator_image", array());
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" />";
                    }
                    // line 333
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-responsive\" />
\t\t\t\t\t\t\t\t\t\t\t</a>\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 335
                    if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "salelabel", array())) {
                        // line 336
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["product"], "special", array())) {
                            // line 337
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_sale\">";
                            echo (isset($context["text_label_sale"]) ? $context["text_label_sale"] : null);
                            echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 338
                        echo " 
\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 340
                    echo "\t\t\t\t\t\t\t\t\t\t\t";
                    if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "newlabel", array())) {
                        // line 341
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["product"], "is_new", array())) {
                            // line 342
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_new\">";
                            echo (isset($context["text_label_new"]) ? $context["text_label_new"] : null);
                            echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 344
                        echo "\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 345
                    echo "\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 346
                    if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showwishlist", array())) {
                        // line 347
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-wishlist\" type=\"button\"  title=\"";
                        echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                        echo "\" onclick=\"wishlist.add('";
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "');\"><span>";
                        echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                        echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 349
                    echo "\t\t\t\t\t\t\t\t\t\t</div><!-- image -->
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div><!-- product-thumb -->
\t\t\t\t\t\t";
                    // line 355
                    if (($this->getAttribute($context["product"], "date_end", array()) && $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "countdown", array()))) {
                        // line 356
                        echo "\t\t\t\t\t\t<script >
\t\t\t\t\t\t\$(document).ready(function () {
\t\t\t\t\t\t\$('#Countdown";
                        // line 358
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "-";
                        echo (isset($context["i"]) ? $context["i"] : null);
                        echo "').countdown({
\t\t\t\t\t\tuntil: new Date(";
                        // line 359
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "Y");
                        echo ", ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "m");
                        echo "-1, ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "d");
                        echo ", ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "H");
                        echo ", ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "i");
                        echo ", ";
                        echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "s");
                        echo "),
\t\t\t\t\t\tlabels: ['";
                        // line 360
                        echo (isset($context["text_years"]) ? $context["text_years"] : null);
                        echo "', '";
                        echo (isset($context["text_months"]) ? $context["text_months"] : null);
                        echo " ', '";
                        echo (isset($context["text_weeks"]) ? $context["text_weeks"] : null);
                        echo "', '";
                        echo (isset($context["text_days"]) ? $context["text_days"] : null);
                        echo "', '";
                        echo (isset($context["text_hrs"]) ? $context["text_hrs"] : null);
                        echo "', '";
                        echo (isset($context["text_mins"]) ? $context["text_mins"] : null);
                        echo "', '";
                        echo (isset($context["text_secs"]) ? $context["text_secs"] : null);
                        echo "'],
\t\t\t\t\t\tlabels1: ['";
                        // line 361
                        echo (isset($context["text_year"]) ? $context["text_year"] : null);
                        echo "', '";
                        echo (isset($context["text_month"]) ? $context["text_month"] : null);
                        echo " ', '";
                        echo (isset($context["text_week"]) ? $context["text_week"] : null);
                        echo "', '";
                        echo (isset($context["text_day"]) ? $context["text_day"] : null);
                        echo "', '";
                        echo (isset($context["text_hr"]) ? $context["text_hr"] : null);
                        echo "', '";
                        echo (isset($context["text_min"]) ? $context["text_min"] : null);
                        echo "', '";
                        echo (isset($context["text_sec"]) ? $context["text_sec"] : null);
                        echo "'],
\t\t\t\t\t\t});
\t\t\t\t\t\t //\$('#Countdown";
                        // line 363
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "-";
                        echo (isset($context["i"]) ? $context["i"] : null);
                        echo "').countdown('pause');
\t\t\t\t\t\t});
\t\t\t\t\t\t</script>
\t\t\t\t\t\t";
                    }
                    // line 367
                    echo "\t\t\t\t</div><!-- product-layout -->
            ";
                }
                // line 369
                echo "\t\t\t\t";
                if (((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0) || ((isset($context["count"]) ? $context["count"] : null) == twig_length_filter($this->env, (isset($context["products"]) ? $context["products"] : null))))) {
                    // line 370
                    echo "\t\t\t\t</div>
\t\t\t\t";
                }
                // line 372
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "\t
    </div>
\t</div>
\t";
        } else {
            // line 376
            echo "\t\t<p class=\"text_empty\">";
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
\t";
        }
        // line 378
        echo "\t<div class=\"clearfix\"></div>
</div>
";
        // line 380
        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "slider", array())) {
            // line 381
            echo "    <script >
        \$(document).ready(function() {
            \$(\"#product_module";
            // line 383
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .tt-product\").owlCarousel({
                loop: ";
            // line 384
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "loop", array())) {
                echo " true ";
            } else {
                echo " false ";
            }
            echo ",
                margin: ";
            // line 385
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "margin", array(), "any", true, true)) {
                echo " ";
                echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "margin", array());
                echo " ";
            } else {
                echo " 20 ";
            }
            echo ",
                nav: ";
            // line 386
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "navigation", array())) {
                echo " true ";
            } else {
                echo " false ";
            }
            echo ",
                dots: ";
            // line 387
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "pagination", array())) {
                echo " true ";
            } else {
                echo " false ";
            }
            echo ",
                autoplay:  ";
            // line 388
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "auto", array())) {
                echo " true ";
            } else {
                echo " false ";
            }
            echo ",
                autoplayTimeout: ";
            // line 389
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "time", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "time", array());
                echo " ";
            } else {
                echo " 2000 ";
            }
            echo ",
                autoplayHoverPause: true,
                autoplaySpeed: ";
            // line 391
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "speed", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "speed", array());
                echo " ";
            } else {
                echo " 3000 ";
            }
            echo ",
                navSpeed: ";
            // line 392
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "speed", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "speed", array());
                echo " ";
            } else {
                echo " 3000 ";
            }
            echo ",
                dotsSpeed: ";
            // line 393
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "speed", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "speed", array());
                echo " ";
            } else {
                echo " 3000 ";
            }
            echo ",
\t\t\t\tlazyLoad: true,
                responsive:{
\t\t\t\t\t0:{
\t\t\t\t\t\titems: 1,
\t\t\t\t\t\tnav: false
\t\t\t\t\t},
\t\t\t\t\t480:{
\t\t\t\t\t\titems: ";
            // line 401
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "smobile", array());
            echo ",
\t\t\t\t\t\tnav: false
\t\t\t\t\t},
\t\t\t\t\t768:{
\t\t\t\t\t\titems: ";
            // line 405
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "mobile", array());
            echo "
\t\t\t\t\t},
\t\t\t\t\t992:{
\t\t\t\t\t\titems: ";
            // line 408
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "tablet", array());
            echo "
\t\t\t\t\t},
\t\t\t\t\t1200:{
\t\t\t\t\t\titems: ";
            // line 411
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "desktop", array());
            echo "
\t\t\t\t\t},
\t\t\t\t\t1600:{
\t\t\t\t\t\titems: ";
            // line 414
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "items", array());
            echo "
\t\t\t\t\t}
                },
\t\t\t\tonInitialized: function() {
\t\t\t\t\tvar count = \$(\"#product_module";
            // line 418
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .tt-product .owl-item.active\").length;
\t\t\t\t\tif(count == 1) {
\t\t\t\t\t\t\$(\"#product_module";
            // line 420
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .tt-product .owl-item\").removeClass('first');
\t\t\t\t\t\t\$(\"#product_module";
            // line 421
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .tt-product .active\").addClass('first');
\t\t\t\t\t} else {
\t\t\t\t\t\t\$(\"#product_module";
            // line 423
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .tt-product .owl-item\").removeClass('first');
\t\t\t\t\t\t\$(\"#product_module";
            // line 424
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .tt-product .owl-item.active:first\").addClass('first');
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t},
\t\t\t\tonTranslated: function() {
\t\t\t\t\tvar count = \$(\"#product_module";
            // line 429
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .tt-product .owl-item.active\").length;
\t\t\t\t\tif(count == 1) {
\t\t\t\t\t\t\$(\"#product_module";
            // line 431
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .tt-product .owl-item\").removeClass('first');
\t\t\t\t\t\t\$(\"#product_module";
            // line 432
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .tt-product .active\").addClass('first');
\t\t\t\t\t} else {
\t\t\t\t\t\t\$(\"#product_module";
            // line 434
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .tt-product .owl-item\").removeClass('first');
\t\t\t\t\t\t\$(\"#product_module";
            // line 435
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .tt-product .owl-item.active:first\").addClass('first');
\t\t\t\t\t}
\t\t\t\t}
            });
\t\t\t
        });
    </script>
";
        }
    }

    public function getTemplateName()
    {
        return "tt_naturecircle1/template/extension/module/ocproduct.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1431 => 435,  1427 => 434,  1422 => 432,  1418 => 431,  1413 => 429,  1405 => 424,  1401 => 423,  1396 => 421,  1392 => 420,  1387 => 418,  1380 => 414,  1374 => 411,  1368 => 408,  1362 => 405,  1355 => 401,  1338 => 393,  1328 => 392,  1318 => 391,  1307 => 389,  1299 => 388,  1291 => 387,  1283 => 386,  1273 => 385,  1265 => 384,  1261 => 383,  1257 => 381,  1255 => 380,  1251 => 378,  1245 => 376,  1234 => 372,  1230 => 370,  1227 => 369,  1223 => 367,  1214 => 363,  1197 => 361,  1181 => 360,  1167 => 359,  1161 => 358,  1157 => 356,  1155 => 355,  1147 => 349,  1137 => 347,  1135 => 346,  1132 => 345,  1129 => 344,  1123 => 342,  1120 => 341,  1117 => 340,  1113 => 338,  1107 => 337,  1104 => 336,  1102 => 335,  1092 => 333,  1084 => 332,  1080 => 331,  1073 => 326,  1062 => 325,  1056 => 321,  1053 => 320,  1043 => 318,  1040 => 317,  1037 => 316,  1034 => 315,  1024 => 313,  1021 => 312,  1018 => 311,  1008 => 309,  1006 => 308,  1000 => 306,  995 => 303,  989 => 302,  983 => 300,  980 => 299,  977 => 298,  973 => 297,  969 => 295,  966 => 294,  963 => 293,  959 => 291,  951 => 289,  948 => 288,  943 => 286,  938 => 285,  932 => 283,  930 => 282,  926 => 281,  923 => 280,  920 => 279,  918 => 278,  912 => 274,  905 => 272,  900 => 271,  897 => 270,  889 => 267,  885 => 266,  882 => 265,  880 => 264,  874 => 263,  864 => 255,  860 => 253,  851 => 249,  834 => 247,  818 => 246,  804 => 245,  798 => 244,  794 => 242,  792 => 241,  786 => 237,  783 => 236,  773 => 234,  770 => 233,  767 => 232,  757 => 231,  754 => 230,  751 => 229,  747 => 227,  739 => 225,  736 => 224,  731 => 222,  726 => 221,  720 => 219,  718 => 218,  714 => 217,  711 => 216,  708 => 215,  705 => 214,  699 => 212,  697 => 211,  690 => 210,  685 => 207,  679 => 206,  673 => 204,  670 => 203,  667 => 202,  663 => 201,  659 => 199,  656 => 198,  648 => 195,  645 => 194,  643 => 193,  639 => 191,  635 => 189,  632 => 188,  622 => 186,  619 => 185,  617 => 184,  614 => 183,  604 => 181,  601 => 180,  591 => 178,  589 => 177,  583 => 175,  580 => 174,  574 => 172,  571 => 171,  568 => 170,  565 => 169,  559 => 167,  556 => 166,  554 => 165,  544 => 163,  536 => 162,  532 => 161,  524 => 155,  522 => 154,  519 => 153,  510 => 149,  493 => 147,  477 => 146,  463 => 145,  457 => 144,  453 => 142,  451 => 141,  445 => 137,  442 => 136,  432 => 134,  429 => 133,  426 => 132,  416 => 131,  409 => 129,  405 => 127,  402 => 126,  398 => 124,  390 => 122,  387 => 121,  382 => 119,  377 => 118,  371 => 116,  369 => 115,  365 => 114,  362 => 113,  359 => 112,  356 => 111,  350 => 109,  348 => 108,  341 => 107,  336 => 104,  330 => 103,  324 => 101,  321 => 100,  318 => 99,  314 => 98,  310 => 96,  307 => 95,  299 => 92,  296 => 91,  294 => 90,  289 => 87,  285 => 85,  282 => 84,  272 => 82,  269 => 81,  267 => 80,  264 => 79,  254 => 77,  251 => 76,  241 => 74,  239 => 73,  234 => 71,  231 => 70,  228 => 69,  222 => 67,  219 => 66,  216 => 65,  212 => 63,  206 => 62,  203 => 61,  201 => 60,  191 => 58,  183 => 57,  179 => 56,  172 => 51,  169 => 50,  166 => 49,  158 => 47,  156 => 46,  153 => 45,  149 => 44,  143 => 43,  139 => 42,  136 => 41,  133 => 40,  130 => 39,  127 => 38,  124 => 37,  121 => 36,  118 => 35,  115 => 34,  112 => 33,  110 => 32,  105 => 31,  102 => 30,  99 => 29,  96 => 28,  93 => 27,  90 => 26,  87 => 25,  84 => 24,  81 => 23,  78 => 22,  75 => 21,  73 => 20,  70 => 19,  64 => 16,  61 => 15,  59 => 14,  56 => 13,  50 => 11,  44 => 9,  41 => 8,  35 => 6,  33 => 5,  19 => 1,);
    }
}
/* <div class="tt_product_module{% if config_module.row >=2 %}{{' multi-rows'}}{% endif %} {{ config_module.class }}" id="product_module{{ config_module.module_id }}">*/
/*     <div class="module-title">*/
/* 	  */
/* 	  <h2>*/
/* 		{% if text_store1 %}*/
/* 		  <span>{{ text_store1 }}</span>*/
/* 		{% endif %}*/
/* 		{% if config_module.title_lang[code].title %}*/
/* 		  {{ config_module.title_lang[code].title }}*/
/* 		{% else %}*/
/* 		  {{ heading_title }}*/
/* 		{% endif %}*/
/* 	  </h2>*/
/* 	  {% if module_description %}*/
/* 		<div class="module-description">*/
/* 		  {{ module_description }}*/
/* 		</div>*/
/* 	  {% endif %}*/
/* 	</div>*/
/* 	{% if config_module.slider %}*/
/* 		{% set class_slider = ' owl-carousel owl-theme '%}*/
/* 	{% else %}*/
/* 		{% set class_slider = ''%}*/
/* 	{% endif %}*/
/* 	{% if config_module.nrow == 0 %}*/
/* 		{% set class = 'two_items col-lg-6 col-md-6 col-sm-6 col-xs-12' %}*/
/* 	{% elseif config_module.nrow == 1 %}*/
/* 		{% set class = 'three_items col-lg-4 col-md-4 col-sm-4 col-xs-12' %}*/
/* 	{% elseif config_module.nrow == 2 %}*/
/* 		{% set class = 'four_items col-lg-3 col-md-3 col-sm-3 col-xs-12' %}*/
/* 	{% else %}		*/
/* 		{% set class = 'six_items col-lg-2 col-md-2 col-sm-2 col-xs-12' %}*/
/* 	{% endif %}*/
/* 	{% if products|length > 0 %}*/
/* 		{% if config_module.row %}*/
/* 			{% set rows = config_module.row %}*/
/* 		{% else %}*/
/* 			{% set rows = 1 %}*/
/* 		{% endif %}*/
/* 		{% set count = 0 %}*/
/*     <div class="owl-container">*/
/* 	<div class="tt-product {{ class_slider }}">	*/
/*         {% for product in products %} */
/* 			{% if not config_module.slider %}{% set rows = 1 %}{% endif %}*/
/*             <!-- Grid -->*/
/* 			{% if count % rows == 0 %}*/
/* 			<div class="row_items {% if not config_module.slider %}{{ class }}{% endif %}">*/
/* 			{% endif %}*/
/* 			{% set count = count + 1 %}*/
/*             {% if config_module.type == 0 %}*/
/* 				<div class="product-layout grid-style ">*/
/* 					<div class="product-thumb transition">*/
/* 						<div class="item">*/
/* 							<div class="item-inner">*/
/* 								<div class="image images-container">*/
/* 									<a href="{{ product.href }}" class="product-image">*/
/* 										{% if config_module.rotator and product.rotator_image %}<img class="img-r" src="{{ product.rotator_image }}" alt="{{ product.name }}" />{% endif %}*/
/* 										<img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive" />*/
/* 									</a>				  */
/* 									{% if config_module.salelabel %}*/
/* 										{% if product.special %}*/
/* 										<div class="label-product label_sale">{{ text_label_sale }}</div>*/
/* 										{% endif %} */
/* 									{% endif %}*/
/* 									{% if config_module.newlabel %}*/
/* 										{% if product.is_new %}*/
/* 										<div class="label-product label_new">{{ text_label_new }}</div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 									*/
/* 									{% if config_module.showwishlist or config_module.showquickview or  config_module.showcompare %}	*/
/* 										<div class="action-links">*/
/* 											{% if config_module.showwishlist %}*/
/* 												<button class="button btn-wishlist" type="button"  title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><span>{{ button_wishlist }}</span></button>*/
/* 											{% endif %}*/
/* 											{% if config_module.showcompare %}*/
/* 												<button class="button btn-compare" type="button"  title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><span>{{ button_compare }}</span></button>*/
/* 											{% endif %}*/
/* 											*/
/* 											{% if use_quickview %}*/
/* 												{% if config_module.showquickview %}*/
/* 													<button class="button btn-quickview" type="button"  title="{{ button_quickview }}" onclick="ocquickview.ajaxView('{{ product.href }}')"><span>{{ button_quickview }}</span></button>*/
/* 												{% endif %}*/
/* 											{% endif %}*/
/* 										</div>*/
/* 									{% endif %}*/
/* 								</div><!-- image -->*/
/* 								<div class="caption">*/
/* 									*/
/* 									{% if product.manufacturer %}*/
/* 									<p class="manufacture-product">*/
/* 										<a href="{{ product.manufacturers }}">{{ product.manufacturer }}</a>*/
/* 									</p>*/
/* 									{% endif %}*/
/* 									{% if product.rating %}*/
/* 										<div class="ratings">*/
/* 											<div class="rating-box">*/
/* 											{% for i in 0..5 %}*/
/* 												{% if product.rating == i %}*/
/* 												{% set class_r = "rating"~i %}*/
/* 												<div class="{{ class_r }}">rating</div>*/
/* 												{% endif %}*/
/* 											{% endfor %}*/
/* 											</div>*/
/* 										</div>					*/
/* 									{% endif %}*/
/* 									<h4 class="product-name"><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/* 									{% if config_module.description %}*/
/* 									<div class="product-des">{{ product.description }}</div>*/
/* 									{% endif %}*/
/* 									{% if use_catalog %}*/
/* 									{% if product.price %}*/
/* 										<div class="price-box">*/
/* 										<label>{{ price_label }}</label>*/
/* 										{% if not product.special %}*/
/* 											<p class="regular-price"><span class="price">{{ product.price }}</span></p>*/
/* 										{% else %}*/
/* 											<p class="special-price"><span class="price">{{ product.special }}</span></p>*/
/* 											<p class="old-price"><span class="price">{{ product.price }}</span></p>						  */
/* 										{% endif %}*/
/* 										{% if product.tax %}*/
/* 											<p class="price-tax"><span class="price">{{ text_tax }} {{ product.tax }}</span></p>*/
/* 										{% endif %}*/
/* 										</div>*/
/* 									{% endif %}*/
/* 									{% endif %}*/
/* 									     */
/* 									*/
/* 									<p class="available">{{ text_stock }}<span class="ex-text">{{ product.stock }}</span></p>*/
/* 									*/
/* 									{% if config_module.countdown %}<div class="text-hurryup"><p>{{ text_hurryup }}</p></div><div id="Countdown{{ product.product_id }}-{{ i }}" class="box-timer"></div> {% endif %}*/
/* 									{% if use_catalog %}*/
/* 										{% if config_module.showcart %}*/
/* 											<div class="box-hover"><button class="button btn-cart" type="button"  title="{{ button_cart }}" onclick="cart.add('{{ product.product_id }}');"><span>{{ button_cart }}</span></button></div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 								</div><!-- caption -->*/
/* 							</div>*/
/* 						</div>*/
/* 					</div><!-- product-thumb -->*/
/* 						{% if product.date_end and config_module.countdown %}*/
/* 						<script >*/
/* 						$(document).ready(function () {*/
/* 						$('#Countdown{{ product.product_id }}-{{ i }}').countdown({*/
/* 						until: new Date({{ product.date_end|date("Y") }}, {{ product.date_end|date("m") }}-1, {{ product.date_end|date("d") }}, {{ product.date_end|date("H") }}, {{ product.date_end|date("i") }}, {{ product.date_end|date("s") }}),*/
/* 						labels: ['{{ text_years }}', '{{ text_months }} ', '{{ text_weeks }}', '{{ text_days }}', '{{ text_hrs }}', '{{ text_mins }}', '{{ text_secs }}'],*/
/* 						labels1: ['{{ text_year }}', '{{ text_month }} ', '{{ text_week }}', '{{ text_day }}', '{{ text_hr }}', '{{ text_min }}', '{{ text_sec }}'],*/
/* 						});*/
/* 						 //$('#Countdown{{ product.product_id }}-{{ i }}').countdown('pause');*/
/* 						});*/
/* 						</script>*/
/* 						{% endif %}*/
/* 				</div><!-- product-layout -->*/
/*             {% elseif config_module.type == 1 %}*/
/*             <!-- List -->*/
/*             <div class="product-layout list-style ">*/
/* 					<div class="product-thumb transition">*/
/* 						<div class="item">*/
/* 							<div class="item-inner">*/
/* 								<div class="image images-container">*/
/* 									<a href="{{ product.href }}" class="product-image">*/
/* 										{% if config_module.rotator and product.rotator_image %}<img class="img-r" src="{{ product.rotator_image }}" alt="{{ product.name }}" />{% endif %}*/
/* 										<img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive" />*/
/* 									</a>				  */
/* 									{% if config_module.salelabel %}*/
/* 										{% if product.special %}*/
/* 										<div class="label-product label_sale">{{ text_label_sale }}</div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 									{% if config_module.newlabel %}*/
/* 										{% if product.is_new %}*/
/* 										<div class="label-product label_new">{{ text_label_new }}</div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 									{% if config_module.showwishlist or config_module.showquickview or  config_module.showcompare %}	*/
/* 										<div class="action-links">*/
/* 											{% if config_module.showwishlist %}*/
/* 												<button class="button btn-wishlist" type="button"  title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><span>{{ button_wishlist }}</span></button>*/
/* 											{% endif %}*/
/* 											{% if config_module.showcompare %}*/
/* 												<button class="button btn-compare" type="button"  title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><span>{{ button_compare }}</span></button>*/
/* 											{% endif %}*/
/* 											*/
/* 											{% if use_quickview %}*/
/* 												{% if config_module.showquickview %}*/
/* 													<button class="button btn-quickview" type="button"  title="{{ button_quickview }}" onclick="ocquickview.ajaxView('{{ product.href }}')"><span>{{ button_quickview }}</span></button>*/
/* 												{% endif %}*/
/* 											{% endif %}*/
/* 										</div>*/
/* 									{% endif %}*/
/* 								</div><!-- image -->*/
/* 								<div class="caption">*/
/* 									{% if product.manufacturer %}*/
/* 									<p class="manufacture-product">*/
/* 										<a href="{{ product.manufacturers }}">{{ product.manufacturer }}</a>*/
/* 									</p>*/
/* 									{% endif %}*/
/* 									{% if product.rating %}*/
/* 									<div class="ratings">*/
/* 										<div class="rating-box">*/
/* 										{% for i in 0..5 %}*/
/* 											{% if product.rating == i %}*/
/* 											{% set class_r = "rating"~i %}*/
/* 											<div class="{{ class_r }}">rating</div>*/
/* 											{% endif %}*/
/* 										{% endfor %}*/
/* 										</div>*/
/* 									</div>					*/
/* 									{% endif %}*/
/* 									<h4 class="product-name"><a href="{{ product.href }}">{{ product.name }}</a></h4> */
/* 									{% if config_module.description %}*/
/* 									<div class="product-des">{{ product.description }}</div>*/
/* 									{% endif %}*/
/* 									{% if use_catalog %}*/
/* 									{% if product.price %}*/
/* 										<div class="price-box">*/
/* 										<label>{{ price_label }}</label>*/
/* 										{% if not product.special %}*/
/* 											<p class="regular-price"><span class="price">{{ product.price }}</span></p>*/
/* 										{% else %}*/
/* 											<p class="special-price"><span class="price">{{ product.special }}</span></p>*/
/* 											<p class="old-price"><span class="price">{{ product.price }}</span></p>						  */
/* 										{% endif %}*/
/* 										{% if product.tax %}*/
/* 											<p class="price-tax"><span class="price">{{ text_tax }} {{ product.tax }}</span></p>*/
/* 										{% endif %}*/
/* 										</div>*/
/* 									{% endif %}*/
/* 									{% endif %}*/
/* 									*/
/* 									{% if config_module.countdown %}<div class="text-hurryup"><p>{{ text_hurryup }}</p></div><div id="Countdown{{ product.product_id }}-{{ i }}" class="box-timer"></div> {% endif %}*/
/* 									{% if use_catalog %}*/
/* 										{% if config_module.showcart %}*/
/* 											<div class="box-hover"><button class="button btn-cart" type="button"  title="{{ button_cart }}" onclick="cart.add('{{ product.product_id }}');"><span>{{ button_cart }}</span></button></div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 								</div><!-- caption -->*/
/* 							</div>*/
/* 						</div>*/
/* 					</div><!-- product-thumb -->*/
/* 						{% if product.date_end and config_module.countdown %}*/
/* 						<script >*/
/* 						$(document).ready(function () {*/
/* 						$('#Countdown{{ product.product_id }}-{{ i }}').countdown({*/
/* 						until: new Date({{ product.date_end|date("Y") }}, {{ product.date_end|date("m") }}-1, {{ product.date_end|date("d") }}, {{ product.date_end|date("H") }}, {{ product.date_end|date("i") }}, {{ product.date_end|date("s") }}),*/
/* 						labels: ['{{ text_years }}', '{{ text_months }} ', '{{ text_weeks }}', '{{ text_days }}', '{{ text_hrs }}', '{{ text_mins }}', '{{ text_secs }}'],*/
/* 						labels1: ['{{ text_year }}', '{{ text_month }} ', '{{ text_week }}', '{{ text_day }}', '{{ text_hr }}', '{{ text_min }}', '{{ text_sec }}'],*/
/* 						});*/
/* 						//$('#Countdown{{ product.product_id }}-{{ i }}').countdown('pause');*/
/* 						});*/
/* 						</script>*/
/* 						{% endif %}*/
/* 				</div><!-- product-layout -->*/
/*             {% else %}*/
/*             <!-- other type -->*/
/*             <div class="product-layout product-customize ">*/
/* 					<div class="product-thumb transition">*/
/* 						<div class="item">*/
/* 							<div class="item-inner">*/
/* 								<div class="row">*/
/* 									<div class="col col1 col-sm-4 col-xs-12">*/
/* 										<div class="caption-top">*/
/* 											<h4 class="product-name"><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/* 											{% if product.manufacturer %}*/
/* 											<p class="manufacture-product">*/
/* 												{{ text_manufacture }}*/
/* 												<a href="{{ product.manufacturers }}">{{ product.manufacturer }}</a>*/
/* 											</p>*/
/* 											{% endif %}*/
/* 											{% if config_module.description %}*/
/* 											<div class="product-des">{{ product.description }}</div>*/
/* 											<a class="read-more" href="{{ product.href }}">{{ text_more }}</a>*/
/* 											{% endif %}*/
/* 										</div>*/
/* 									</div>*/
/* 									<div class="col col2 col-sm-4 col-xs-12">*/
/* 										<div class="caption">*/
/* 											{% if use_catalog %}*/
/* 											{% if product.price %}*/
/* 												<div class="price-box">*/
/* 												<label>{{ price_label }}</label>*/
/* 												{% if not product.special %}*/
/* 													<p class="regular-price"><span class="price">{{ product.price }}</span></p>*/
/* 												{% else %}*/
/* 													<p class="special-price"><span class="price">{{ product.special }}</span></p>*/
/* 													<p class="old-price"><span class="price">{{ product.price }}</span></p>						  */
/* 												{% endif %}*/
/* 												{% if product.tax %}*/
/* 													<p class="price-tax"><span class="price">{{ text_tax }} {{ product.tax }}</span></p>*/
/* 												{% endif %}*/
/* 												</div>*/
/* 											{% endif %}*/
/* 											{% endif %}*/
/* 											{% if product.rating %}*/
/* 											<div class="ratings">*/
/* 												<div class="rating-box">*/
/* 												{% for i in 0..5 %}*/
/* 													{% if product.rating == i %}*/
/* 													{% set class_r = "rating"~i %}*/
/* 													<div class="{{ class_r }}">rating</div>*/
/* 													{% endif %}*/
/* 												{% endfor %}*/
/* 												</div>*/
/* 											</div>					*/
/* 											{% endif %}*/
/* 											{% if config_module.showcart or config_module.showwishlist or  config_module.showcompare %}	*/
/* 												<div class="action-links">*/
/* 													{% if config_module.showcompare %}*/
/* 														<button class="button btn-compare" type="button"  title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><span>{{ button_compare }}</span></button>*/
/* 													{% endif %}*/
/* 													{% if use_catalog %}*/
/* 														{% if config_module.showcart %}*/
/* 															<button class="button btn-cart" type="button"  title="{{ button_cart }}" onclick="cart.add('{{ product.product_id }}');"><span>{{ button_cart }}</span></button>*/
/* 														{% endif %}*/
/* 													{% endif %}*/
/* 													{% if use_quickview %}*/
/* 														{% if config_module.showquickview %}*/
/* 															<button class="button btn-quickview" type="button"  title="{{ button_quickview }}" onclick="ocquickview.ajaxView('{{ product.href }}')"><span>{{ button_quickview }}</span></button>*/
/* 														{% endif %}*/
/* 													{% endif %}*/
/* 													*/
/* 													*/
/* 												</div>*/
/* 											{% endif %}*/
/* 											{% if config_module.countdown %}<div class="text-hurryup"><p>{{ text_hurryup }}</p></div><div id="Countdown{{ product.product_id }}-{{ i }}" class="box-timer"></div> {% endif %}*/
/* 											*/
/* 										</div><!-- caption -->*/
/* 									</div>*/
/* 									<div class="col col3 col-sm-4 col-xs-12">*/
/* 										<div class="image images-container ">*/
/* 											<a href="{{ product.href }}" class="product-image">*/
/* 												{% if config_module.rotator and product.rotator_image %}<img class="img-r" src="{{ product.rotator_image }}" alt="{{ product.name }}" />{% endif %}*/
/* 												<img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive" />*/
/* 											</a>				  */
/* 											{% if config_module.salelabel %}*/
/* 												{% if product.special %}*/
/* 												<div class="label-product label_sale">{{ text_label_sale }}</div>*/
/* 												{% endif %} */
/* 											{% endif %}*/
/* 											{% if config_module.newlabel %}*/
/* 												{% if product.is_new %}*/
/* 												<div class="label-product label_new">{{ text_label_new }}</div>*/
/* 												{% endif %}*/
/* 											{% endif %}*/
/* 											*/
/* 											{% if config_module.showwishlist %}*/
/* 												<button class="button btn-wishlist" type="button"  title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><span>{{ button_wishlist }}</span></button>*/
/* 											{% endif %}*/
/* 										</div><!-- image -->*/
/* 									</div>*/
/* 								</div>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div><!-- product-thumb -->*/
/* 						{% if product.date_end and config_module.countdown %}*/
/* 						<script >*/
/* 						$(document).ready(function () {*/
/* 						$('#Countdown{{ product.product_id }}-{{ i }}').countdown({*/
/* 						until: new Date({{ product.date_end|date("Y") }}, {{ product.date_end|date("m") }}-1, {{ product.date_end|date("d") }}, {{ product.date_end|date("H") }}, {{ product.date_end|date("i") }}, {{ product.date_end|date("s") }}),*/
/* 						labels: ['{{ text_years }}', '{{ text_months }} ', '{{ text_weeks }}', '{{ text_days }}', '{{ text_hrs }}', '{{ text_mins }}', '{{ text_secs }}'],*/
/* 						labels1: ['{{ text_year }}', '{{ text_month }} ', '{{ text_week }}', '{{ text_day }}', '{{ text_hr }}', '{{ text_min }}', '{{ text_sec }}'],*/
/* 						});*/
/* 						 //$('#Countdown{{ product.product_id }}-{{ i }}').countdown('pause');*/
/* 						});*/
/* 						</script>*/
/* 						{% endif %}*/
/* 				</div><!-- product-layout -->*/
/*             {% endif %}*/
/* 				{% if (count % rows == 0) or (count == products|length ) %}*/
/* 				</div>*/
/* 				{% endif %}*/
/*         {% endfor %}	*/
/*     </div>*/
/* 	</div>*/
/* 	{% else %}*/
/* 		<p class="text_empty">{{ text_empty }}</p>*/
/* 	{% endif %}*/
/* 	<div class="clearfix"></div>*/
/* </div>*/
/* {% if config_module.slider %}*/
/*     <script >*/
/*         $(document).ready(function() {*/
/*             $("#product_module{{ config_module.module_id }} .tt-product").owlCarousel({*/
/*                 loop: {% if config_module.loop %} true {% else %} false {% endif %},*/
/*                 margin: {% if config_module.margin is defined %} {{ config_module.margin }} {% else %} 20 {% endif %},*/
/*                 nav: {% if config_module.navigation %} true {% else %} false {% endif %},*/
/*                 dots: {% if config_module.pagination %} true {% else %} false {% endif %},*/
/*                 autoplay:  {% if config_module.auto %} true {% else %} false {% endif %},*/
/*                 autoplayTimeout: {% if config_module.time %} {{ config_module.time }} {% else %} 2000 {% endif %},*/
/*                 autoplayHoverPause: true,*/
/*                 autoplaySpeed: {% if config_module.speed %} {{ config_module.speed }} {% else %} 3000 {% endif %},*/
/*                 navSpeed: {% if config_module.speed %} {{ config_module.speed }} {% else %} 3000 {% endif %},*/
/*                 dotsSpeed: {% if config_module.speed %} {{ config_module.speed }} {% else %} 3000 {% endif %},*/
/* 				lazyLoad: true,*/
/*                 responsive:{*/
/* 					0:{*/
/* 						items: 1,*/
/* 						nav: false*/
/* 					},*/
/* 					480:{*/
/* 						items: {{ config_module.smobile }},*/
/* 						nav: false*/
/* 					},*/
/* 					768:{*/
/* 						items: {{ config_module.mobile }}*/
/* 					},*/
/* 					992:{*/
/* 						items: {{ config_module.tablet }}*/
/* 					},*/
/* 					1200:{*/
/* 						items: {{ config_module.desktop }}*/
/* 					},*/
/* 					1600:{*/
/* 						items: {{ config_module.items }}*/
/* 					}*/
/*                 },*/
/* 				onInitialized: function() {*/
/* 					var count = $("#product_module{{ config_module.module_id }} .tt-product .owl-item.active").length;*/
/* 					if(count == 1) {*/
/* 						$("#product_module{{ config_module.module_id }} .tt-product .owl-item").removeClass('first');*/
/* 						$("#product_module{{ config_module.module_id }} .tt-product .active").addClass('first');*/
/* 					} else {*/
/* 						$("#product_module{{ config_module.module_id }} .tt-product .owl-item").removeClass('first');*/
/* 						$("#product_module{{ config_module.module_id }} .tt-product .owl-item.active:first").addClass('first');*/
/* 					}*/
/* 					*/
/* 				},*/
/* 				onTranslated: function() {*/
/* 					var count = $("#product_module{{ config_module.module_id }} .tt-product .owl-item.active").length;*/
/* 					if(count == 1) {*/
/* 						$("#product_module{{ config_module.module_id }} .tt-product .owl-item").removeClass('first');*/
/* 						$("#product_module{{ config_module.module_id }} .tt-product .active").addClass('first');*/
/* 					} else {*/
/* 						$("#product_module{{ config_module.module_id }} .tt-product .owl-item").removeClass('first');*/
/* 						$("#product_module{{ config_module.module_id }} .tt-product .owl-item.active:first").addClass('first');*/
/* 					}*/
/* 				}*/
/*             });*/
/* 			*/
/*         });*/
/*     </script>*/
/* {% endif %}*/
