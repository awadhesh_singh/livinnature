<?php

/* tt_naturecircle1/template/extension/module/occmsblock.twig */
class __TwigTemplate_7b8e9599b26d4635c7d8bcdbb18e4f1ab80180623837e78592f01746bdc67cb0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->getAttribute($this->getAttribute((isset($context["cmsblocks"]) ? $context["cmsblocks"] : null), 0, array(), "array"), "description", array(), "array");
    }

    public function getTemplateName()
    {
        return "tt_naturecircle1/template/extension/module/occmsblock.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ cmsblocks[0]['description']  }}*/
