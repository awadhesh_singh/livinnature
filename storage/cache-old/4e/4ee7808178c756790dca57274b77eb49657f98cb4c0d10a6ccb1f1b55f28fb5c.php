<?php

/* tt_naturecircle1/template/extension/module/oclayerednavigation/occategory.twig */
class __TwigTemplate_7f3c232d3efbf98f79409449385e4b8c39b66e069bb9235d8dffe656e0edda06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<div class=\"container layer-category\">
    <div class=\"layered-navigation-block\"></div>
    <div class=\"ajax-loader\">
        <img src=\"";
        // line 5
        echo (isset($context["module_oclayerednavigation_loader_img"]) ? $context["module_oclayerednavigation_loader_img"] : null);
        echo "\" alt=\"\" />
    </div>
\t
    <ul class=\"breadcrumb\">
        ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 10
            echo "            <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "    </ul>
    <div class=\"row\"><div class=\"col-order\">";
        // line 13
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
        ";
        // line 14
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 15
            echo "            ";
            $context["class"] = "col-md-6 col-sm-12";
            // line 16
            echo "        ";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 17
            echo "            ";
            $context["class"] = "col-md-9 col-sm-12";
            // line 18
            echo "        ";
        } else {
            // line 19
            echo "            ";
            $context["class"] = "col-sm-12";
            // line 20
            echo "        ";
        }
        // line 21
        echo "        <div id=\"content\" class=\"";
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
\t\t\t<h1 class=\"category-name\">";
        // line 22
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
\t\t\t<div class=\"category-info\">
            
            
\t\t\t";
        // line 26
        if ((isset($context["thumb"]) ? $context["thumb"] : null)) {
            // line 27
            echo "\t\t\t\t<div class=\"box-image\">
\t\t\t\t\t<div class=\"category-image\"><img src=\"";
            // line 28
            echo (isset($context["thumb"]) ? $context["thumb"] : null);
            echo "\" alt=\"";
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "\" title=\"";
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "\" class=\"img-thumbnail\" /></div>
\t\t\t\t</div>
\t\t\t
\t\t\t";
        }
        // line 32
        echo "\t\t\t";
        if ((isset($context["description"]) ? $context["description"] : null)) {
            // line 33
            echo "\t\t\t\t<div class=\"category-des\">";
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "</div>
\t\t\t";
        }
        // line 34
        echo " 
\t\t\t\t
\t\t\t
\t\t\t</div>
            ";
        // line 38
        if ((isset($context["categories"]) ? $context["categories"] : null)) {
            // line 39
            echo "                <h3 class=\"text-refine\">";
            echo (isset($context["text_refine"]) ? $context["text_refine"] : null);
            echo "</h3>
                ";
            // line 40
            if ((twig_length_filter($this->env, (isset($context["categories"]) ? $context["categories"] : null)) <= 5)) {
                // line 41
                echo "                    <div class=\"row\">
                        <div class=\"col-sm-3\">
                            <ul class=\"list-cate\">
                                ";
                // line 44
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 45
                    echo "                                    <li><a href=\"";
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a></li>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 47
                echo "                            </ul>
                        </div>
                    </div>
                ";
            } else {
                // line 51
                echo "                    <div class=\"row\"> ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_array_batch((isset($context["categories"]) ? $context["categories"] : null), twig_round((twig_length_filter($this->env, (isset($context["categories"]) ? $context["categories"] : null)) / 4), 1, "ceil")));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 52
                    echo "                        <div class=\"col-sm-3\">
                            <ul>
                                ";
                    // line 54
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 55
                        echo "                                    <li><a href=\"";
                        echo $this->getAttribute($context["category"], "href", array());
                        echo "\">";
                        echo $this->getAttribute($context["category"], "name", array());
                        echo "</a></li>
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 57
                    echo "                            </ul>
                        </div>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 59
                echo " </div>
                    <br />
                ";
            }
            // line 62
            echo "            ";
        }
        // line 63
        echo "            <div class=\"custom-category\">
                ";
        // line 64
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 65
            echo "                <div><a href=\"";
            echo (isset($context["compare"]) ? $context["compare"] : null);
            echo "\" id=\"compare-total\">";
            echo (isset($context["text_compare"]) ? $context["text_compare"] : null);
            echo "</a></div>
\t\t\t\t
\t\t\t\t<div class=\"toolbar toolbar-products\">
\t\t\t\t\t<div class=\"modes\">
\t\t\t\t\t\t<div class=\"btn-group\">
\t\t\t\t\t\t\t<button type=\"button\" id=\"grid-view\" class=\"btn\"></button>
\t\t\t\t\t\t\t<button type=\"button\" id=\"list-view\" class=\"btn\"></button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"toolbar-amount\">
\t\t\t\t\t\t<span>";
            // line 75
            echo (isset($context["results"]) ? $context["results"] : null);
            echo "</span>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"sorter\">
\t\t\t\t\t\t<label>";
            // line 78
            echo (isset($context["text_sort"]) ? $context["text_sort"] : null);
            echo "</label>
\t\t\t\t\t\t<select id=\"input-sort\" class=\"form-control\" onchange=\"oclayerednavigationajax.filter(this.value)\">
\t\t\t\t\t\t\t";
            // line 80
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["sorts"]);
            foreach ($context['_seq'] as $context["_key"] => $context["sorts"]) {
                // line 81
                echo "\t\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["sorts"], "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
                    // line 82
                    echo "\t\t\t\t\t\t\t\t\t<option value=\"";
                    echo $this->getAttribute($context["sorts"], "href", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["sorts"], "text", array());
                    echo "</option>
\t\t\t\t\t\t\t\t";
                } else {
                    // line 84
                    echo "\t\t\t\t\t\t\t\t\t<option value=\"";
                    echo $this->getAttribute($context["sorts"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["sorts"], "text", array());
                    echo "</option>
\t\t\t\t\t\t\t\t";
                }
                // line 86
                echo "\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sorts'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 87
            echo "\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"limiter\">
\t\t\t\t\t\t<label>";
            // line 90
            echo (isset($context["text_limit"]) ? $context["text_limit"] : null);
            echo "</label>
\t\t\t\t\t\t<select id=\"input-limit\" class=\"form-control\" onchange=\"oclayerednavigationajax.filter(this.value)\">
\t\t\t\t\t\t\t";
            // line 92
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["limits"]);
            foreach ($context['_seq'] as $context["_key"] => $context["limits"]) {
                // line 93
                echo "\t\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["limits"], "value", array()) == (isset($context["limit"]) ? $context["limit"] : null))) {
                    // line 94
                    echo "\t\t\t\t\t\t\t\t\t<option value=\"";
                    echo $this->getAttribute($context["limits"], "href", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["limits"], "text", array());
                    echo "</option>
\t\t\t\t\t\t\t\t";
                } else {
                    // line 96
                    echo "\t\t\t\t\t\t\t\t\t<option value=\"";
                    echo $this->getAttribute($context["limits"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["limits"], "text", array());
                    echo "</option>
\t\t\t\t\t\t\t\t";
                }
                // line 98
                echo "\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['limits'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 99
            echo "\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t</div>
                <div class=\"custom-products\">
                    <div class=\"row\">
\t\t\t\t\t\t";
            // line 104
            $context["count"] = 1;
            // line 105
            echo "\t\t\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 106
                echo "\t\t\t\t\t\t<div class=\"product-layout product-list\">
\t\t\t\t\t\t\t<div class=\"product-thumb\">
\t\t\t\t\t\t\t\t<div class=\"item ";
                // line 108
                echo ("item" . (isset($context["count"]) ? $context["count"] : null));
                echo "\">
\t\t\t\t\t\t\t\t\t<div class=\"item-inner\">\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"col-image\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"image images-container\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 112
                echo $this->getAttribute($context["product"], "href", array());
                echo "\" class=\"product-image\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                // line 113
                echo $this->getAttribute($context["product"], "thumb", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" class=\"img-responsive\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 114
                if ($this->getAttribute($context["product"], "rotator_image", array())) {
                    echo "<img class=\"img-r img-responsive\" src=\"";
                    echo $this->getAttribute($context["product"], "rotator_image", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\"  />";
                }
                // line 115
                echo "\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 116
                if ($this->getAttribute($context["product"], "special", array())) {
                    // line 117
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_sale\">";
                    echo (isset($context["text_label_sale"]) ? $context["text_label_sale"] : null);
                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 119
                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                if ($this->getAttribute($context["product"], "is_new", array())) {
                    // line 120
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_new\">";
                    echo (isset($context["text_label_new"]) ? $context["text_label_new"] : null);
                    echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 122
                echo "\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"action-links\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-wishlist\" type=\"button\"  title=\"";
                // line 125
                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                echo "\" onclick=\"wishlist.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\"><span>";
                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-compare\" type=\"button\"  title=\"";
                // line 126
                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                echo "\" onclick=\"compare.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\"><span>";
                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-quickview\" type=\"button\"  title=\"";
                // line 127
                echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                echo "\" onclick=\"ocquickview.ajaxView('";
                echo $this->getAttribute($context["product"], "href", array());
                echo "')\"><span>";
                echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"col-des\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 134
                if ($this->getAttribute($context["product"], "manufacturer", array())) {
                    // line 135
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"manufacture-product\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    // line 136
                    echo $this->getAttribute($context["product"], "manufacturers", array());
                    echo "\">";
                    echo $this->getAttribute($context["product"], "manufacturer", array());
                    echo "</a>
\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 139
                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                if ($this->getAttribute($context["product"], "rating", array())) {
                    // line 140
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"ratings\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"rating-box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 142
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(0, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 143
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        if (($this->getAttribute($context["product"], "rating", array()) == $context["i"])) {
                            // line 144
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            $context["class_r"] = ("rating" . $context["i"]);
                            // line 145
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"";
                            echo (isset($context["class_r"]) ? $context["class_r"] : null);
                            echo "\">rating</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 147
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 148
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 150
                echo "\t
\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"product-name\"><a href=\"";
                // line 151
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["product"], "name", array());
                echo "</a></h4>
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 152
                if ($this->getAttribute($context["product"], "price", array())) {
                    // line 153
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"price-box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<label>";
                    // line 154
                    echo (isset($context["price_label"]) ? $context["price_label"] : null);
                    echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 155
                    if ( !$this->getAttribute($context["product"], "special", array())) {
                        // line 156
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"regular-price\"><span class=\"price\">";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "</span></p>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 158
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"special-price\"><span class=\"price\">";
                        echo $this->getAttribute($context["product"], "special", array());
                        echo "</span></p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"old-price\"><span class=\"price\">";
                        // line 159
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "</span></p>\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 161
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    if ($this->getAttribute($context["product"], "tax", array())) {
                        // line 162
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"price-tax\"><span class=\"price\">";
                        echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                        echo " ";
                        echo $this->getAttribute($context["product"], "tax", array());
                        echo "</span></p>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 164
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 166
                echo "\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"product-des\">";
                // line 167
                echo $this->getAttribute($context["product"], "description", array());
                echo "</p>
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"box-hover\"><button class=\"button btn-cart\" type=\"button\"  title=\"";
                // line 169
                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                echo "\" onclick=\"cart.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "', '";
                echo $this->getAttribute($context["product"], "minimum", array());
                echo "')\"><span>";
                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                echo "</span></button></div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<script >
\t\t\t\t\t\t\t\t\$(document).ready(function() {
\t\t\t\t\t\t\t\t\t\$(\".product-list .item";
                // line 177
                echo (isset($context["count"]) ? $context["count"] : null);
                echo " .product-name\").prependTo(\$('.product-list .item";
                echo (isset($context["count"]) ? $context["count"] : null);
                echo " .caption'));
\t\t\t\t\t\t\t\t\t\$('#grid-view').click(function() {
\t\t\t\t\t\t\t\t\t\t\$(\".item";
                // line 179
                echo (isset($context["count"]) ? $context["count"] : null);
                echo " .ratings\").prependTo(\$('.item";
                echo (isset($context["count"]) ? $context["count"] : null);
                echo " .caption'));
\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t\t\$('#list-view').click(function() {
\t\t\t\t\t\t\t\t\t\t\$(\".item";
                // line 182
                echo (isset($context["count"]) ? $context["count"] : null);
                echo " .product-name\").prependTo(\$('.item";
                echo (isset($context["count"]) ? $context["count"] : null);
                echo " .caption'));
\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                // line 187
                $context["count"] = ((isset($context["count"]) ? $context["count"] : null) + 1);
                // line 188
                echo "\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 189
            echo "\t\t\t\t\t</div>
                </div>
                <div class=\"toolbar toolbar-products toolbar-bottom\">
                    <div class=\"toolbar-amount\"><span>";
            // line 192
            echo (isset($context["results"]) ? $context["results"] : null);
            echo "</span></div>
\t\t\t\t\t<div class=\"pages\">";
            // line 193
            echo (isset($context["pagination"]) ? $context["pagination"] : null);
            echo "</div>
                </div>
                ";
        }
        // line 196
        echo "                ";
        if (( !(isset($context["categories"]) ? $context["categories"] : null) &&  !(isset($context["products"]) ? $context["products"] : null))) {
            // line 197
            echo "                    <p class=\"text_empty\">";
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
                    <div class=\"buttons\">
                        <div class=\"pull-right\"><a href=\"";
            // line 199
            echo (isset($context["continue"]) ? $context["continue"] : null);
            echo "\" class=\"btn btn-primary\">";
            echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
            echo "</a></div>
                    </div>
                ";
        }
        // line 202
        echo "            </div>
            ";
        // line 203
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
        ";
        // line 204
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div></div>
</div>
";
        // line 206
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "tt_naturecircle1/template/extension/module/oclayerednavigation/occategory.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  597 => 206,  592 => 204,  588 => 203,  585 => 202,  577 => 199,  571 => 197,  568 => 196,  562 => 193,  558 => 192,  553 => 189,  547 => 188,  545 => 187,  535 => 182,  527 => 179,  520 => 177,  503 => 169,  498 => 167,  495 => 166,  491 => 164,  483 => 162,  480 => 161,  475 => 159,  470 => 158,  464 => 156,  462 => 155,  458 => 154,  455 => 153,  453 => 152,  447 => 151,  444 => 150,  439 => 148,  433 => 147,  427 => 145,  424 => 144,  421 => 143,  417 => 142,  413 => 140,  410 => 139,  402 => 136,  399 => 135,  397 => 134,  383 => 127,  375 => 126,  367 => 125,  362 => 122,  356 => 120,  353 => 119,  347 => 117,  345 => 116,  342 => 115,  332 => 114,  324 => 113,  320 => 112,  313 => 108,  309 => 106,  304 => 105,  302 => 104,  295 => 99,  289 => 98,  281 => 96,  273 => 94,  270 => 93,  266 => 92,  261 => 90,  256 => 87,  250 => 86,  242 => 84,  234 => 82,  231 => 81,  227 => 80,  222 => 78,  216 => 75,  200 => 65,  198 => 64,  195 => 63,  192 => 62,  187 => 59,  179 => 57,  168 => 55,  164 => 54,  160 => 52,  155 => 51,  149 => 47,  138 => 45,  134 => 44,  129 => 41,  127 => 40,  122 => 39,  120 => 38,  114 => 34,  108 => 33,  105 => 32,  94 => 28,  91 => 27,  89 => 26,  82 => 22,  75 => 21,  72 => 20,  69 => 19,  66 => 18,  63 => 17,  60 => 16,  57 => 15,  55 => 14,  51 => 13,  48 => 12,  37 => 10,  33 => 9,  26 => 5,  19 => 1,);
    }
}
/* {{ header }}*/
/* <div class="container layer-category">*/
/*     <div class="layered-navigation-block"></div>*/
/*     <div class="ajax-loader">*/
/*         <img src="{{ module_oclayerednavigation_loader_img }}" alt="" />*/
/*     </div>*/
/* 	*/
/*     <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*             <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*     </ul>*/
/*     <div class="row"><div class="col-order">{{ column_left }}*/
/*         {% if column_left and column_right %}*/
/*             {% set class = 'col-md-6 col-sm-12' %}*/
/*         {% elseif column_left or column_right %}*/
/*             {% set class = 'col-md-9 col-sm-12' %}*/
/*         {% else %}*/
/*             {% set class = 'col-sm-12' %}*/
/*         {% endif %}*/
/*         <div id="content" class="{{ class }}">{{ content_top }}*/
/* 			<h1 class="category-name">{{ heading_title }}</h1>*/
/* 			<div class="category-info">*/
/*             */
/*             */
/* 			{% if thumb %}*/
/* 				<div class="box-image">*/
/* 					<div class="category-image"><img src="{{ thumb }}" alt="{{ heading_title }}" title="{{ heading_title }}" class="img-thumbnail" /></div>*/
/* 				</div>*/
/* 			*/
/* 			{% endif %}*/
/* 			{% if description %}*/
/* 				<div class="category-des">{{ description }}</div>*/
/* 			{% endif %} */
/* 				*/
/* 			*/
/* 			</div>*/
/*             {% if categories %}*/
/*                 <h3 class="text-refine">{{ text_refine }}</h3>*/
/*                 {% if categories|length <= 5 %}*/
/*                     <div class="row">*/
/*                         <div class="col-sm-3">*/
/*                             <ul class="list-cate">*/
/*                                 {% for category in categories %}*/
/*                                     <li><a href="{{ category.href }}">{{ category.name }}</a></li>*/
/*                                 {% endfor %}*/
/*                             </ul>*/
/*                         </div>*/
/*                     </div>*/
/*                 {% else %}*/
/*                     <div class="row"> {% for category in categories|batch((categories|length / 4)|round(1, 'ceil')) %}*/
/*                         <div class="col-sm-3">*/
/*                             <ul>*/
/*                                 {% for category in categories %}*/
/*                                     <li><a href="{{ category.href }}">{{ category.name }}</a></li>*/
/*                                 {% endfor %}*/
/*                             </ul>*/
/*                         </div>*/
/*                         {% endfor %} </div>*/
/*                     <br />*/
/*                 {% endif %}*/
/*             {% endif %}*/
/*             <div class="custom-category">*/
/*                 {% if products %}*/
/*                 <div><a href="{{ compare }}" id="compare-total">{{ text_compare }}</a></div>*/
/* 				*/
/* 				<div class="toolbar toolbar-products">*/
/* 					<div class="modes">*/
/* 						<div class="btn-group">*/
/* 							<button type="button" id="grid-view" class="btn"></button>*/
/* 							<button type="button" id="list-view" class="btn"></button>*/
/* 						</div>*/
/* 					</div>*/
/* 					<div class="toolbar-amount">*/
/* 						<span>{{ results }}</span>*/
/* 					</div>*/
/* 					<div class="sorter">*/
/* 						<label>{{ text_sort }}</label>*/
/* 						<select id="input-sort" class="form-control" onchange="oclayerednavigationajax.filter(this.value)">*/
/* 							{% for sorts in sorts %}*/
/* 								{% if sorts.value == '%s-%s'|format(sort, order) %}*/
/* 									<option value="{{ sorts.href }}" selected="selected">{{ sorts.text }}</option>*/
/* 								{% else %}*/
/* 									<option value="{{ sorts.href }}">{{ sorts.text }}</option>*/
/* 								{% endif %}*/
/* 							{% endfor %}*/
/* 						</select>*/
/* 					</div>*/
/* 					<div class="limiter">*/
/* 						<label>{{ text_limit }}</label>*/
/* 						<select id="input-limit" class="form-control" onchange="oclayerednavigationajax.filter(this.value)">*/
/* 							{% for limits in limits %}*/
/* 								{% if limits.value == limit %}*/
/* 									<option value="{{ limits.href }}" selected="selected">{{ limits.text }}</option>*/
/* 								{% else %}*/
/* 									<option value="{{ limits.href }}">{{ limits.text }}</option>*/
/* 								{% endif %}*/
/* 							{% endfor %}*/
/* 						</select>*/
/* 					</div>*/
/* 				</div>*/
/*                 <div class="custom-products">*/
/*                     <div class="row">*/
/* 						{% 	set count = 1 %}*/
/* 						{% 	for product in products %}*/
/* 						<div class="product-layout product-list">*/
/* 							<div class="product-thumb">*/
/* 								<div class="item {{ 'item' ~ count  }}">*/
/* 									<div class="item-inner">		*/
/* 										<div class="col-image">*/
/* 											<div class="image images-container">*/
/* 												<a href="{{ product.href }}" class="product-image">*/
/* 													<img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive" />*/
/* 													{% if product.rotator_image %}<img class="img-r img-responsive" src="{{ product.rotator_image }}" alt="{{ product.name }}" title="{{ product.name }}"  />{% endif %}*/
/* 												</a>*/
/* 												{% if product.special %}*/
/* 													<div class="label-product label_sale">{{  text_label_sale }}</div>*/
/* 												{% endif %}*/
/* 												{% if product.is_new %}*/
/* 													<div class="label-product label_new">{{ text_label_new }}</div>*/
/* 												{% endif %}*/
/* 												*/
/* 												*/
/* 												<div class="action-links">*/
/* 													<button class="button btn-wishlist" type="button"  title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><span>{{ button_wishlist }}</span></button>*/
/* 													<button class="button btn-compare" type="button"  title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><span>{{ button_compare }}</span></button>*/
/* 													<button class="button btn-quickview" type="button"  title="{{ button_quickview }}" onclick="ocquickview.ajaxView('{{ product.href }}')"><span>{{ button_quickview }}</span></button>*/
/* 												</div>*/
/* 											*/
/* 											</div>*/
/* 										</div>*/
/* 										<div class="col-des">*/
/* 											<div class="caption">*/
/* 												{% if product.manufacturer %}*/
/* 												<p class="manufacture-product">*/
/* 													<a href="{{ product.manufacturers }}">{{ product.manufacturer }}</a>*/
/* 												</p>*/
/* 												{% endif %}*/
/* 												{% if product.rating %}*/
/* 												<div class="ratings">*/
/* 													<div class="rating-box">*/
/* 														{% for i in 0..5 %}*/
/* 															{% if product.rating == i %}*/
/* 															{% set class_r = "rating"~i %}*/
/* 															<div class="{{ class_r }}">rating</div>*/
/* 															{% endif %}*/
/* 														{% endfor %}*/
/* 														</div>*/
/* 													</div>					*/
/* 												{% endif %}	*/
/* 												<h4 class="product-name"><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/* 												{% if product.price %}*/
/* 													<div class="price-box">*/
/* 													<label>{{ price_label }}</label>*/
/* 													{% if not product.special %}*/
/* 														<p class="regular-price"><span class="price">{{ product.price }}</span></p>*/
/* 													{% else %}*/
/* 														<p class="special-price"><span class="price">{{ product.special }}</span></p>*/
/* 														<p class="old-price"><span class="price">{{ product.price }}</span></p>						  */
/* 													{% endif %}*/
/* 													{% if product.tax %}*/
/* 														<p class="price-tax"><span class="price">{{ text_tax }} {{ product.tax }}</span></p>*/
/* 													{% endif %}*/
/* 													</div>*/
/* 												{% endif %}*/
/* 												*/
/* 												<p class="product-des">{{ product.description }}</p>*/
/* 												*/
/* 												<div class="box-hover"><button class="button btn-cart" type="button"  title="{{ button_cart }}" onclick="cart.add('{{ product.product_id }}', '{{ product.minimum }}')"><span>{{ button_cart }}</span></button></div>*/
/* 											</div>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 							</div>*/
/* 							<script >*/
/* 								$(document).ready(function() {*/
/* 									$(".product-list .item{{ count }} .product-name").prependTo($('.product-list .item{{ count }} .caption'));*/
/* 									$('#grid-view').click(function() {*/
/* 										$(".item{{ count }} .ratings").prependTo($('.item{{ count }} .caption'));*/
/* 									});*/
/* 									$('#list-view').click(function() {*/
/* 										$(".item{{ count }} .product-name").prependTo($('.item{{ count }} .caption'));*/
/* 									});*/
/* 								});*/
/* 							</script>*/
/* 						</div>*/
/* 						{% set count = count + 1 %}*/
/* 						{% endfor %}*/
/* 					</div>*/
/*                 </div>*/
/*                 <div class="toolbar toolbar-products toolbar-bottom">*/
/*                     <div class="toolbar-amount"><span>{{ results }}</span></div>*/
/* 					<div class="pages">{{ pagination }}</div>*/
/*                 </div>*/
/*                 {% endif %}*/
/*                 {% if not categories and not products %}*/
/*                     <p class="text_empty">{{ text_empty }}</p>*/
/*                     <div class="buttons">*/
/*                         <div class="pull-right"><a href="{{ continue }}" class="btn btn-primary">{{ button_continue }}</a></div>*/
/*                     </div>*/
/*                 {% endif %}*/
/*             </div>*/
/*             {{ content_bottom }}</div>*/
/*         {{ column_right }}</div></div>*/
/* </div>*/
/* {{ footer }}*/
/* */
