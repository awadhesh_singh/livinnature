<?php

/* tt_naturecircle1/template/extension/module/newslettersubscribe.twig */
class __TwigTemplate_4cf8350d308fbef0eab5ba07b6f1358aa25ef7251f07f7fa52f5e778898e10b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"newletter-subscribe-container\">
<div class=\"container-inner\">
<div class=\"newletter-subscribe\">
\t<div id=\"boxes-normal\" class=\"newletter-container\">
\t\t<div style=\"\" id=\"dialog-normal\" class=\"window\">
\t\t\t<div class=\"box\">
\t\t\t\t\t\t<div class=\"newletter-title\">
\t\t\t\t\t\t\t<h3>";
        // line 8
        echo (isset($context["heading_title2"]) ? $context["heading_title2"] : null);
        echo "</h3>
\t\t\t\t\t\t\t<label>";
        // line 9
        echo (isset($context["newletter_des2"]) ? $context["newletter_des2"] : null);
        echo "</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"box-content newleter-content\">
\t\t\t\t\t\t\t<div id=\"frm_subscribe-normal\">
\t\t\t\t\t\t\t\t<form name=\"subscribe\" id=\"subscribe-normal\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" value=\"\" name=\"subscribe_email\" id=\"subscribe_email-normal\" placeholder=\"";
        // line 14
        echo (isset($context["entry_email"]) ? $context["entry_email"] : null);
        echo "\">
\t\t\t\t\t\t\t\t\t<input type=\"hidden\" value=\"\" name=\"subscribe_name\" id=\"subscribe_name\" />
\t\t\t\t\t\t\t\t\t<a class=\"btn\" onclick=\"email_subscribe()\" title=\"";
        // line 16
        echo (isset($context["entry_button"]) ? $context["entry_button"] : null);
        echo "\"><span>";
        echo (isset($context["entry_button"]) ? $context["entry_button"] : null);
        echo "</span></a>
\t\t\t\t\t\t\t\t\t";
        // line 17
        if ((isset($context["option_unsubscribe"]) ? $context["option_unsubscribe"] : null)) {
            // line 18
            echo "\t\t\t\t\t\t\t\t\t\t<a class=\"btn\" onclick=\"email_unsubscribe()\" title=\"";
            echo (isset($context["entry_unbutton"]) ? $context["entry_unbutton"] : null);
            echo "\"><span>";
            echo (isset($context["entry_unbutton"]) ? $context["entry_unbutton"] : null);
            echo "</span></a>
\t\t\t\t\t\t\t\t\t";
        }
        // line 19
        echo "   
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t</div><!-- /#frm_subscribe -->
\t\t\t\t\t\t\t<div id=\"notification-normal\"></div>
\t\t\t\t\t\t</div><!-- /.box-content -->
\t\t\t</div>
\t\t</div>
<script >
function email_subscribe(){
\t\$.ajax({
\t\ttype: 'post',
\t\turl: 'index.php?route=extension/module/newslettersubscribe/subscribe',
\t\tdataType: 'html',
\t\tdata:\$(\"#subscribe-normal\").serialize(),
\t\tsuccess: function (html) {
\t\t\ttry {
\t\t\t\teval(html);
\t\t\t} 
\t\t\tcatch (e) {
\t\t\t}\t\t\t\t
\t\t}});
}
function email_unsubscribe(){
\t\$.ajax({
\t\ttype: 'post',
\t\turl: 'index.php?route=extension/module/newslettersubscribe/unsubscribe',
\t\tdataType: 'html',
\t\tdata:\$(\"#subscribe\").serialize(),
\t\tsuccess: function (html) {
\t\t\ttry {
\t\t\t\teval(html);
\t\t\t} catch (e) {
\t\t\t}
\t\t}}); 
\t\$('html, body').delay( 1500 ).animate({ scrollTop: 0 }, 'slow'); 
}
</script>
<script >
    \$(document).ready(function() {
\t\t\$('#subscribe_email').keypress(function(e) {
            if(e.which == 13) {
                e.preventDefault();
                email_subscribe();
            }
\t\t\tvar name= \$(this).val();
\t\t  \t\$('#subscribe_name').val(name);
        });
\t\t\$('#subscribe_email').change(function() {
\t\t var name= \$(this).val();
\t\t  \t\t\$('#subscribe_name').val(name);
\t\t});
    });
</script>
</div>
</div>
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "tt_naturecircle1/template/extension/module/newslettersubscribe.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 19,  53 => 18,  51 => 17,  45 => 16,  40 => 14,  32 => 9,  28 => 8,  19 => 1,);
    }
}
/* <div class="newletter-subscribe-container">*/
/* <div class="container-inner">*/
/* <div class="newletter-subscribe">*/
/* 	<div id="boxes-normal" class="newletter-container">*/
/* 		<div style="" id="dialog-normal" class="window">*/
/* 			<div class="box">*/
/* 						<div class="newletter-title">*/
/* 							<h3>{{ heading_title2 }}</h3>*/
/* 							<label>{{ newletter_des2 }}</label>*/
/* 						</div>*/
/* 						<div class="box-content newleter-content">*/
/* 							<div id="frm_subscribe-normal">*/
/* 								<form name="subscribe" id="subscribe-normal">*/
/* 									<input type="text" value="" name="subscribe_email" id="subscribe_email-normal" placeholder="{{ entry_email }}">*/
/* 									<input type="hidden" value="" name="subscribe_name" id="subscribe_name" />*/
/* 									<a class="btn" onclick="email_subscribe()" title="{{ entry_button }}"><span>{{ entry_button }}</span></a>*/
/* 									{% if option_unsubscribe %}*/
/* 										<a class="btn" onclick="email_unsubscribe()" title="{{ entry_unbutton }}"><span>{{ entry_unbutton }}</span></a>*/
/* 									{% endif %}   */
/* 								</form>*/
/* 							</div><!-- /#frm_subscribe -->*/
/* 							<div id="notification-normal"></div>*/
/* 						</div><!-- /.box-content -->*/
/* 			</div>*/
/* 		</div>*/
/* <script >*/
/* function email_subscribe(){*/
/* 	$.ajax({*/
/* 		type: 'post',*/
/* 		url: 'index.php?route=extension/module/newslettersubscribe/subscribe',*/
/* 		dataType: 'html',*/
/* 		data:$("#subscribe-normal").serialize(),*/
/* 		success: function (html) {*/
/* 			try {*/
/* 				eval(html);*/
/* 			} */
/* 			catch (e) {*/
/* 			}				*/
/* 		}});*/
/* }*/
/* function email_unsubscribe(){*/
/* 	$.ajax({*/
/* 		type: 'post',*/
/* 		url: 'index.php?route=extension/module/newslettersubscribe/unsubscribe',*/
/* 		dataType: 'html',*/
/* 		data:$("#subscribe").serialize(),*/
/* 		success: function (html) {*/
/* 			try {*/
/* 				eval(html);*/
/* 			} catch (e) {*/
/* 			}*/
/* 		}}); */
/* 	$('html, body').delay( 1500 ).animate({ scrollTop: 0 }, 'slow'); */
/* }*/
/* </script>*/
/* <script >*/
/*     $(document).ready(function() {*/
/* 		$('#subscribe_email').keypress(function(e) {*/
/*             if(e.which == 13) {*/
/*                 e.preventDefault();*/
/*                 email_subscribe();*/
/*             }*/
/* 			var name= $(this).val();*/
/* 		  	$('#subscribe_name').val(name);*/
/*         });*/
/* 		$('#subscribe_email').change(function() {*/
/* 		 var name= $(this).val();*/
/* 		  		$('#subscribe_name').val(name);*/
/* 		});*/
/*     });*/
/* </script>*/
/* </div>*/
/* </div>*/
/* </div>*/
/* </div>*/
