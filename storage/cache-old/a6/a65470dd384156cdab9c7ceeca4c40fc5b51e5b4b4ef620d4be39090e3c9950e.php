<?php

/* tt_naturecircle1/template/extension/module/octabproducts.twig */
class __TwigTemplate_87b29f26bbc7ad6a3f0fa3ae0002d311c6352213452f8fe238573b630edbe8a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"tt_tabsproduct_module";
        if (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "row", array()) >= 2)) {
            echo " multi-rows";
        }
        echo " ";
        echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "class", array());
        echo "\" id=\"product_module";
        echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
        echo "\">
\t<div class=\"module-title\">
\t  
\t  <h2>
\t\t";
        // line 5
        if ((isset($context["text_store1"]) ? $context["text_store1"] : null)) {
            // line 6
            echo "\t\t  <span>";
            echo (isset($context["text_store1"]) ? $context["text_store1"] : null);
            echo "</span>
\t\t";
        }
        // line 8
        echo "\t\t";
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "title_lang", array()), (isset($context["code"]) ? $context["code"] : null), array(), "array"), "title", array())) {
            // line 9
            echo "\t\t  ";
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "title_lang", array()), (isset($context["code"]) ? $context["code"] : null), array(), "array"), "title", array());
            echo "
\t\t";
        } else {
            // line 11
            echo "\t\t  ";
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "
\t\t";
        }
        // line 13
        echo "\t  </h2>
\t  ";
        // line 14
        if ((isset($context["module_description"]) ? $context["module_description"] : null)) {
            // line 15
            echo "\t\t<div class=\"module-description\">
\t\t  ";
            // line 16
            echo (isset($context["module_description"]) ? $context["module_description"] : null);
            echo "
\t\t</div>
\t  ";
        }
        // line 19
        echo "\t
\t<div class=\"clearfix\"></div>  
  </div>
  <ul class=\"tab-heading tabs-categorys\">
\t\t  ";
        // line 23
        $context["i"] = 0;
        // line 24
        echo "\t\t  ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["octabs"]) ? $context["octabs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["octab"]) {
            // line 25
            echo "\t\t\t<li>
\t\t\t\t<a data-toggle=\"pill\" href=\"#tab-";
            // line 26
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo (isset($context["i"]) ? $context["i"] : null);
            echo "\">
\t\t\t\t\t";
            // line 27
            if (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "type", array()) == 2)) {
                echo "\t
\t\t\t\t\t\t";
                // line 28
                if ($this->getAttribute($context["octab"], "thumbnail_image", array())) {
                    // line 29
                    echo "\t\t\t\t\t\t\t<span class=\"wrapper-thumb\"><img class=\"thumb-img\" src=\"";
                    echo $this->getAttribute($context["octab"], "thumbnail_image", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["octab"], "title", array());
                    echo "\" /></span>
\t\t\t\t\t\t";
                }
                // line 31
                echo "\t\t\t\t\t";
            }
            // line 32
            echo "\t\t\t\t\t<span>";
            echo $this->getAttribute($context["octab"], "title", array());
            echo "</span>
\t\t\t\t</a>
\t\t\t</li>
\t\t\t";
            // line 35
            $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
            // line 36
            echo "\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['octab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "\t</ul>
\t<div class=\"clearfix\"></div> 
  ";
        // line 39
        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "slider", array())) {
            // line 40
            echo "\t";
            $context["class_slider"] = " owl-carousel owl-theme ";
            // line 41
            echo "  ";
        } else {
            // line 42
            echo "\t";
            $context["class_slider"] = "";
            // line 43
            echo "  ";
        }
        // line 44
        echo "  ";
        if (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "nrow", array()) == 0)) {
            // line 45
            echo "\t";
            $context["class"] = "two_items col-lg-6 col-md-6 col-sm-6 col-xs-12";
            echo "\t
  ";
        } elseif (($this->getAttribute(        // line 46
(isset($context["config_module"]) ? $context["config_module"] : null), "nrow", array()) == 1)) {
            // line 47
            echo "\t";
            $context["class"] = "three_items col-lg-4 col-md-4 col-sm-4 col-xs-12";
            // line 48
            echo "  ";
        } elseif (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "nrow", array()) == 2)) {
            // line 49
            echo "\t";
            $context["class"] = "four_items col-lg-3 col-md-3 col-sm-3 col-xs-12";
            // line 50
            echo "  ";
        } else {
            // line 51
            echo "\t";
            $context["class"] = "six_items col-lg-2 col-md-2 col-sm-2 col-xs-12";
            // line 52
            echo "  ";
        }
        // line 53
        echo "  <div class=\"box-style\">
\t<div class=\"owl-container\">
  <div class=\"tt-product \">
    <div class=\"tab-content\">
\t\t";
        // line 57
        $context["i"] = 0;
        echo "\t  
\t\t";
        // line 58
        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "row", array())) {
            // line 59
            echo "\t\t\t";
            $context["rows"] = $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "row", array());
            // line 60
            echo "\t\t";
        } else {
            // line 61
            echo "\t\t\t";
            $context["rows"] = 1;
            // line 62
            echo "\t\t";
        }
        echo "\t\t
\t";
        // line 63
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["octabs"]) ? $context["octabs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["octab"]) {
            // line 64
            echo "\t";
            list($context["count"], $context["count_i"]) =             array(0, 0);
            // line 65
            echo "\t";
            list($context["items_numb"], $context["items_gnumb"]) =             array(3, 6);
            // line 66
            echo "\t<div class=\"tab-container-";
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " ";
            echo (isset($context["class_slider"]) ? $context["class_slider"] : null);
            echo " tab-pane fade\" id=\"tab-";
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo (isset($context["i"]) ? $context["i"] : null);
            echo "\">\t\t
\t";
            // line 67
            if ((twig_length_filter($this->env, $this->getAttribute($context["octab"], "products", array())) > 0)) {
                // line 68
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["octab"], "products", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    // line 69
                    echo "\t\t\t";
                    if ( !$this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "slider", array())) {
                        $context["rows"] = 1;
                    }
                    // line 70
                    echo "            <!-- Grid -->
\t\t\t";
                    // line 71
                    if (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "type", array()) == 0)) {
                        echo "\t
\t\t\t";
                        // line 72
                        if ((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0)) {
                            // line 73
                            echo "\t\t\t<div class=\"row_items ";
                            if ( !$this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "slider", array())) {
                                echo (isset($context["class"]) ? $context["class"] : null);
                            }
                            echo "\">
\t\t\t";
                        }
                        // line 75
                        echo "\t\t\t";
                        $context["count"] = ((isset($context["count"]) ? $context["count"] : null) + 1);
                        // line 76
                        echo "\t\t\t\t\t
\t\t\t\t<div class=\"product-layout grid-style \">
\t\t\t\t\t<div class=\"product-thumb transition\">
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"item-inner\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"image images-container\">
\t\t\t\t\t\t\t\t\t<a href=\"";
                        // line 83
                        echo $this->getAttribute($context["product"], "href", array());
                        echo "\" class=\"product-image\">
\t\t\t\t\t\t\t\t\t\t";
                        // line 84
                        if (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "rotator", array()) && $this->getAttribute($context["product"], "rotator_image", array()))) {
                            echo "<img class=\"img-r\" src=\"";
                            echo $this->getAttribute($context["product"], "rotator_image", array());
                            echo "\" alt=\"";
                            echo $this->getAttribute($context["product"], "name", array());
                            echo "\" />";
                        }
                        // line 85
                        echo "\t\t\t\t\t\t\t\t\t\t<img src=\"";
                        echo $this->getAttribute($context["product"], "thumb", array());
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" title=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" class=\"img-responsive\" />
\t\t\t\t\t\t\t\t\t</a>\t\t\t\t  
\t\t\t\t\t\t\t\t\t";
                        // line 87
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "salelabel", array())) {
                            // line 88
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($context["product"], "special", array())) {
                                // line 89
                                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_sale\">";
                                echo (isset($context["text_label_sale"]) ? $context["text_label_sale"] : null);
                                echo "</div>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 91
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        // line 92
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "newlabel", array())) {
                            // line 93
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($context["product"], "is_new", array())) {
                                // line 94
                                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_new\">";
                                echo (isset($context["text_label_new"]) ? $context["text_label_new"] : null);
                                echo "</div>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 96
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        // line 97
                        echo "\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                        // line 98
                        if ((($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showwishlist", array()) || $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showquickview", array())) || $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcompare", array()))) {
                            echo "\t
\t\t\t\t\t\t\t\t\t\t<div class=\"action-links\">
\t\t\t\t\t\t\t\t\t\t\t";
                            // line 100
                            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showwishlist", array())) {
                                // line 101
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-wishlist\" type=\"button\"  title=\"";
                                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                                echo "\" onclick=\"wishlist.add('";
                                echo $this->getAttribute($context["product"], "product_id", array());
                                echo "');\"><span>";
                                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 103
                            echo "\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcompare", array())) {
                                // line 104
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-compare\" type=\"button\"  title=\"";
                                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                                echo "\" onclick=\"compare.add('";
                                echo $this->getAttribute($context["product"], "product_id", array());
                                echo "');\"><span>";
                                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 106
                            echo "\t\t\t\t\t\t\t\t\t\t\t";
                            if ((isset($context["use_quickview"]) ? $context["use_quickview"] : null)) {
                                // line 107
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                                if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showquickview", array())) {
                                    // line 108
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-quickview\" type=\"button\"  title=\"";
                                    echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                                    echo "\" onclick=\"ocquickview.ajaxView('";
                                    echo $this->getAttribute($context["product"], "href", array());
                                    echo "')\"><span>";
                                    echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                                    echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 110
                                echo "\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 111
                            echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 114
                        echo "\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div><!-- image -->
\t\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                        // line 118
                        if ($this->getAttribute($context["product"], "manufacturer", array())) {
                            // line 119
                            echo "\t\t\t\t\t\t\t\t\t<p class=\"manufacture-product\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
                            // line 120
                            echo $this->getAttribute($context["product"], "manufacturers", array());
                            echo "\">";
                            echo $this->getAttribute($context["product"], "manufacturer", array());
                            echo "</a>
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 123
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["product"], "rating", array())) {
                            // line 124
                            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"ratings\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"rating-box\">
\t\t\t\t\t\t\t\t\t\t\t";
                            // line 126
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(range(0, 5));
                            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                                // line 127
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                                if (($this->getAttribute($context["product"], "rating", array()) == $context["i"])) {
                                    // line 128
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                                    $context["class_r"] = ("rating" . $context["i"]);
                                    // line 129
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"";
                                    echo (isset($context["class_r"]) ? $context["class_r"] : null);
                                    echo "\">rating</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 131
                                echo "\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 132
                            echo "\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 134
                        echo "\t
\t\t\t\t\t\t\t\t\t<h4 class=\"product-name\"><a href=\"";
                        // line 135
                        echo $this->getAttribute($context["product"], "href", array());
                        echo "\">";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "</a></h4>
\t\t\t\t\t\t\t\t\t";
                        // line 136
                        if ((isset($context["use_catalog"]) ? $context["use_catalog"] : null)) {
                            // line 137
                            echo "\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($context["product"], "price", array())) {
                                // line 138
                                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"price-box\">
\t\t\t\t\t\t\t\t\t\t<label>";
                                // line 139
                                echo (isset($context["price_label"]) ? $context["price_label"] : null);
                                echo "</label>
\t\t\t\t\t\t\t\t\t\t";
                                // line 140
                                if ( !$this->getAttribute($context["product"], "special", array())) {
                                    // line 141
                                    echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"regular-price\"><span class=\"price\">";
                                    echo $this->getAttribute($context["product"], "price", array());
                                    echo "</span></p>
\t\t\t\t\t\t\t\t\t\t";
                                } else {
                                    // line 143
                                    echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"special-price\"><span class=\"price\">";
                                    echo $this->getAttribute($context["product"], "special", array());
                                    echo "</span></p>
\t\t\t\t\t\t\t\t\t\t\t<p class=\"old-price\"><span class=\"price\">";
                                    // line 144
                                    echo $this->getAttribute($context["product"], "price", array());
                                    echo "</span></p>\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 146
                                echo "\t\t\t\t\t\t\t\t\t\t";
                                if ($this->getAttribute($context["product"], "tax", array())) {
                                    // line 147
                                    echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"price-tax\"><span class=\"price\">";
                                    echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                                    echo " ";
                                    echo $this->getAttribute($context["product"], "tax", array());
                                    echo "</span></p>
\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 149
                                echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                            }
                            // line 151
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        // line 152
                        echo "\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                        // line 154
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "description", array())) {
                            // line 155
                            echo "\t\t\t\t\t\t\t\t\t\t<p class=\"product-des\">";
                            echo $this->getAttribute($context["product"], "description", array());
                            echo "</p>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 157
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "countdown", array())) {
                            echo "<div class=\"text-hurryup\"><p>";
                            echo (isset($context["text_hurryup"]) ? $context["text_hurryup"] : null);
                            echo "</p></div><div id=\"Countdown";
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "-";
                            echo (isset($context["i"]) ? $context["i"] : null);
                            echo "\" class=\"box-timer\"></div> ";
                        }
                        // line 158
                        echo "\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                        // line 159
                        if ((isset($context["use_catalog"]) ? $context["use_catalog"] : null)) {
                            // line 160
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcart", array())) {
                                // line 161
                                echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"box-hover\">
\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-cart\" type=\"button\"  title=\"";
                                // line 162
                                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                                echo "\" onclick=\"cart.add('";
                                echo $this->getAttribute($context["product"], "product_id", array());
                                echo "');\"><span>";
                                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 165
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        // line 166
                        echo "\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div><!-- caption -->
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div><!-- product-thumb -->
\t\t\t\t\t\t";
                        // line 171
                        if (($this->getAttribute($context["product"], "date_end", array()) && $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "countdown", array()))) {
                            // line 172
                            echo "\t\t\t\t\t\t<script >
\t\t\t\t\t\t\$(document).ready(function () {
\t\t\t\t\t\t\$('#Countdown";
                            // line 174
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "-";
                            echo (isset($context["i"]) ? $context["i"] : null);
                            echo "').countdown({
\t\t\t\t\t\tuntil: new Date(";
                            // line 175
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "Y");
                            echo ", ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "m");
                            echo "-1, ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "d");
                            echo ", ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "H");
                            echo ", ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "i");
                            echo ", ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "s");
                            echo "),
\t\t\t\t\t\tlabels: ['";
                            // line 176
                            echo (isset($context["text_years"]) ? $context["text_years"] : null);
                            echo "', '";
                            echo (isset($context["text_months"]) ? $context["text_months"] : null);
                            echo " ', '";
                            echo (isset($context["text_weeks"]) ? $context["text_weeks"] : null);
                            echo "', '";
                            echo (isset($context["text_days"]) ? $context["text_days"] : null);
                            echo "', '";
                            echo (isset($context["text_hrs"]) ? $context["text_hrs"] : null);
                            echo "', '";
                            echo (isset($context["text_mins"]) ? $context["text_mins"] : null);
                            echo "', '";
                            echo (isset($context["text_secs"]) ? $context["text_secs"] : null);
                            echo "'],
\t\t\t\t\t\tlabels1: ['";
                            // line 177
                            echo (isset($context["text_year"]) ? $context["text_year"] : null);
                            echo "', '";
                            echo (isset($context["text_month"]) ? $context["text_month"] : null);
                            echo " ', '";
                            echo (isset($context["text_week"]) ? $context["text_week"] : null);
                            echo "', '";
                            echo (isset($context["text_day"]) ? $context["text_day"] : null);
                            echo "', '";
                            echo (isset($context["text_hr"]) ? $context["text_hr"] : null);
                            echo "', '";
                            echo (isset($context["text_min"]) ? $context["text_min"] : null);
                            echo "', '";
                            echo (isset($context["text_sec"]) ? $context["text_sec"] : null);
                            echo "'],
\t\t\t\t\t\t});
\t\t\t\t\t\t// \$('#Countdown";
                            // line 179
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "-";
                            echo (isset($context["i"]) ? $context["i"] : null);
                            echo "').countdown('pause');
\t\t\t\t\t\t});
\t\t\t\t\t\t</script>
\t\t\t\t\t\t";
                        }
                        // line 183
                        echo "\t\t\t\t</div><!-- product-layout -->
\t\t\t\t";
                        // line 184
                        if (((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0) || ((isset($context["count"]) ? $context["count"] : null) == twig_length_filter($this->env, $this->getAttribute($context["octab"], "products", array()))))) {
                            // line 185
                            echo "\t\t\t\t</div>
\t\t\t\t";
                        }
                        // line 187
                        echo "            ";
                    } elseif (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "type", array()) == 1)) {
                        // line 188
                        echo "            <!-- List -->
\t\t\t";
                        // line 189
                        if ((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0)) {
                            // line 190
                            echo "\t\t\t<div class=\"row_items ";
                            if ( !$this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "slider", array())) {
                                echo (isset($context["class"]) ? $context["class"] : null);
                            }
                            echo "\">
\t\t\t";
                        }
                        // line 192
                        echo "\t\t\t";
                        $context["count"] = ((isset($context["count"]) ? $context["count"] : null) + 1);
                        // line 193
                        echo "\t\t\t\t<div class=\"product-layout list-style \">
\t\t\t\t\t<div class=\"product-thumb transition\">
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"item-inner\">
\t\t\t\t\t\t\t\t<div class=\"image images-container\">
\t\t\t\t\t\t\t\t\t<a href=\"";
                        // line 198
                        echo $this->getAttribute($context["product"], "href", array());
                        echo "\" class=\"product-image\">
\t\t\t\t\t\t\t\t\t\t";
                        // line 199
                        if (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "rotator", array()) && $this->getAttribute($context["product"], "rotator_image", array()))) {
                            echo "<img class=\"img-r\" src=\"";
                            echo $this->getAttribute($context["product"], "rotator_image", array());
                            echo "\" alt=\"";
                            echo $this->getAttribute($context["product"], "name", array());
                            echo "\" />";
                        }
                        // line 200
                        echo "\t\t\t\t\t\t\t\t\t\t<img src=\"";
                        echo $this->getAttribute($context["product"], "thumb", array());
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" title=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" class=\"img-responsive\" />
\t\t\t\t\t\t\t\t\t</a>\t\t\t\t  
\t\t\t\t\t\t\t\t\t";
                        // line 202
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "salelabel", array())) {
                            // line 203
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($context["product"], "special", array())) {
                                // line 204
                                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_sale\">";
                                echo (isset($context["text_label_sale"]) ? $context["text_label_sale"] : null);
                                echo "</div>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 206
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        // line 207
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "newlabel", array())) {
                            // line 208
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($context["product"], "is_new", array())) {
                                // line 209
                                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_new\">";
                                echo (isset($context["text_label_new"]) ? $context["text_label_new"] : null);
                                echo "</div>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 211
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        // line 212
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ((isset($context["use_quickview"]) ? $context["use_quickview"] : null)) {
                            // line 213
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showquickview", array())) {
                                // line 214
                                echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"quick-view\"><button class=\"button btn-quickview\" type=\"button\"  title=\"";
                                echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                                echo "\" onclick=\"ocquickview.ajaxView('";
                                echo $this->getAttribute($context["product"], "href", array());
                                echo "')\"><span>";
                                echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                                echo "</span></button></div>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 216
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        // line 217
                        echo "\t\t\t\t\t\t\t\t</div><!-- image -->
\t\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t\t";
                        // line 219
                        if ($this->getAttribute($context["product"], "manufacturer", array())) {
                            // line 220
                            echo "\t\t\t\t\t\t\t\t\t<p class=\"manufacture-product\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
                            // line 221
                            echo $this->getAttribute($context["product"], "manufacturers", array());
                            echo "\">";
                            echo $this->getAttribute($context["product"], "manufacturer", array());
                            echo "</a>
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 224
                        echo "\t\t\t\t\t\t\t\t\t<h4 class=\"product-name\"><a href=\"";
                        echo $this->getAttribute($context["product"], "href", array());
                        echo "\">";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "</a></h4> 
\t\t\t\t\t\t\t\t\t";
                        // line 225
                        if ((isset($context["use_catalog"]) ? $context["use_catalog"] : null)) {
                            // line 226
                            echo "\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($context["product"], "price", array())) {
                                // line 227
                                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"price-box\">
\t\t\t\t\t\t\t\t\t\t<label>";
                                // line 228
                                echo (isset($context["price_label"]) ? $context["price_label"] : null);
                                echo "</label>
\t\t\t\t\t\t\t\t\t\t";
                                // line 229
                                if ( !$this->getAttribute($context["product"], "special", array())) {
                                    // line 230
                                    echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"regular-price\"><span class=\"price\">";
                                    echo $this->getAttribute($context["product"], "price", array());
                                    echo "</span></p>
\t\t\t\t\t\t\t\t\t\t";
                                } else {
                                    // line 232
                                    echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"special-price\"><span class=\"price\">";
                                    echo $this->getAttribute($context["product"], "special", array());
                                    echo "</span></p>
\t\t\t\t\t\t\t\t\t\t\t<p class=\"old-price\"><span class=\"price\">";
                                    // line 233
                                    echo $this->getAttribute($context["product"], "price", array());
                                    echo "</span></p>\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 235
                                echo "\t\t\t\t\t\t\t\t\t\t";
                                if ($this->getAttribute($context["product"], "tax", array())) {
                                    // line 236
                                    echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"price-tax\"><span class=\"price\">";
                                    echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                                    echo " ";
                                    echo $this->getAttribute($context["product"], "tax", array());
                                    echo "</span></p>
\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 238
                                echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                            }
                            // line 240
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        // line 241
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["product"], "rating", array())) {
                            // line 242
                            echo "\t\t\t\t\t\t\t\t\t<div class=\"ratings\">
\t\t\t\t\t\t\t\t\t\t<div class=\"rating-box\">
\t\t\t\t\t\t\t\t\t\t";
                            // line 244
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(range(0, 5));
                            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                                // line 245
                                echo "\t\t\t\t\t\t\t\t\t\t\t";
                                if (($this->getAttribute($context["product"], "rating", array()) == $context["i"])) {
                                    // line 246
                                    echo "\t\t\t\t\t\t\t\t\t\t\t";
                                    $context["class_r"] = ("rating" . $context["i"]);
                                    // line 247
                                    echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"";
                                    echo (isset($context["class_r"]) ? $context["class_r"] : null);
                                    echo "\">rating</div>
\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 249
                                echo "\t\t\t\t\t\t\t\t\t\t";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 250
                            echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 253
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "description", array())) {
                            // line 254
                            echo "\t\t\t\t\t\t\t\t\t<p class=\"product-des\">";
                            echo $this->getAttribute($context["product"], "description", array());
                            echo "</p>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 256
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "countdown", array())) {
                            echo "<div class=\"text-hurryup\"><p>";
                            echo (isset($context["text_hurryup"]) ? $context["text_hurryup"] : null);
                            echo "</p></div><div id=\"Countdown";
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "-";
                            echo (isset($context["i"]) ? $context["i"] : null);
                            echo "\" class=\"box-timer\"></div> ";
                        }
                        // line 257
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ((($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcart", array()) || $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showwishlist", array())) || $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcompare", array()))) {
                            echo "\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"action-links\">
\t\t\t\t\t\t\t\t\t\t\t";
                            // line 259
                            if ((isset($context["use_catalog"]) ? $context["use_catalog"] : null)) {
                                // line 260
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                                if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcart", array())) {
                                    // line 261
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-cart\" type=\"button\"  title=\"";
                                    echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                                    echo "\" onclick=\"cart.add('";
                                    echo $this->getAttribute($context["product"], "product_id", array());
                                    echo "');\"><span>";
                                    echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                                    echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 263
                                echo "\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 264
                            echo "\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showwishlist", array())) {
                                // line 265
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-wishlist\" type=\"button\"  title=\"";
                                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                                echo "\" onclick=\"wishlist.add('";
                                echo $this->getAttribute($context["product"], "product_id", array());
                                echo "');\"><span>";
                                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 267
                            echo "\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcompare", array())) {
                                // line 268
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-compare\" type=\"button\"  title=\"";
                                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                                echo "\" onclick=\"compare.add('";
                                echo $this->getAttribute($context["product"], "product_id", array());
                                echo "');\"><span>";
                                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 270
                            echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 272
                        echo "\t\t\t\t\t\t\t\t</div><!-- caption -->
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div><!-- product-thumb -->
\t\t\t\t\t\t";
                        // line 276
                        if (($this->getAttribute($context["product"], "date_end", array()) && $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "countdown", array()))) {
                            // line 277
                            echo "\t\t\t\t\t\t<script >
\t\t\t\t\t\t\$(document).ready(function () {
\t\t\t\t\t\t\$('#Countdown";
                            // line 279
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "-";
                            echo (isset($context["i"]) ? $context["i"] : null);
                            echo "').countdown({
\t\t\t\t\t\tuntil: new Date(";
                            // line 280
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "Y");
                            echo ", ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "m");
                            echo "-1, ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "d");
                            echo ", ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "H");
                            echo ", ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "i");
                            echo ", ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "s");
                            echo "),
\t\t\t\t\t\tlabels: ['";
                            // line 281
                            echo (isset($context["text_years"]) ? $context["text_years"] : null);
                            echo "', '";
                            echo (isset($context["text_months"]) ? $context["text_months"] : null);
                            echo " ', '";
                            echo (isset($context["text_weeks"]) ? $context["text_weeks"] : null);
                            echo "', '";
                            echo (isset($context["text_days"]) ? $context["text_days"] : null);
                            echo "', '";
                            echo (isset($context["text_hrs"]) ? $context["text_hrs"] : null);
                            echo "', '";
                            echo (isset($context["text_mins"]) ? $context["text_mins"] : null);
                            echo "', '";
                            echo (isset($context["text_secs"]) ? $context["text_secs"] : null);
                            echo "'],
\t\t\t\t\t\tlabels1: ['";
                            // line 282
                            echo (isset($context["text_year"]) ? $context["text_year"] : null);
                            echo "', '";
                            echo (isset($context["text_month"]) ? $context["text_month"] : null);
                            echo " ', '";
                            echo (isset($context["text_week"]) ? $context["text_week"] : null);
                            echo "', '";
                            echo (isset($context["text_day"]) ? $context["text_day"] : null);
                            echo "', '";
                            echo (isset($context["text_hr"]) ? $context["text_hr"] : null);
                            echo "', '";
                            echo (isset($context["text_min"]) ? $context["text_min"] : null);
                            echo "', '";
                            echo (isset($context["text_sec"]) ? $context["text_sec"] : null);
                            echo "'],
\t\t\t\t\t\t});
\t\t\t\t\t\t// \$('#Countdown";
                            // line 284
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "-";
                            echo (isset($context["i"]) ? $context["i"] : null);
                            echo "').countdown('pause');
\t\t\t\t\t\t});
\t\t\t\t\t\t</script>
\t\t\t\t\t\t";
                        }
                        // line 288
                        echo "\t\t\t\t</div><!-- product-layout -->
\t\t\t\t";
                        // line 289
                        if (((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0) || ((isset($context["count"]) ? $context["count"] : null) == twig_length_filter($this->env, $this->getAttribute($context["octab"], "products", array()))))) {
                            // line 290
                            echo "\t\t\t\t</div>
\t\t\t\t";
                        }
                        // line 292
                        echo "            ";
                    } else {
                        // line 293
                        echo "            <!-- other type -->
\t\t\t";
                        // line 294
                        if ((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0)) {
                            // line 295
                            echo "\t\t\t<div class=\"row_items ";
                            if ( !$this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "slider", array())) {
                                echo (isset($context["class"]) ? $context["class"] : null);
                            }
                            echo "\">
\t\t\t";
                        }
                        // line 297
                        echo "\t\t\t";
                        $context["count"] = ((isset($context["count"]) ? $context["count"] : null) + 1);
                        // line 298
                        echo "\t\t\t\t<div class=\"product-layout product-customize \">
\t\t\t\t\t<div class=\"product-thumb transition \">
\t\t\t\t\t\t<div class=\"item\">
\t\t\t\t\t\t\t<div class=\"item-inner\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"image images-container\">
\t\t\t\t\t\t\t\t\t<a href=\"";
                        // line 304
                        echo $this->getAttribute($context["product"], "href", array());
                        echo "\" class=\"product-image\">
\t\t\t\t\t\t\t\t\t\t";
                        // line 305
                        if (($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "rotator", array()) && $this->getAttribute($context["product"], "rotator_image", array()))) {
                            echo "<img class=\"img-r\" src=\"";
                            echo $this->getAttribute($context["product"], "rotator_image", array());
                            echo "\" alt=\"";
                            echo $this->getAttribute($context["product"], "name", array());
                            echo "\" />";
                        }
                        // line 306
                        echo "\t\t\t\t\t\t\t\t\t\t<img src=\"";
                        echo $this->getAttribute($context["product"], "thumb", array());
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" title=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" class=\"img-responsive\" />
\t\t\t\t\t\t\t\t\t</a>\t\t\t\t  
\t\t\t\t\t\t\t\t\t";
                        // line 308
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "salelabel", array())) {
                            // line 309
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($context["product"], "special", array())) {
                                // line 310
                                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_sale\">";
                                echo (isset($context["text_label_sale"]) ? $context["text_label_sale"] : null);
                                echo "</div>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 312
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        // line 313
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "newlabel", array())) {
                            // line 314
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($context["product"], "is_new", array())) {
                                // line 315
                                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"label-product label_new\">";
                                echo (isset($context["text_label_new"]) ? $context["text_label_new"] : null);
                                echo "</div>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 317
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        // line 318
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ((isset($context["use_quickview"]) ? $context["use_quickview"] : null)) {
                            // line 319
                            echo "\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showquickview", array())) {
                                // line 320
                                echo "\t\t\t\t\t\t\t\t\t\t\t<div class=\"quick-view\"><button class=\"button btn-quickview\" type=\"button\"  title=\"";
                                echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                                echo "\" onclick=\"ocquickview.ajaxView('";
                                echo $this->getAttribute($context["product"], "href", array());
                                echo "')\"><span>";
                                echo (isset($context["button_quickview"]) ? $context["button_quickview"] : null);
                                echo "</span></button></div>
\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 322
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        // line 323
                        echo "\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t</div><!-- image -->
\t\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t\t";
                        // line 327
                        if ($this->getAttribute($context["product"], "manufacturer", array())) {
                            // line 328
                            echo "\t\t\t\t\t\t\t\t\t<p class=\"manufacture-product\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
                            // line 329
                            echo $this->getAttribute($context["product"], "manufacturers", array());
                            echo "\">";
                            echo $this->getAttribute($context["product"], "manufacturer", array());
                            echo "</a>
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 332
                        echo "\t\t\t\t\t\t\t\t\t<h4 class=\"product-name\"><a href=\"";
                        echo $this->getAttribute($context["product"], "href", array());
                        echo "\">";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "</a></h4>
\t\t\t\t\t\t\t\t\t";
                        // line 333
                        if ($this->getAttribute($context["product"], "rating", array())) {
                            // line 334
                            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"ratings\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"rating-box\">
\t\t\t\t\t\t\t\t\t\t\t";
                            // line 336
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(range(0, 5));
                            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                                // line 337
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                                if (($this->getAttribute($context["product"], "rating", array()) == $context["i"])) {
                                    // line 338
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                                    $context["class_r"] = ("rating" . $context["i"]);
                                    // line 339
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"";
                                    echo (isset($context["class_r"]) ? $context["class_r"] : null);
                                    echo "\">rating</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 341
                                echo "\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 342
                            echo "\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 344
                        echo "\t
\t\t\t\t\t\t\t\t\t";
                        // line 345
                        if ((isset($context["use_catalog"]) ? $context["use_catalog"] : null)) {
                            // line 346
                            echo "\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute($context["product"], "price", array())) {
                                // line 347
                                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"price-box\">
\t\t\t\t\t\t\t\t\t\t<label>";
                                // line 348
                                echo (isset($context["price_label"]) ? $context["price_label"] : null);
                                echo "</label>
\t\t\t\t\t\t\t\t\t\t";
                                // line 349
                                if ( !$this->getAttribute($context["product"], "special", array())) {
                                    // line 350
                                    echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"regular-price\"><span class=\"price\">";
                                    echo $this->getAttribute($context["product"], "price", array());
                                    echo "</span></p>
\t\t\t\t\t\t\t\t\t\t";
                                } else {
                                    // line 352
                                    echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"special-price\"><span class=\"price\">";
                                    echo $this->getAttribute($context["product"], "special", array());
                                    echo "</span></p>
\t\t\t\t\t\t\t\t\t\t\t<p class=\"old-price\"><span class=\"price\">";
                                    // line 353
                                    echo $this->getAttribute($context["product"], "price", array());
                                    echo "</span></p>\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 355
                                echo "\t\t\t\t\t\t\t\t\t\t";
                                if ($this->getAttribute($context["product"], "tax", array())) {
                                    // line 356
                                    echo "\t\t\t\t\t\t\t\t\t\t\t<p class=\"price-tax\"><span class=\"price\">";
                                    echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                                    echo " ";
                                    echo $this->getAttribute($context["product"], "tax", array());
                                    echo "</span></p>
\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 358
                                echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                            }
                            // line 360
                            echo "\t\t\t\t\t\t\t\t\t";
                        }
                        // line 361
                        echo "\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
                        // line 362
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "countdown", array())) {
                            echo "<div class=\"text-hurryup\"><p>";
                            echo (isset($context["text_hurryup"]) ? $context["text_hurryup"] : null);
                            echo "</p></div><div id=\"Countdown";
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "-";
                            echo (isset($context["i"]) ? $context["i"] : null);
                            echo "\" class=\"box-timer\"></div> ";
                        }
                        // line 363
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ((($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcart", array()) || $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showwishlist", array())) || $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcompare", array()))) {
                            echo "\t
\t\t\t\t\t\t\t\t\t\t<div class=\"action-links\">
\t\t\t\t\t\t\t\t\t\t\t";
                            // line 365
                            if ((isset($context["use_catalog"]) ? $context["use_catalog"] : null)) {
                                // line 366
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                                if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcart", array())) {
                                    // line 367
                                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-cart\" type=\"button\"  title=\"";
                                    echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                                    echo "\" onclick=\"cart.add('";
                                    echo $this->getAttribute($context["product"], "product_id", array());
                                    echo "');\"><span>";
                                    echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                                    echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t\t";
                                }
                                // line 369
                                echo "\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 370
                            echo "\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showwishlist", array())) {
                                // line 371
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-wishlist\" type=\"button\"  title=\"";
                                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                                echo "\" onclick=\"wishlist.add('";
                                echo $this->getAttribute($context["product"], "product_id", array());
                                echo "');\"><span>";
                                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 373
                            echo "\t\t\t\t\t\t\t\t\t\t\t";
                            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "showcompare", array())) {
                                // line 374
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"button btn-compare\" type=\"button\"  title=\"";
                                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                                echo "\" onclick=\"compare.add('";
                                echo $this->getAttribute($context["product"], "product_id", array());
                                echo "');\"><span>";
                                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                                echo "</span></button>
\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 376
                            echo "\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 378
                        echo "\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "description", array())) {
                            // line 379
                            echo "\t\t\t\t\t\t\t\t\t\t<p class=\"product-des\">";
                            echo $this->getAttribute($context["product"], "description", array());
                            echo "</p>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 381
                        echo "\t\t\t\t\t\t\t\t</div><!-- caption -->
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t</div><!-- product-thumb -->
\t\t\t\t\t\t";
                        // line 386
                        if (($this->getAttribute($context["product"], "date_end", array()) && $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "countdown", array()))) {
                            // line 387
                            echo "\t\t\t\t\t\t<script >
\t\t\t\t\t\t\$(document).ready(function () {
\t\t\t\t\t\t\$('#Countdown";
                            // line 389
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "-";
                            echo (isset($context["i"]) ? $context["i"] : null);
                            echo "').countdown({
\t\t\t\t\t\tuntil: new Date(";
                            // line 390
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "Y");
                            echo ", ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "m");
                            echo "-1, ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "d");
                            echo ", ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "H");
                            echo ", ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "i");
                            echo ", ";
                            echo twig_date_format_filter($this->env, $this->getAttribute($context["product"], "date_end", array()), "s");
                            echo "),
\t\t\t\t\t\tlabels: ['";
                            // line 391
                            echo (isset($context["text_years"]) ? $context["text_years"] : null);
                            echo "', '";
                            echo (isset($context["text_months"]) ? $context["text_months"] : null);
                            echo " ', '";
                            echo (isset($context["text_weeks"]) ? $context["text_weeks"] : null);
                            echo "', '";
                            echo (isset($context["text_days"]) ? $context["text_days"] : null);
                            echo "', '";
                            echo (isset($context["text_hrs"]) ? $context["text_hrs"] : null);
                            echo "', '";
                            echo (isset($context["text_mins"]) ? $context["text_mins"] : null);
                            echo "', '";
                            echo (isset($context["text_secs"]) ? $context["text_secs"] : null);
                            echo "'],
\t\t\t\t\t\tlabels1: ['";
                            // line 392
                            echo (isset($context["text_year"]) ? $context["text_year"] : null);
                            echo "', '";
                            echo (isset($context["text_month"]) ? $context["text_month"] : null);
                            echo " ', '";
                            echo (isset($context["text_week"]) ? $context["text_week"] : null);
                            echo "', '";
                            echo (isset($context["text_day"]) ? $context["text_day"] : null);
                            echo "', '";
                            echo (isset($context["text_hr"]) ? $context["text_hr"] : null);
                            echo "', '";
                            echo (isset($context["text_min"]) ? $context["text_min"] : null);
                            echo "', '";
                            echo (isset($context["text_sec"]) ? $context["text_sec"] : null);
                            echo "'],
\t\t\t\t\t\t});
\t\t\t\t\t\t// \$('#Countdown";
                            // line 394
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "-";
                            echo (isset($context["i"]) ? $context["i"] : null);
                            echo "').countdown('pause');
\t\t\t\t\t\t});
\t\t\t\t\t\t</script>
\t\t\t\t\t\t";
                        }
                        // line 398
                        echo "\t\t\t\t</div><!-- product-layout -->
\t\t\t";
                        // line 399
                        if (((((isset($context["count"]) ? $context["count"] : null) % (isset($context["rows"]) ? $context["rows"] : null)) == 0) || ((isset($context["count"]) ? $context["count"] : null) == twig_length_filter($this->env, $this->getAttribute($context["octab"], "products", array()))))) {
                            // line 400
                            echo "\t\t\t\t</div>
\t\t\t";
                        }
                        // line 402
                        echo "\t\t\t";
                    }
                    // line 403
                    echo "\t\t\t\t
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 404
                echo "\t\t  \t\t  
\t\t";
            } else {
                // line 406
                echo "\t\t\t<p class=\"text_empty\">";
                echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
                echo "</p>
\t\t";
            }
            // line 408
            echo "        </div>
\t\t";
            // line 409
            $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
            echo "\t\t
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['octab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 410
        echo "\t\t
    </div>
  </div>
  </div>
  </div>
  <div class=\"clearfix\"></div>
</div>

  <script >
    \$(document).ready(function() {
      \$('a[href=\"#tab-";
        // line 420
        echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
        echo "0\"]').trigger( \"click\" );
\t  ";
        // line 421
        if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "slider", array())) {
            // line 422
            echo "      \$(\".tab-container-";
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo "\").owlCarousel({
\t\titems: ";
            // line 423
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "items", array());
            echo ",
        loop: ";
            // line 424
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "loop", array())) {
                echo " true ";
            } else {
                echo " false ";
            }
            echo ",
        margin: ";
            // line 425
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "margin", array(), "any", true, true)) {
                echo " ";
                echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "margin", array());
                echo " ";
            } else {
                echo " 20 ";
            }
            echo ",
        nav: ";
            // line 426
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "navigation", array())) {
                echo " true ";
            } else {
                echo " false ";
            }
            echo ",
        dots: ";
            // line 427
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "pagination", array())) {
                echo " true ";
            } else {
                echo " false ";
            }
            echo ",
        autoplay:  ";
            // line 428
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "auto", array())) {
                echo " true ";
            } else {
                echo " false ";
            }
            echo ",
        autoplayTimeout: ";
            // line 429
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "time", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "time", array());
                echo " ";
            } else {
                echo " 2000 ";
            }
            echo ",
        autoplayHoverPause: true,
        autoplaySpeed: ";
            // line 431
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "speed", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "speed", array());
                echo " ";
            } else {
                echo " 3000 ";
            }
            echo ",
        navSpeed: ";
            // line 432
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "speed", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "speed", array());
                echo " ";
            } else {
                echo " 3000 ";
            }
            echo ",
        dotsSpeed: ";
            // line 433
            if ($this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "speed", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "speed", array());
                echo " ";
            } else {
                echo " 3000 ";
            }
            echo ",
\t\tlazyLoad: true,
        responsive:{
\t\t\t0:{
\t\t\t\titems: 1,
\t\t\t\tnav: false
\t\t\t},
\t\t\t480:{
\t\t\t\titems: ";
            // line 441
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "smobile", array());
            echo ",
\t\t\t\tnav: false
\t\t\t},
\t\t\t768:{
\t\t\t\titems: ";
            // line 445
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "mobile", array());
            echo "
\t\t\t},
\t\t\t992:{
\t\t\t\titems: ";
            // line 448
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "tablet", array());
            echo "
\t\t\t},
\t\t\t1200:{
\t\t\t\titems: ";
            // line 451
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "desktop", array());
            echo "
\t\t\t},
\t\t\t1600:{
\t\t\t\titems: ";
            // line 454
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "items", array());
            echo "
\t\t\t}
\t\t},
\t\tonInitialized: function() {
\t\t\tvar count = \$(\".tab-container-";
            // line 458
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .owl-item.active\").length;
\t\t\tif(count == 1) {
\t\t\t\t\$(\".tab-container-";
            // line 460
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .owl-item\").removeClass('first');
\t\t\t\t\$(\".tab-container-";
            // line 461
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .owl-item.active\").addClass('first');
\t\t\t} else {
\t\t\t\t\$(\".tab-container-";
            // line 463
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .owl-item\").removeClass('first');
\t\t\t\t\$(\".tab-container-";
            // line 464
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .owl-item.active:first\").addClass('first');
\t\t\t}
\t\t},
\t\tonTranslated: function() {
\t\t\tvar count = \$(\".tab-container-";
            // line 468
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .owl-item.active\").length;
\t\t\tif(count == 1) {
\t\t\t\t\$(\".tab-container-";
            // line 470
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .owl-item\").removeClass('first');
\t\t\t\t\$(\".tab-container-";
            // line 471
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .owl-item.active\").addClass('first');
\t\t\t} else {
\t\t\t\t\$(\".tab-container-";
            // line 473
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .owl-item\").removeClass('first');
\t\t\t\t\$(\".tab-container-";
            // line 474
            echo $this->getAttribute((isset($context["config_module"]) ? $context["config_module"] : null), "module_id", array());
            echo " .owl-item.active:first\").addClass('first');
\t\t\t}
\t\t}
      });
\t";
        }
        // line 479
        echo "    });
  </script>

";
    }

    public function getTemplateName()
    {
        return "tt_naturecircle1/template/extension/module/octabproducts.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1562 => 479,  1554 => 474,  1550 => 473,  1545 => 471,  1541 => 470,  1536 => 468,  1529 => 464,  1525 => 463,  1520 => 461,  1516 => 460,  1511 => 458,  1504 => 454,  1498 => 451,  1492 => 448,  1486 => 445,  1479 => 441,  1462 => 433,  1452 => 432,  1442 => 431,  1431 => 429,  1423 => 428,  1415 => 427,  1407 => 426,  1397 => 425,  1389 => 424,  1385 => 423,  1380 => 422,  1378 => 421,  1374 => 420,  1362 => 410,  1354 => 409,  1351 => 408,  1345 => 406,  1341 => 404,  1334 => 403,  1331 => 402,  1327 => 400,  1325 => 399,  1322 => 398,  1313 => 394,  1296 => 392,  1280 => 391,  1266 => 390,  1260 => 389,  1256 => 387,  1254 => 386,  1247 => 381,  1241 => 379,  1238 => 378,  1234 => 376,  1224 => 374,  1221 => 373,  1211 => 371,  1208 => 370,  1205 => 369,  1195 => 367,  1192 => 366,  1190 => 365,  1184 => 363,  1174 => 362,  1171 => 361,  1168 => 360,  1164 => 358,  1156 => 356,  1153 => 355,  1148 => 353,  1143 => 352,  1137 => 350,  1135 => 349,  1131 => 348,  1128 => 347,  1125 => 346,  1123 => 345,  1120 => 344,  1115 => 342,  1109 => 341,  1103 => 339,  1100 => 338,  1097 => 337,  1093 => 336,  1089 => 334,  1087 => 333,  1080 => 332,  1072 => 329,  1069 => 328,  1067 => 327,  1061 => 323,  1058 => 322,  1048 => 320,  1045 => 319,  1042 => 318,  1039 => 317,  1033 => 315,  1030 => 314,  1027 => 313,  1024 => 312,  1018 => 310,  1015 => 309,  1013 => 308,  1003 => 306,  995 => 305,  991 => 304,  983 => 298,  980 => 297,  972 => 295,  970 => 294,  967 => 293,  964 => 292,  960 => 290,  958 => 289,  955 => 288,  946 => 284,  929 => 282,  913 => 281,  899 => 280,  893 => 279,  889 => 277,  887 => 276,  881 => 272,  877 => 270,  867 => 268,  864 => 267,  854 => 265,  851 => 264,  848 => 263,  838 => 261,  835 => 260,  833 => 259,  827 => 257,  816 => 256,  810 => 254,  807 => 253,  802 => 250,  796 => 249,  790 => 247,  787 => 246,  784 => 245,  780 => 244,  776 => 242,  773 => 241,  770 => 240,  766 => 238,  758 => 236,  755 => 235,  750 => 233,  745 => 232,  739 => 230,  737 => 229,  733 => 228,  730 => 227,  727 => 226,  725 => 225,  718 => 224,  710 => 221,  707 => 220,  705 => 219,  701 => 217,  698 => 216,  688 => 214,  685 => 213,  682 => 212,  679 => 211,  673 => 209,  670 => 208,  667 => 207,  664 => 206,  658 => 204,  655 => 203,  653 => 202,  643 => 200,  635 => 199,  631 => 198,  624 => 193,  621 => 192,  613 => 190,  611 => 189,  608 => 188,  605 => 187,  601 => 185,  599 => 184,  596 => 183,  587 => 179,  570 => 177,  554 => 176,  540 => 175,  534 => 174,  530 => 172,  528 => 171,  521 => 166,  518 => 165,  508 => 162,  505 => 161,  502 => 160,  500 => 159,  497 => 158,  486 => 157,  480 => 155,  478 => 154,  474 => 152,  471 => 151,  467 => 149,  459 => 147,  456 => 146,  451 => 144,  446 => 143,  440 => 141,  438 => 140,  434 => 139,  431 => 138,  428 => 137,  426 => 136,  420 => 135,  417 => 134,  412 => 132,  406 => 131,  400 => 129,  397 => 128,  394 => 127,  390 => 126,  386 => 124,  383 => 123,  375 => 120,  372 => 119,  370 => 118,  364 => 114,  359 => 111,  356 => 110,  346 => 108,  343 => 107,  340 => 106,  330 => 104,  327 => 103,  317 => 101,  315 => 100,  310 => 98,  307 => 97,  304 => 96,  298 => 94,  295 => 93,  292 => 92,  289 => 91,  283 => 89,  280 => 88,  278 => 87,  268 => 85,  260 => 84,  256 => 83,  247 => 76,  244 => 75,  236 => 73,  234 => 72,  230 => 71,  227 => 70,  222 => 69,  217 => 68,  215 => 67,  205 => 66,  202 => 65,  199 => 64,  195 => 63,  190 => 62,  187 => 61,  184 => 60,  181 => 59,  179 => 58,  175 => 57,  169 => 53,  166 => 52,  163 => 51,  160 => 50,  157 => 49,  154 => 48,  151 => 47,  149 => 46,  144 => 45,  141 => 44,  138 => 43,  135 => 42,  132 => 41,  129 => 40,  127 => 39,  123 => 37,  117 => 36,  115 => 35,  108 => 32,  105 => 31,  97 => 29,  95 => 28,  91 => 27,  86 => 26,  83 => 25,  78 => 24,  76 => 23,  70 => 19,  64 => 16,  61 => 15,  59 => 14,  56 => 13,  50 => 11,  44 => 9,  41 => 8,  35 => 6,  33 => 5,  19 => 1,);
    }
}
/* <div class="tt_tabsproduct_module{% if config_module.row >=2 %}{{' multi-rows'}}{% endif %} {{ config_module.class }}" id="product_module{{ config_module.module_id }}">*/
/* 	<div class="module-title">*/
/* 	  */
/* 	  <h2>*/
/* 		{% if text_store1 %}*/
/* 		  <span>{{ text_store1 }}</span>*/
/* 		{% endif %}*/
/* 		{% if config_module.title_lang[code].title %}*/
/* 		  {{ config_module.title_lang[code].title }}*/
/* 		{% else %}*/
/* 		  {{ heading_title }}*/
/* 		{% endif %}*/
/* 	  </h2>*/
/* 	  {% if module_description %}*/
/* 		<div class="module-description">*/
/* 		  {{ module_description }}*/
/* 		</div>*/
/* 	  {% endif %}*/
/* 	*/
/* 	<div class="clearfix"></div>  */
/*   </div>*/
/*   <ul class="tab-heading tabs-categorys">*/
/* 		  {% set i= 0 %}*/
/* 		  {% for octab in octabs %}*/
/* 			<li>*/
/* 				<a data-toggle="pill" href="#tab-{{ config_module.module_id }}{{ i }}">*/
/* 					{% if config_module.type == 2 %}	*/
/* 						{% if octab.thumbnail_image %}*/
/* 							<span class="wrapper-thumb"><img class="thumb-img" src="{{ octab.thumbnail_image }}" alt="{{ octab.title }}" /></span>*/
/* 						{% endif %}*/
/* 					{% endif %}*/
/* 					<span>{{ octab.title }}</span>*/
/* 				</a>*/
/* 			</li>*/
/* 			{% set i= i +1  %}*/
/* 		  {% endfor %}*/
/* 	</ul>*/
/* 	<div class="clearfix"></div> */
/*   {% if config_module.slider %}*/
/* 	{% set class_slider = ' owl-carousel owl-theme '%}*/
/*   {% else %}*/
/* 	{% set class_slider = ''%}*/
/*   {% endif %}*/
/*   {% if config_module.nrow == 0 %}*/
/* 	{% set class = 'two_items col-lg-6 col-md-6 col-sm-6 col-xs-12' %}	*/
/*   {% elseif config_module.nrow == 1 %}*/
/* 	{% set class = 'three_items col-lg-4 col-md-4 col-sm-4 col-xs-12' %}*/
/*   {% elseif config_module.nrow == 2 %}*/
/* 	{% set class = 'four_items col-lg-3 col-md-3 col-sm-3 col-xs-12' %}*/
/*   {% else %}*/
/* 	{% set class = 'six_items col-lg-2 col-md-2 col-sm-2 col-xs-12' %}*/
/*   {% endif %}*/
/*   <div class="box-style">*/
/* 	<div class="owl-container">*/
/*   <div class="tt-product ">*/
/*     <div class="tab-content">*/
/* 		{% set i= 0 %}	  */
/* 		{% if config_module.row %}*/
/* 			{% set rows = config_module.row %}*/
/* 		{% else %}*/
/* 			{% set rows = 1 %}*/
/* 		{% endif %}		*/
/* 	{% for octab in octabs %}*/
/* 	{% set count, count_i = 0, 0 %}*/
/* 	{% set items_numb, items_gnumb = 3, 6 %}*/
/* 	<div class="tab-container-{{ config_module.module_id }} {{ class_slider }} tab-pane fade" id="tab-{{ config_module.module_id }}{{ i }}">		*/
/* 	{% if octab.products|length > 0 %}*/
/*         {% for product in octab.products %}*/
/* 			{% if not config_module.slider %}{% set rows = 1 %}{% endif %}*/
/*             <!-- Grid -->*/
/* 			{% if config_module.type == 0 %}	*/
/* 			{% if count % rows == 0 %}*/
/* 			<div class="row_items {% if not config_module.slider %}{{ class }}{% endif %}">*/
/* 			{% endif %}*/
/* 			{% set count = count + 1 %}*/
/* 					*/
/* 				<div class="product-layout grid-style ">*/
/* 					<div class="product-thumb transition">*/
/* 						<div class="item">*/
/* 							<div class="item-inner">*/
/* 								*/
/* 								<div class="image images-container">*/
/* 									<a href="{{ product.href }}" class="product-image">*/
/* 										{% if config_module.rotator and product.rotator_image %}<img class="img-r" src="{{ product.rotator_image }}" alt="{{ product.name }}" />{% endif %}*/
/* 										<img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive" />*/
/* 									</a>				  */
/* 									{% if config_module.salelabel %}*/
/* 										{% if product.special %}*/
/* 										<div class="label-product label_sale">{{ text_label_sale }}</div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 									{% if config_module.newlabel %}*/
/* 										{% if product.is_new %}*/
/* 										<div class="label-product label_new">{{ text_label_new }}</div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 									*/
/* 									{% if config_module.showwishlist or config_module.showquickview or  config_module.showcompare %}	*/
/* 										<div class="action-links">*/
/* 											{% if config_module.showwishlist %}*/
/* 												<button class="button btn-wishlist" type="button"  title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><span>{{ button_wishlist }}</span></button>*/
/* 											{% endif %}*/
/* 											{% if config_module.showcompare %}*/
/* 												<button class="button btn-compare" type="button"  title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><span>{{ button_compare }}</span></button>*/
/* 											{% endif %}*/
/* 											{% if use_quickview %}*/
/* 												{% if config_module.showquickview %}*/
/* 													<button class="button btn-quickview" type="button"  title="{{ button_quickview }}" onclick="ocquickview.ajaxView('{{ product.href }}')"><span>{{ button_quickview }}</span></button>*/
/* 												{% endif %}*/
/* 											{% endif %}*/
/* 										</div>*/
/* 										*/
/* 									{% endif %}*/
/* 									*/
/* 								</div><!-- image -->*/
/* 								<div class="caption">*/
/* 									*/
/* 									{% if product.manufacturer %}*/
/* 									<p class="manufacture-product">*/
/* 										<a href="{{ product.manufacturers }}">{{ product.manufacturer }}</a>*/
/* 									</p>*/
/* 									{% endif %}*/
/* 									{% if product.rating %}*/
/* 										<div class="ratings">*/
/* 											<div class="rating-box">*/
/* 											{% for i in 0..5 %}*/
/* 												{% if product.rating == i %}*/
/* 												{% set class_r = "rating"~i %}*/
/* 												<div class="{{ class_r }}">rating</div>*/
/* 												{% endif %}*/
/* 											{% endfor %}*/
/* 											</div>*/
/* 										</div>					*/
/* 									{% endif %}	*/
/* 									<h4 class="product-name"><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/* 									{% if use_catalog %}*/
/* 									{% if product.price %}*/
/* 										<div class="price-box">*/
/* 										<label>{{ price_label }}</label>*/
/* 										{% if not product.special %}*/
/* 											<p class="regular-price"><span class="price">{{ product.price }}</span></p>*/
/* 										{% else %}*/
/* 											<p class="special-price"><span class="price">{{ product.special }}</span></p>*/
/* 											<p class="old-price"><span class="price">{{ product.price }}</span></p>						  */
/* 										{% endif %}*/
/* 										{% if product.tax %}*/
/* 											<p class="price-tax"><span class="price">{{ text_tax }} {{ product.tax }}</span></p>*/
/* 										{% endif %}*/
/* 										</div>*/
/* 									{% endif %}*/
/* 									{% endif %}*/
/* 									*/
/* 									*/
/* 									{% if config_module.description %}*/
/* 										<p class="product-des">{{ product.description }}</p>*/
/* 									{% endif %}*/
/* 									{% if config_module.countdown %}<div class="text-hurryup"><p>{{ text_hurryup }}</p></div><div id="Countdown{{ product.product_id }}-{{ i }}" class="box-timer"></div> {% endif %}*/
/* 									*/
/* 									{% if use_catalog %}*/
/* 										{% if config_module.showcart %}*/
/* 											<div class="box-hover">*/
/* 												<button class="button btn-cart" type="button"  title="{{ button_cart }}" onclick="cart.add('{{ product.product_id }}');"><span>{{ button_cart }}</span></button>*/
/* 											</div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 									*/
/* 								</div><!-- caption -->*/
/* 							</div>*/
/* 						</div>*/
/* 					</div><!-- product-thumb -->*/
/* 						{% if product.date_end and config_module.countdown %}*/
/* 						<script >*/
/* 						$(document).ready(function () {*/
/* 						$('#Countdown{{ product.product_id }}-{{ i }}').countdown({*/
/* 						until: new Date({{ product.date_end|date("Y") }}, {{ product.date_end|date("m") }}-1, {{ product.date_end|date("d") }}, {{ product.date_end|date("H") }}, {{ product.date_end|date("i") }}, {{ product.date_end|date("s") }}),*/
/* 						labels: ['{{ text_years }}', '{{ text_months }} ', '{{ text_weeks }}', '{{ text_days }}', '{{ text_hrs }}', '{{ text_mins }}', '{{ text_secs }}'],*/
/* 						labels1: ['{{ text_year }}', '{{ text_month }} ', '{{ text_week }}', '{{ text_day }}', '{{ text_hr }}', '{{ text_min }}', '{{ text_sec }}'],*/
/* 						});*/
/* 						// $('#Countdown{{ product.product_id }}-{{ i }}').countdown('pause');*/
/* 						});*/
/* 						</script>*/
/* 						{% endif %}*/
/* 				</div><!-- product-layout -->*/
/* 				{% if (count % rows == 0) or (count == octab.products|length) %}*/
/* 				</div>*/
/* 				{% endif %}*/
/*             {% elseif config_module.type == 1 %}*/
/*             <!-- List -->*/
/* 			{% if count % rows == 0 %}*/
/* 			<div class="row_items {% if not config_module.slider %}{{ class }}{% endif %}">*/
/* 			{% endif %}*/
/* 			{% set count = count + 1 %}*/
/* 				<div class="product-layout list-style ">*/
/* 					<div class="product-thumb transition">*/
/* 						<div class="item">*/
/* 							<div class="item-inner">*/
/* 								<div class="image images-container">*/
/* 									<a href="{{ product.href }}" class="product-image">*/
/* 										{% if config_module.rotator and product.rotator_image %}<img class="img-r" src="{{ product.rotator_image }}" alt="{{ product.name }}" />{% endif %}*/
/* 										<img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive" />*/
/* 									</a>				  */
/* 									{% if config_module.salelabel %}*/
/* 										{% if product.special %}*/
/* 										<div class="label-product label_sale">{{ text_label_sale }}</div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 									{% if config_module.newlabel %}*/
/* 										{% if product.is_new %}*/
/* 										<div class="label-product label_new">{{ text_label_new }}</div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 									{% if use_quickview %}*/
/* 										{% if config_module.showquickview %}*/
/* 											<div class="quick-view"><button class="button btn-quickview" type="button"  title="{{ button_quickview }}" onclick="ocquickview.ajaxView('{{ product.href }}')"><span>{{ button_quickview }}</span></button></div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 								</div><!-- image -->*/
/* 								<div class="caption">*/
/* 									{% if product.manufacturer %}*/
/* 									<p class="manufacture-product">*/
/* 										<a href="{{ product.manufacturers }}">{{ product.manufacturer }}</a>*/
/* 									</p>*/
/* 									{% endif %}*/
/* 									<h4 class="product-name"><a href="{{ product.href }}">{{ product.name }}</a></h4> */
/* 									{% if use_catalog %}*/
/* 									{% if product.price %}*/
/* 										<div class="price-box">*/
/* 										<label>{{ price_label }}</label>*/
/* 										{% if not product.special %}*/
/* 											<p class="regular-price"><span class="price">{{ product.price }}</span></p>*/
/* 										{% else %}*/
/* 											<p class="special-price"><span class="price">{{ product.special }}</span></p>*/
/* 											<p class="old-price"><span class="price">{{ product.price }}</span></p>						  */
/* 										{% endif %}*/
/* 										{% if product.tax %}*/
/* 											<p class="price-tax"><span class="price">{{ text_tax }} {{ product.tax }}</span></p>*/
/* 										{% endif %}*/
/* 										</div>*/
/* 									{% endif %}*/
/* 									{% endif %}*/
/* 									{% if product.rating %}*/
/* 									<div class="ratings">*/
/* 										<div class="rating-box">*/
/* 										{% for i in 0..5 %}*/
/* 											{% if product.rating == i %}*/
/* 											{% set class_r = "rating"~i %}*/
/* 											<div class="{{ class_r }}">rating</div>*/
/* 											{% endif %}*/
/* 										{% endfor %}*/
/* 										</div>*/
/* 									</div>					*/
/* 									{% endif %}*/
/* 									{% if config_module.description %}*/
/* 									<p class="product-des">{{ product.description }}</p>*/
/* 									{% endif %}*/
/* 									{% if config_module.countdown %}<div class="text-hurryup"><p>{{ text_hurryup }}</p></div><div id="Countdown{{ product.product_id }}-{{ i }}" class="box-timer"></div> {% endif %}*/
/* 									{% if config_module.showcart or config_module.showwishlist or  config_module.showcompare %}		*/
/* 										<div class="action-links">*/
/* 											{% if use_catalog %}*/
/* 												{% if config_module.showcart %}*/
/* 													<button class="button btn-cart" type="button"  title="{{ button_cart }}" onclick="cart.add('{{ product.product_id }}');"><span>{{ button_cart }}</span></button>*/
/* 												{% endif %}*/
/* 											{% endif %}*/
/* 											{% if config_module.showwishlist %}*/
/* 												<button class="button btn-wishlist" type="button"  title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><span>{{ button_wishlist }}</span></button>*/
/* 											{% endif %}*/
/* 											{% if config_module.showcompare %}*/
/* 												<button class="button btn-compare" type="button"  title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><span>{{ button_compare }}</span></button>*/
/* 											{% endif %}*/
/* 										</div>*/
/* 									{% endif %}*/
/* 								</div><!-- caption -->*/
/* 							</div>*/
/* 						</div>*/
/* 					</div><!-- product-thumb -->*/
/* 						{% if product.date_end and config_module.countdown %}*/
/* 						<script >*/
/* 						$(document).ready(function () {*/
/* 						$('#Countdown{{ product.product_id }}-{{ i }}').countdown({*/
/* 						until: new Date({{ product.date_end|date("Y") }}, {{ product.date_end|date("m") }}-1, {{ product.date_end|date("d") }}, {{ product.date_end|date("H") }}, {{ product.date_end|date("i") }}, {{ product.date_end|date("s") }}),*/
/* 						labels: ['{{ text_years }}', '{{ text_months }} ', '{{ text_weeks }}', '{{ text_days }}', '{{ text_hrs }}', '{{ text_mins }}', '{{ text_secs }}'],*/
/* 						labels1: ['{{ text_year }}', '{{ text_month }} ', '{{ text_week }}', '{{ text_day }}', '{{ text_hr }}', '{{ text_min }}', '{{ text_sec }}'],*/
/* 						});*/
/* 						// $('#Countdown{{ product.product_id }}-{{ i }}').countdown('pause');*/
/* 						});*/
/* 						</script>*/
/* 						{% endif %}*/
/* 				</div><!-- product-layout -->*/
/* 				{% if (count % rows == 0) or (count == octab.products|length) %}*/
/* 				</div>*/
/* 				{% endif %}*/
/*             {% else %}*/
/*             <!-- other type -->*/
/* 			{% if count % rows == 0 %}*/
/* 			<div class="row_items {% if not config_module.slider %}{{ class }}{% endif %}">*/
/* 			{% endif %}*/
/* 			{% set count = count + 1 %}*/
/* 				<div class="product-layout product-customize ">*/
/* 					<div class="product-thumb transition ">*/
/* 						<div class="item">*/
/* 							<div class="item-inner">*/
/* 								*/
/* 								<div class="image images-container">*/
/* 									<a href="{{ product.href }}" class="product-image">*/
/* 										{% if config_module.rotator and product.rotator_image %}<img class="img-r" src="{{ product.rotator_image }}" alt="{{ product.name }}" />{% endif %}*/
/* 										<img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive" />*/
/* 									</a>				  */
/* 									{% if config_module.salelabel %}*/
/* 										{% if product.special %}*/
/* 										<div class="label-product label_sale">{{ text_label_sale }}</div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 									{% if config_module.newlabel %}*/
/* 										{% if product.is_new %}*/
/* 										<div class="label-product label_new">{{ text_label_new }}</div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 									{% if use_quickview %}*/
/* 										{% if config_module.showquickview %}*/
/* 											<div class="quick-view"><button class="button btn-quickview" type="button"  title="{{ button_quickview }}" onclick="ocquickview.ajaxView('{{ product.href }}')"><span>{{ button_quickview }}</span></button></div>*/
/* 										{% endif %}*/
/* 									{% endif %}*/
/* 									*/
/* */
/* 								</div><!-- image -->*/
/* 								<div class="caption">*/
/* 									{% if product.manufacturer %}*/
/* 									<p class="manufacture-product">*/
/* 										<a href="{{ product.manufacturers }}">{{ product.manufacturer }}</a>*/
/* 									</p>*/
/* 									{% endif %}*/
/* 									<h4 class="product-name"><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/* 									{% if product.rating %}*/
/* 										<div class="ratings">*/
/* 											<div class="rating-box">*/
/* 											{% for i in 0..5 %}*/
/* 												{% if product.rating == i %}*/
/* 												{% set class_r = "rating"~i %}*/
/* 												<div class="{{ class_r }}">rating</div>*/
/* 												{% endif %}*/
/* 											{% endfor %}*/
/* 											</div>*/
/* 										</div>					*/
/* 									{% endif %}	*/
/* 									{% if use_catalog %}*/
/* 									{% if product.price %}*/
/* 										<div class="price-box">*/
/* 										<label>{{ price_label }}</label>*/
/* 										{% if not product.special %}*/
/* 											<p class="regular-price"><span class="price">{{ product.price }}</span></p>*/
/* 										{% else %}*/
/* 											<p class="special-price"><span class="price">{{ product.special }}</span></p>*/
/* 											<p class="old-price"><span class="price">{{ product.price }}</span></p>						  */
/* 										{% endif %}*/
/* 										{% if product.tax %}*/
/* 											<p class="price-tax"><span class="price">{{ text_tax }} {{ product.tax }}</span></p>*/
/* 										{% endif %}*/
/* 										</div>*/
/* 									{% endif %}*/
/* 									{% endif %}*/
/* 									*/
/* 									{% if config_module.countdown %}<div class="text-hurryup"><p>{{ text_hurryup }}</p></div><div id="Countdown{{ product.product_id }}-{{ i }}" class="box-timer"></div> {% endif %}*/
/* 									{% if config_module.showcart or config_module.showwishlist or  config_module.showcompare %}	*/
/* 										<div class="action-links">*/
/* 											{% if use_catalog %}*/
/* 												{% if config_module.showcart %}*/
/* 													<button class="button btn-cart" type="button"  title="{{ button_cart }}" onclick="cart.add('{{ product.product_id }}');"><span>{{ button_cart }}</span></button>*/
/* 												{% endif %}*/
/* 											{% endif %}*/
/* 											{% if config_module.showwishlist %}*/
/* 												<button class="button btn-wishlist" type="button"  title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><span>{{ button_wishlist }}</span></button>*/
/* 											{% endif %}*/
/* 											{% if config_module.showcompare %}*/
/* 												<button class="button btn-compare" type="button"  title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><span>{{ button_compare }}</span></button>*/
/* 											{% endif %}*/
/* 										</div>*/
/* 									{% endif %}*/
/* 									{% if config_module.description %}*/
/* 										<p class="product-des">{{ product.description }}</p>*/
/* 									{% endif %}*/
/* 								</div><!-- caption -->*/
/* 							</div>*/
/* 						</div>*/
/* 						*/
/* 					</div><!-- product-thumb -->*/
/* 						{% if product.date_end and config_module.countdown %}*/
/* 						<script >*/
/* 						$(document).ready(function () {*/
/* 						$('#Countdown{{ product.product_id }}-{{ i }}').countdown({*/
/* 						until: new Date({{ product.date_end|date("Y") }}, {{ product.date_end|date("m") }}-1, {{ product.date_end|date("d") }}, {{ product.date_end|date("H") }}, {{ product.date_end|date("i") }}, {{ product.date_end|date("s") }}),*/
/* 						labels: ['{{ text_years }}', '{{ text_months }} ', '{{ text_weeks }}', '{{ text_days }}', '{{ text_hrs }}', '{{ text_mins }}', '{{ text_secs }}'],*/
/* 						labels1: ['{{ text_year }}', '{{ text_month }} ', '{{ text_week }}', '{{ text_day }}', '{{ text_hr }}', '{{ text_min }}', '{{ text_sec }}'],*/
/* 						});*/
/* 						// $('#Countdown{{ product.product_id }}-{{ i }}').countdown('pause');*/
/* 						});*/
/* 						</script>*/
/* 						{% endif %}*/
/* 				</div><!-- product-layout -->*/
/* 			{% if (count % rows == 0) or (count == octab.products|length) %}*/
/* 				</div>*/
/* 			{% endif %}*/
/* 			{% endif %}*/
/* 				*/
/*           {% endfor %}		  		  */
/* 		{% else %}*/
/* 			<p class="text_empty">{{ text_empty }}</p>*/
/* 		{% endif %}*/
/*         </div>*/
/* 		{% set i= i+1 %}		*/
/* 		{% endfor %}		*/
/*     </div>*/
/*   </div>*/
/*   </div>*/
/*   </div>*/
/*   <div class="clearfix"></div>*/
/* </div>*/
/* */
/*   <script >*/
/*     $(document).ready(function() {*/
/*       $('a[href="#tab-{{ config_module.module_id }}0"]').trigger( "click" );*/
/* 	  {% if config_module.slider %}*/
/*       $(".tab-container-{{ config_module.module_id }}").owlCarousel({*/
/* 		items: {{ config_module.items }},*/
/*         loop: {% if config_module.loop %} true {% else %} false {% endif %},*/
/*         margin: {% if config_module.margin is defined %} {{ config_module.margin }} {% else %} 20 {% endif %},*/
/*         nav: {% if config_module.navigation %} true {% else %} false {% endif %},*/
/*         dots: {% if config_module.pagination %} true {% else %} false {% endif %},*/
/*         autoplay:  {% if config_module.auto %} true {% else %} false {% endif %},*/
/*         autoplayTimeout: {% if config_module.time %} {{ config_module.time }} {% else %} 2000 {% endif %},*/
/*         autoplayHoverPause: true,*/
/*         autoplaySpeed: {% if config_module.speed %} {{ config_module.speed }} {% else %} 3000 {% endif %},*/
/*         navSpeed: {% if config_module.speed %} {{ config_module.speed }} {% else %} 3000 {% endif %},*/
/*         dotsSpeed: {% if config_module.speed %} {{ config_module.speed }} {% else %} 3000 {% endif %},*/
/* 		lazyLoad: true,*/
/*         responsive:{*/
/* 			0:{*/
/* 				items: 1,*/
/* 				nav: false*/
/* 			},*/
/* 			480:{*/
/* 				items: {{ config_module.smobile }},*/
/* 				nav: false*/
/* 			},*/
/* 			768:{*/
/* 				items: {{ config_module.mobile }}*/
/* 			},*/
/* 			992:{*/
/* 				items: {{ config_module.tablet }}*/
/* 			},*/
/* 			1200:{*/
/* 				items: {{ config_module.desktop }}*/
/* 			},*/
/* 			1600:{*/
/* 				items: {{ config_module.items }}*/
/* 			}*/
/* 		},*/
/* 		onInitialized: function() {*/
/* 			var count = $(".tab-container-{{ config_module.module_id }} .owl-item.active").length;*/
/* 			if(count == 1) {*/
/* 				$(".tab-container-{{ config_module.module_id }} .owl-item").removeClass('first');*/
/* 				$(".tab-container-{{ config_module.module_id }} .owl-item.active").addClass('first');*/
/* 			} else {*/
/* 				$(".tab-container-{{ config_module.module_id }} .owl-item").removeClass('first');*/
/* 				$(".tab-container-{{ config_module.module_id }} .owl-item.active:first").addClass('first');*/
/* 			}*/
/* 		},*/
/* 		onTranslated: function() {*/
/* 			var count = $(".tab-container-{{ config_module.module_id }} .owl-item.active").length;*/
/* 			if(count == 1) {*/
/* 				$(".tab-container-{{ config_module.module_id }} .owl-item").removeClass('first');*/
/* 				$(".tab-container-{{ config_module.module_id }} .owl-item.active").addClass('first');*/
/* 			} else {*/
/* 				$(".tab-container-{{ config_module.module_id }} .owl-item").removeClass('first');*/
/* 				$(".tab-container-{{ config_module.module_id }} .owl-item.active:first").addClass('first');*/
/* 			}*/
/* 		}*/
/*       });*/
/* 	{% endif %}*/
/*     });*/
/*   </script>*/
/* */
/* */
