<?php
// Heading
$_['heading_title']    		= '<b style="color:rgb(219, 0, 0);font-size:15px;">Live Sales Popup: product sold notification	Module</b>';
$_['heading_title1']    	= 'Live Sales Popup: product sold notification	Module
';

// Text
$_['text_module']      		= 'Modules';
$_['text_success']     	= 'Success: You have modified live sales popup module!';
$_['text_edit']        		= 'Edit Live Sales Popup: product sold notification	Module';

$_['text_top_left']        	= 'Top Left';
$_['text_top_right']        = 'Top Right';
$_['text_bottom_left']      = 'Bottom Left';
$_['text_bottom_right']     = 'Bottom Right';
$_['text_mtop']     		= 'Top';
$_['text_mbottom']     		= 'Bottom';
$_['text_random']     		= 'Random';
$_['text_asc']     			= 'ASC';
$_['text_desc']     		= 'DESC';

// Tabs
$_['tab_general'] 			= 'General';
$_['tab_popup_setting'] 	= 'Order Popup Setting';
$_['tab_cart_popup'] 		= 'Add to cart popup setting';
$_['tab_about'] 			= 'About';

// Button
$_['button_save_stay'] 		= 'Save & stay';

// Entry
$_['entry_status_1'] 		= 'Status';
$_['entry_oct_tune'] 		= 'Order and cart time tune';
$_['entry_thumb'] 			= 'Thumb dimensions';
$_['entry_delay_first'] 	= 'Delay first notification';
$_['entry_display_time'] 	= 'Display time';
$_['entry_delay_between'] 	= 'Delay between notifications';
$_['entry_sound'] 			= 'Play sound on notification display';
$_['entry_cutomer_session'] = 'Display X times for customer (session)';
$_['entry_close_button'] 	= 'Enable close button';
$_['entry_cookie_time'] 	= 'Close cookie lifetime';
$_['entry_bg_color'] 		= 'Background color';
$_['entry_border_color'] 	= 'Border color';
$_['entry_border_radius'] 	= 'Border radius';
$_['entry_shadow_color'] 	= 'Shadow color';
$_['entry_shadow_size'] 	= 'Shadow size';
$_['entry_text_color'] 		= 'Text color';
$_['entry_link_color'] 		= 'Link color';
$_['entry_close_btn_color'] = 'Close button color';
$_['entry_opacity'] 		= 'Opacity';
$_['entry_opacity_on_hover'] = 'Opacity on hover';
$_['entry_in_animation']	= 'In animation';
$_['entry_out_animation'] 	= 'Out animation';
$_['entry_extra_css'] 		= 'Extra CSS styles';
$_['entry_position'] 		= 'Desktop Position';
$_['entry_top'] 			= 'Desktop Top';
$_['entry_bottom'] 			= 'Desktop Bottom';
$_['entry_left'] 			= 'Desktop Left';
$_['entry_right'] 			= 'Desktop Right';
$_['entry_popup_width'] 	= 'Desktop Popup width';
$_['entry_popup_height'] 	= 'Desktop Popup height';
$_['entry_mposition'] 		= 'Mobile Position';
$_['entry_mtop'] 			= 'Mobile Top';
$_['entry_mbottom'] 		= 'Mobile Bottom';
$_['entry_mleft'] 			= 'Mobile Left';
$_['entry_mright'] 			= 'Mobile Right';
$_['entry_mpopup_width'] 	= 'Mobile Popup width';
$_['entry_mpopup_height'] 	= 'Mobile Popup height';
$_['entry_status'] 			= 'Enabled';
$_['entry_content_1'] 		= 'Content line 1';
$_['entry_content_2'] 		= 'Content line 2';
$_['entry_content_3'] 		= 'Content line 3';
$_['entry_content_4'] 		= 'Content line 4';
$_['entry_content_5'] 		= 'Content line 5';
$_['entry_sequence'] 		= 'Order sequence';
$_['entry_loop'] 			= 'Loop';
$_['entry_active_product'] 	= 'Display not active products';
$_['entry_min_amount'] 		= 'Minimum order amount';
$_['entry_order_status'] 	= 'Show orders with status';
$_['entry_placeholder'] 	= 'Name placeholder';

// Help
$_['help_delay_first'] 		= 'Delay first notification for x miliseconds after page is loaded. You can enter two values exploded &quot;-&quot; sign. Exmple: 2000-10000';
$_['help_display_time'] 	= 'How long a notification stays on the screen (in miliseconds)';
$_['help_delay_between'] 	= 'Delay for next notificaton will be displayed (in miliseconds). You can enter two values exploded &quot;-&quot; sign. Exmple: 2000-10000';
$_['help_customer_session'] = 'How many times popups will be displayed per user session. Enter 0 to unlimited number of displays';
$_['help_cookie_time'] 		= 'Value in days or 0. If customer has closed the popup, it will not be displayed the specified number of days. 0 - popup will be displayed on next session.';
$_['help_popup_width'] 		= 'Popup width in &quot;px&quot; or &quot;%&quot;. Example: 300px, 100%';
$_['help_popup_height'] 	= 'Popup height in &quot;px&quot;. Example: 92';
$_['help_active_product'] 	= 'If this option active, order popup will be displayed even if product is not active (link to this product will be removed from popup).';
$_['help_placeholder'] 		= 'If product added by guest user tags firstname, lastname, firstname_lastname and lastname_firstname will be replaced by this value';
$_['help_version_support']	= 'Support version 2.1.0.1 and Latter';


// Error
$_['error_permission'] = 'Warning: You do not have permission to modify live sales popup module!';