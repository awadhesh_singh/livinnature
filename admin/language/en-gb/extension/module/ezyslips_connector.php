<?php
// Heading
$_['heading_title']    		= '<font color = "#0099FF"><b>Ezyslips Connector</b></font>'; 
$_['site_title']       		= 'Ezyslips Connector';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Ezyslips Connector!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_edit']   		  = 'Connect to Ezyslips';

// Entry
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Buttons

$_['button_back']		= 'Back';
$_['button_save']		= 'Save';
$_['button_cancel']		= 'Cancel';
// Error
$_['error_permission']  = 'Warning: You do not have permission to modify module Ezyslips Connector!';
$_['error_email']    	= 'Warning: Please fill a valid email.';
$_['error_password']    = 'Warning: Please enter password.';
$_['invalid_fields']    = 'Warning: Please fill valid ezyslips credentials.';
$_['login_failed']    	= 'Warning: Login failed. Email or password is incorrect.';
$_['error_order_status']= 'Warning: Please select order status!';

?>