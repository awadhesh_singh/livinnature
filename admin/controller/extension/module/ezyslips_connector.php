<?php
class ControllerExtensionModuleEzyslipsConnector extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/ezyslips_connector');

		$this->document->setTitle($this->language->get('site_title'));

		$this->load->model('setting/module');
		
		$this->load->model('setting/setting');
		
		$data['error_warning'] = '';
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			$this->model_setting_setting->editSetting('module_ezyslips_connector', $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], true));
		}
        
	    $this->load->model('localisation/order_status');
	    $order_status = $this->model_localisation_order_status->getOrderStatuses();
	    $data['order_statuses'] = $order_status;
		$data['heading_title'] 	= $this->language->get('heading_title');
		$data['text_edit'] 		= $this->language->get('text_edit');
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'], true)
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/ezyslips_connector', 'user_token=' . $this->session->data['user_token'], true)
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/ezyslips_connector', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true)
			);
		}
		
		$data['action'] = $this->url->link('extension/module/ezyslips_connector', 'user_token=' . $this->session->data['user_token'], true);
		
		$data['cancel'] = $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'], true);
		
		$data['button_save'] 	= $this->language->get('button_save');
		$data['button_cancel'] 	= $this->language->get('button_cancel');

		if (isset($this->request->post['module_ezyslips_connector'])) {
			
            $data['email']    	= $this->request->post['module_ezyslips_connector']['email'];
            $data['password'] 	= $this->request->post['module_ezyslips_connector']['password'];
            $data['store_url'] 	= $this->request->post['module_ezyslips_connector']['store_url'];

            if(isset($this->request->post['ezyslips_connector_selected'])){
            $data['selected']   = $this->request->post['module_ezyslips_connector_selected'];
			}else{
			$data['selected']   = array();	
			}
            
			
        } elseif ($this->config->get('module_ezyslips_connector')) {
			$order_info_selected = $this->config->get('module_ezyslips_connector_selected');
            $module_info		= $this->config->get('module_ezyslips_connector');
            $data['email']    	= $module_info['email'];
            $data['password'] 	= $module_info['password'];
            $data['store_url'] 	= $module_info['store_url'];
            $data['selected']   = $order_info_selected;
           
        } else {
			
            $data['email']    	= "";
            $data['password'] 	= "";
            $data['store_url'] 	= "";
            $data['selected']   = array();
           
        }
		
		$ezyslips_connector_status = 0;
		
		if( isset($this->request->post['module_ezyslips_connector_status']) ){
			$ezyslips_connector_status = $this->request->post['module_ezyslips_connector_status'];
		}elseif($this->config->get('module_ezyslips_connector_status')){
			$ezyslips_connector_status = $this->config->get('module_ezyslips_connector_status');
		}else{
			$ezyslips_connector_status = 0;
		}
		
		$data['ezyslips_connector_status'] = $ezyslips_connector_status;
		 
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['order_status'])) {
			$data['error_order_status'] = $this->error['order_status'];
		} else {
			$data['error_order_status'] = '';
		}

		
		
		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}
		
		if (isset($this->error['password'])) {
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}
		
		$data['user_token'] = $this->session->data['user_token'];
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/ezyslips_connector', $data));
		
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/ezyslips_connector')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if( !isset($this->request->post['module_ezyslips_connector']['email']) ){
			$this->error['email'] = $this->language->get('error_email');
		}
		
		if( !isset($this->request->post['module_ezyslips_connector']['password']) ){
			$this->error['password'] = $this->language->get('error_password');
		}
		
		if(!filter_var($this->request->post['module_ezyslips_connector']['email'], FILTER_VALIDATE_EMAIL)){
			$this->error['email'] = $this->language->get('error_email');
		}
		
		if(!$this->authenticateEzyslips( trim($this->request->post['module_ezyslips_connector']['email']) , trim($this->request->post['module_ezyslips_connector']['password']) , trim($this->request->post['module_ezyslips_connector']['store_url']) ) ){
			$this->error['warning'] = "Ezyslips authentication failed.";
		}

		if( !isset($this->request->post['module_ezyslips_connector_selected']) ){
			$this->error['order_status'] = $this->language->get('error_order_status');
		
		}

		return !$this->error;
	}
	
	protected function authenticateEzyslips($username , $api_key ,$store_url ){
		
		if(!$store_url){
			return false;
		}
		
		$storeInfo = array();
		$storeInfo['group']	= 'store';
		$storeInfo['key']	= 'opencart';
		$storeInfo['value']	= array('store_url'=>$store_url,'api_version' => '1.0.4');
		
		$jsonData = array(
			'operation' => 'save_compnay_setting',
			'company_settings' => $storeInfo
		);
		
		$jsonData = json_encode($jsonData);
		
		$url = "http://ezyslips.com/api/ezyauth";
		
		$auth_hash = base64_encode( $username.':'.$api_key );
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);		
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonData);
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'authorization: Basic '.$auth_hash,
			'Content-Type: application/json',
			'Accept: application/json'
		));
		
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		$output = curl_exec($curl);
		$output = json_decode($output,true);	
		
		if(curl_errno($curl) > 0)  {
			return 0 ;
		}
		curl_close($curl);

		return ( isset($output['status']) && $output['status'] == 'success' );
	}
}

?>