<?php
class ControllerExtensionModuleLiveSalesPopup extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/live_sales_popup');

		$this->document->setTitle($this->language->get('heading_title1'));

		$this->load->model('setting/setting');

		$this->document->addStyle('view/javascript/colorpicker/dist/css/bootstrap-colorpicker.min.css');
		$this->document->addScript('view/javascript/colorpicker/dist/js/bootstrap-colorpicker.min.js');
		
		if(!$this->config->has('module_live_sales_popup')){ //die('12');
			$this->installDB();
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

		$this->model_setting_setting->editSetting('module_live_sales_popup', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');
			
			if($this->request->post['save_stay'] && $this->request->post['save_stay'] == 1){
				$this->response->redirect($this->url->link('extension/module/live_sales_popup', 'user_token=' . $this->session->data['user_token'], true));
			}else{
				$this->response->redirect($this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] .'&type=module', true));
			}
		}

		$data['heading_title1'] = $this->language->get('heading_title1');
		$data['heading_title2'] = $this->language->get('heading_title2');
		
		$data['user_token'] = $this->session->data['user_token'];

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');		
		$data['text_height'] = $this->language->get('text_height');
		$data['text_width'] = $this->language->get('text_width');		
		$data['text_none'] = $this->language->get('text_none');
		$data['text_top_left'] = $this->language->get('text_top_left');
		$data['text_top_right'] = $this->language->get('text_top_right');
		$data['text_bottom_left'] = $this->language->get('text_bottom_left');
		$data['text_bottom_right'] = $this->language->get('text_bottom_right');
		$data['text_mtop'] = $this->language->get('text_mtop');
		$data['text_mbottom'] = $this->language->get('text_mbottom');
		$data['text_random'] = $this->language->get('text_random');
		$data['text_asc'] = $this->language->get('text_asc');
		$data['text_desc'] = $this->language->get('text_desc');
		
		
		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_popup_setting'] = $this->language->get('tab_popup_setting');
		$data['tab_cart_popup'] = $this->language->get('tab_cart_popup');
		$data['tab_about'] = $this->language->get('tab_about');
		
		$data['entry_status_1'] = $this->language->get('entry_status_1');
		$data['entry_oct_tune'] = $this->language->get('entry_oct_tune');
		$data['entry_thumb'] = $this->language->get('entry_thumb');
		$data['entry_delay_first'] = $this->language->get('entry_delay_first');
		$data['entry_display_time'] = $this->language->get('entry_display_time');
		$data['entry_delay_between'] = $this->language->get('entry_delay_between');
		$data['entry_sound'] = $this->language->get('entry_sound');
		$data['entry_cutomer_session'] = $this->language->get('entry_cutomer_session');		
		$data['entry_close_button'] = $this->language->get('entry_close_button');
		$data['entry_cookie_time'] = $this->language->get('entry_cookie_time');
		$data['entry_bg_color'] = $this->language->get('entry_bg_color');
		$data['entry_border_color'] = $this->language->get('entry_border_color');
		$data['entry_border_radius'] = $this->language->get('entry_border_radius');
		$data['entry_shadow_color'] = $this->language->get('entry_shadow_color');
		$data['entry_shadow_size'] = $this->language->get('entry_shadow_size');
		$data['entry_text_color'] = $this->language->get('entry_text_color');
		$data['entry_link_color'] = $this->language->get('entry_link_color');
		$data['entry_close_btn_color'] = $this->language->get('entry_close_btn_color');
		$data['entry_opacity'] = $this->language->get('entry_opacity');
		$data['entry_opacity_on_hover'] = $this->language->get('entry_opacity_on_hover');
		$data['entry_in_animation'] = $this->language->get('entry_in_animation');
		$data['entry_out_animation'] = $this->language->get('entry_out_animation');
		$data['entry_extra_css'] = $this->language->get('entry_extra_css');
		$data['entry_position'] = $this->language->get('entry_position');
		$data['entry_top'] = $this->language->get('entry_top');
		$data['entry_bottom'] = $this->language->get('entry_bottom');
		$data['entry_left'] = $this->language->get('entry_left');
		$data['entry_right'] = $this->language->get('entry_right');
		$data['entry_popup_width'] = $this->language->get('entry_popup_width');
		$data['entry_popup_height'] = $this->language->get('entry_popup_height');
		$data['entry_mposition'] = $this->language->get('entry_mposition');
		$data['entry_mtop'] = $this->language->get('entry_mtop');
		$data['entry_mbottom'] = $this->language->get('entry_mbottom');
		$data['entry_mleft'] = $this->language->get('entry_mleft');
		$data['entry_mright'] = $this->language->get('entry_mright');
		$data['entry_mpopup_width'] = $this->language->get('entry_mpopup_width');
		$data['entry_extra_css'] = $this->language->get('entry_extra_css');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_content_1'] = $this->language->get('entry_content_1');
		$data['entry_content_2'] = $this->language->get('entry_content_2');
		$data['entry_content_3'] = $this->language->get('entry_content_3');
		$data['entry_content_4'] = $this->language->get('entry_content_4');
		$data['entry_content_5'] = $this->language->get('entry_content_5');
		$data['entry_sequence'] = $this->language->get('entry_sequence');
		$data['entry_active_product'] = $this->language->get('entry_active_product');
		$data['entry_min_amount'] = $this->language->get('entry_min_amount');
		$data['entry_order_status'] = $this->language->get('entry_order_status');
		$data['entry_placeholder'] = $this->language->get('entry_placeholder');
		
		$data['help_delay_first'] = $this->language->get('help_delay_first');
		$data['help_display_time'] = $this->language->get('help_display_time');
		$data['help_delay_between'] = $this->language->get('help_delay_between');
		$data['help_cookie_time'] = $this->language->get('help_cookie_time');
		$data['help_popup_width'] = $this->language->get('help_popup_width');
		$data['help_popup_height'] = $this->language->get('help_popup_height');
		$data['help_active_product'] = $this->language->get('help_active_product');
		$data['help_placeholder'] = $this->language->get('help_placeholder');
		$data['help_version_support'] = $this->language->get('help_version_support');
		

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_save_stay'] = $this->language->get('button_save_stay');
		$data['ar_lsf_time_offset'] = array();
		$data['ar_lsf_time_offset'] = array(
			'-21600' =>  '-06:00',
			'-18000' =>  '-05:00',
			'-14400' =>  '-04:00',
			'-10800' =>  '-03:00',
			'-7200' =>  '-02:00',
			'-3600' =>  '-01:00',
			'0' => 'None',   
			'3600' =>  '+01:00',   
			'7200' =>  '+02:00',   
			'10800' =>  '+03:00',   
			'14400' =>  '+04:00',   
			'18000' =>  '+05:00',   
			'21600' => '+06:00'
		);
		
		$data['sounds'] = array();
		
		$data['sounds'] = array(
			'notification-03.mp3', 'notification-02.mp3', 'notification-01.mp3', 'facebook-notification.mp3', 'iphone-sms.mp3', 'samsung-whistle-notification.mp3'
		);

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title1'),
			'href' => $this->url->link('extension/module/live_sales_popup', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/live_sales_popup', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token']. '&type=module', true);

		if (isset($this->session->data['success'])) {
			
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$data['in_animations'] = array(
					'Bounce' => 'GROUP_START',
					'bounceIn' => 'bounceIn', 
					'bounceInDown' => 'bounceInDown', 
					'bounceInLeft' => 'bounceInLeft', 
					'bounceInRight'  => 'bounceInRight', 
					'bounceInUp' => 'bounceInUp', 
					'Bounceend' => 'GROUP_END',
					'Fade' => 'GROUP_START',
					'fadeIn' => 'fadeIn', 
					'fadeInDown' => 'fadeInDown', 
					'fadeInDownBig' => 'fadeInDownBig', 
					'fadeInLeft' => 'fadeInLeft', 
					'fadeInLeftBig' => 'fadeInLeftBig', 
					'fadeInRight' => 'fadeInRight', 
					'fadeInRightBig' => 'fadeInRightBig', 
					'fadeInUp' => 'fadeInUp', 
					'fadeInUpBig' => 'fadeInUpBig', 
					'Fadeend' => 'GROUP_END',
					'Flip' => 'GROUP_START',
					'flip' => 'flip', 
					'flipInX' => 'flipInX', 
					'flipInY' => 'flipInY', 
					'LightSpeed' => 'GROUP',
					'lightSpeedIn' => 'lightSpeedIn', 
					'Rotate' => 'GROUP_START',
					'rotateIn' => 'rotateIn', 
					'rotateInDownLeft' => 'rotateInDownLeft', 
					'rotateInDownRight' => 'rotateInDownRight', 
					'rotateInUpLeft' => 'rotateInUpLeft', 
					'rotateInUpRight' => 'rotateInUpRight',
					'Flipend' => 'GROUP_END',					
					'Slide' => 'GROUP_START',
					'slideInUp' => 'slideInUp', 
					'slideInDown' => 'slideInDown', 
					'slideInLeft' => 'slideInLeft', 
					'slideInRight' => 'slideInRight', 
					'Slideend' => 'GROUP_END',
					'Zoom' => 'GROUP_START',
					'zoomIn' => 'zoomIn', 
					'zoomInDown' => 'zoomInDown', 
					'zoomInLeft' => 'zoomInLeft', 
					'zoomInRight' => 'zoomInRight', 
					'zoomInUp' => 'zoomInUp',
					'Zoomend' => 'GROUP_END',			
					'Roll' => 'GROUP_START',
					'rollIn' => 'rollIn',
					'Rollend' => 'GROUP_END'
				);
		
		$data['out_animations'] = array(
					'Bounce' => 'GROUP_START',
					'bounceOut' => 'bounceOut',
					'bounceOutDown' => 'bounceOutDown',
					'bounceOutLeft' => 'bounceOutLeft',
					'bounceOutRight' => 'bounceOutRight',
					'bounceOutUp' => 'bounceOutUp',
					'Bounceend' => 'GROUP_END',
					'Fade' => 'GROUP_START',
					'fadeOut' => 'fadeOut',
					'fadeOutDown' => 'fadeOutDown',
					'fadeOutDownBig' => 'fadeOutDownBig',
					'fadeOutLeft' => 'fadeOutLeft',
					'fadeOutLeftBig' => 'fadeOutLeftBig',
					'fadeOutRight' => 'fadeOutRight',
					'fadeOutRightBig' => 'fadeOutRightBig',
					'fadeOutUp' => 'fadeOutUp',
					'fadeOutUpBig' => 'fadeOutUpBig',
					'Fadeend' => 'GROUP_END',
					'Flip' => 'GROUP_START',
					'flipOutX' => 'flipOutX',
					'flipOutY' => 'flipOutY',
					'Flipend' => 'GROUP_END',
					'Light' => 'GROUP_START',
					'lightSpeedOut' => 'lightSpeedOut',
					'lightend' => 'GROUP_END',
					'Rotate' => 'GROUP_START',
					'rotateOut' => 'rotateOut',
					'rotateOutDownLeft' => 'rotateOutDownLeft',
					'rotateOutDownRight' => 'rotateOutDownRight',
					'rotateOutUpLeft' => 'rotateOutUpLeft',
					'rotateOutUpRight' => 'rotateOutUpRight',
					'Rotateend' => 'GROUP_END',
					'Slide' => 'GROUP_START',
					'slideOutUp' => 'slideOutUp',
					'slideOutDown' => 'slideOutDown',
					'slideOutLeft' => 'slideOutLeft',
					'slideOutRight' => 'slideOutRight',
					'Slideend' => 'GROUP_END',
					'Zoom' => 'GROUP_START',
					'zoomOut' => 'zoomOut',
					'zoomOutDown' => 'zoomOutDown',
					'zoomOutLeft' => 'zoomOutLeft',
					'zoomOutRight' => 'zoomOutRight',
					'zoomOutUp' => 'zoomOutUp',
					'Zoomend' => 'GROUP_END',
					'Roll' => 'GROUP_START',
					'hinge' => 'hinge',
					'rollOut' => 'rollOut',
					'Rollend' => 'GROUP_END'

				);

				
		$live_sales_popup = $this->config->get('module_live_sales_popup');
		
		//echo "<pre>"; print_r($live_sales_popup); echo "</pre>";
		
		$data['live_sales_popup_status'] = $this->config->get('module_live_sales_popup_status');
		
		$data['live_sales'] = array();
		foreach($live_sales_popup as $key => $livesales){
			$data['live_sales'][$key] = (isset($livesales)) ? $livesales : '';
		}
		//echo "<pre>"; print_r($data['live_sales']); echo "</pre>";
	
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/live_sales_popup', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/live_sales_popup')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	
	private function installDB(){
		$this->load->model('setting/setting');
		$arr_live_sales = array();
		
		$arr_live_sales['live_sales_popup'] = array('width' => '100', 'height' => '100', 'delay_first' => '1000-2000', 'display_time' => '5000', 'delay_between' => '5000-10000', 'sound' => 'iphone-sms.mp3', 'cutomer_session' => '0', 'close_button' => '1', 'cookie_time' => '365', 'bg_color' => '#292929', 'border_color' => '#000000', 'border_radius' => '8px', 'shadow_color' => '#767676', 'shadow_size' => '3', 'text_color' => '#ffffff', 'link_color' => '#ff0000', 'close_btn_color' => '#ffda00', 'opacity' => '0.80', 'opacity_on_hover' => '1', 'in_animation' => 'bounceInLeft', 'out_animation' => 'bounceOutLeft', 'extra_css' => '',  'position' => 'bottom_left', 'top' => '20', 'bottom' => '20', 'left' => '20', 'right' => '20', 'popup_width' => '450px', 'popup_height' => '100px',  'mposition' => 'mbottom', 'mtop' => '20', 'mbottom' => '20', 'mleft' => '10', 'mright' => '10', 'mpopup_width' => 'auto', 'mpopup_height' => '92', 'content_line_1' => '{firstname} {lastname} from {city} bought', 'content_line_2' => '{product_name}',  'content_line_3' => 'Order Total : {order_total}', 'content_line_4' => 'about {time_ago}', 'content_line_5' => '', 'loop' => '1', 'sequence' => 'rand', 'active_product' => '1', 'min_amount' => '', 'order_status' => array());	
		$this->model_setting_setting->editSetting('module_live_sales_popup', $arr_live_sales);
	}
}